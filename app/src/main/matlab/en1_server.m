function en1_server(cfg)
	%dbstop if error;

	% Options for 3Dexperiments EXECOMM
	defopt.query_type 				= 'nn';
	defopt.ehash 					= 'd57cf534c6b1382f832558ca1810e4a9';

	global srvsock;
	if srvsock ~= -1, msclose(srvsock); end

	defcfg.engine_sock = 3122;
	defcfg.cache = false;
	cfg = optmerge(defcfg, cfg);
	if cfg.cache
		disp('Using cache...');
	end

	srvsock = mslisten(cfg.engine_sock);

	comm_newcache;
	while 1
		out = [];
		try
			fprintf(['----------------------------------------------\n' ...
			         '  Listening (%s:%d)...'], cfg.host, cfg.engine_sock);

			sock = -1;
			while (sock == -1)
				sock = msaccept(srvsock, 1);
				drawnow %to process event queue
				heartbeat();
			end
			fprintf('\n');
			msg = msrecv(sock);
            fprintf('recv ended\n');
			clear mex; rehash;

			if (isempty(msg) || strcmp(msg, '[]'))
				query = [];
			else
				try
					query = json2mat(msg);

					if (~isstruct(query))
						whos msg query;
						disp(query);
						error('Server is accepting only json comands. Use a new frontend!');
					end
				catch
					disp(msg);
				end
            end
            
            disp(msg);
            disp('query:');
            disp(query);

			query.cfg = [];
			cfg = optmerge(cfg, query.cfg);
            opt = [];
            
            whos query;
            
            %if lower(query.command) == 'query'
             %   opt = query.opt;
            %end
            
            out.request = query;

			fprintf('\n%s: \n', datestr(now));

			%disp('query:'); disp(query);
			disp('opt:'); disp(opt);
			disp('cfg:'); disp(cfg);

			%take loaded engine from cache or load it
			% EE = funcache(@en4_load, opt.ehash, cfg.en4_rootdir);
			% out.engine = en4_get_engine_info(EE);

			EE = textread('filenames.txt', '%s\n');  % dummy search engine, just give some filenames
			cfg.N = numel(EE); % how many images exist in the database
			opt.ntop = 10; % how many images to show

			switch lower(query.command)
				case 'stop'
				break;

				case 'query'
					out = optmerge(out, comm1_query(EE, opt, cfg));

                case 'upload'
                    out.request = 'upload';
                    out.filename = strcat(datestr(now,'dd-mm-yyyy_HH_MM_SS_FFF'), query.filename);
                    packed_data = base64decode(query.val);
                    fileId = fopen(out.filename , 'w');
                    fwrite(fileId, packed_data);
                    fclose(fileId);
                    out = optmerge(out, comm1_query(EE, opt, cfg));
				otherwise
					error('Error. Unknown command.');
			end
			disp(out);

			% keyboard

			mssend(sock, mat2json(out));
			msclose(sock);

		catch MExc
			err = lasterror;
			out.error = showerror(MExc);
			disp(out.error);

			%reformat output to be stringifiable by json
			out.error(out.error == '"') = '''';
			out.error = implode(explode(out.error, sprintf('\n')), '~~~');

			mat2json(out)
			mssend(sock, mat2json(out));
			msclose(sock);
		end

	end

	fprintf('Stopping the service\n');
	msclose(srvsock);
	srvsock = -1;
end
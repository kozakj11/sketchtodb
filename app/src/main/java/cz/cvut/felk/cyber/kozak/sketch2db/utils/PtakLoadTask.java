package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.graphics.Bitmap;
import android.widget.ImageView;
import cz.cvut.felk.cyber.kozak.sketch2db.gallery.PtakImage;

import java.lang.ref.WeakReference;

/**
 * Created by kozack on 27. 6. 2016.
 */
public class PtakLoadTask{

    private byte[] buffer;
    private Bitmap resultImage;
    private boolean isCacheEnabled;
    private PtakLoadDownloader loadDownloader;
    private PtakLoadDecoder loadDecoder;
    private Thread currentThread;
    private PtakLoadCacher cacher;
    private int scale;

    private WeakReference<PtakImage> callerView;
    private String path;

    private int reqHeight, reqWidth;

    public PtakLoadTask() {
        this.loadDownloader = new PtakLoadDownloader(this);
        this.loadDecoder = new PtakLoadDecoder(this);
        this.cacher = new PtakLoadCacher(this, false);
    }

    public void initTask(PtakImage callerView, int reqHeight, int reqWidth, boolean isCacheEnabled, boolean useDetailed) {
        this.callerView = new WeakReference<>(callerView);
        this.path = callerView.getPath();
        this.isCacheEnabled = isCacheEnabled;
        this.reqHeight = reqHeight;
        this.reqWidth = reqWidth;
        this.loadDownloader.useDetailed(useDetailed);
    }

    public int getReqHeight() {
        return reqHeight;
    }

    public int getReqWidth() {
        return reqWidth;
    }

    public PtakImage getCallerView() {
        if (callerView != null) {
            return callerView.get();
        } else {
            return null;
        }
    }

    public Bitmap getResultImage() {
        return resultImage;
    }

    public void setResultImage(Bitmap resultImage, int scale) {
        this.resultImage = resultImage;
        this.scale = scale;
    }

    public int getScale() {
        return scale;
    }

    public Thread getCurrentThread() {
        synchronized (PtakLoadManager.PTAK_LOAD_MANAGER) {
            return currentThread;
        }
    }

    public void setCurrentThread(Thread currentThread) {
        synchronized (PtakLoadManager.PTAK_LOAD_MANAGER) {
            this.currentThread = currentThread;
        }
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public void setBuffer(byte[] buffer) {
        this.buffer = buffer;
    }

    public boolean isCacheEnabled() {
        return isCacheEnabled;
    }

    public void setCacheEnabled(boolean cacheEnabled) {
        isCacheEnabled = cacheEnabled;
    }

    public PtakLoadDecoder getLoadDecoder() {
        return loadDecoder;
    }

    public void setLoadDecoder(PtakLoadDecoder loadDecoder) {
        this.loadDecoder = loadDecoder;
    }

    public PtakLoadDownloader getLoadDownloader() {
        return loadDownloader;
    }

    public void setLoadDownloader(PtakLoadDownloader loadDownloader) {
        this.loadDownloader = loadDownloader;
    }

    public String getPath() {
        return path;
    }

    public PtakLoadCacher getCacher() {
        return cacher;
    }

    public void setCacher(PtakLoadCacher cacher) {
        this.cacher = cacher;
    }

    public void handleDecodeState(int state) {
        switch (state) {
            case PtakLoadDecoder.DECODING_OK:
                PtakLoadManager.PTAK_LOAD_MANAGER.handleState(this, PtakLoadManager.TASK_FINISHED);
                break;
            default:
                PtakLoadManager.PTAK_LOAD_MANAGER.handleState(this, PtakLoadManager.TASK_FAILED);
        }
    }

    public void handleDownloadState(int state) {
        switch (state) {
            case PtakLoadDownloader.DOWNLOAD_OK:
                PtakLoadManager.PTAK_LOAD_MANAGER.handleState(this, PtakLoadManager.DOWNLOAD_COMPLETED);
                break;
            default:
                PtakLoadManager.PTAK_LOAD_MANAGER.handleState(this, PtakLoadManager.TASK_FAILED);
        }
    }

    public void handleCacheCheckState(int state) {
        switch (state) {
            case PtakLoadManager.APP_CACHE_HIT:
                PtakLoadManager.PTAK_LOAD_MANAGER.handleState(this, PtakLoadManager.DOWNLOAD_COMPLETED);
                break;
            default:
                PtakLoadManager.PTAK_LOAD_MANAGER.handleState(this, PtakLoadManager.APP_CACHE_MISS);
        }
    }

    public void recycle() {
        this.buffer = null;
        this.resultImage = null;

        if (callerView != null) {
            callerView.clear();
            callerView = null;
        }
    }
}

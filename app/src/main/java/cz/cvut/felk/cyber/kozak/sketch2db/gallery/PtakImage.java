package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.graphics.Bitmap;

/**
 * Interface for downloading images from server to the application and showing results on UI thread.
 * Created Jan Kozak
 */
public interface PtakImage {

    /**
     * File path on the server (after "http://ptak.felk.cvut.cz/G2G/data_img2/")
     * @return file path to the image
     */
    String getPath();

    /**
     * Sets path to be used when querying the image.
     * @param path
     */
    void setPath(String path);

    /**
     * Getter for wanted width of the result view.
     * @return width of the view
     */
    int getStartWidth();

    /**
     * Setter for wanted width of the result view.
     * @param startWidth requested width of the view
     */
    void setStartWidth(int startWidth);

    /**
     * Getter for wanted height of the result view.
     * @return height of the view
     */
    int getStartHeight();

    /**
     * Setter for wanted height of the result view.
     * @param startHeight requested height of the view
     */
    void setStartHeight(int startHeight);

    /**
     * Starts download task from the server.
     */
    void startDownload(boolean useOnlyFileCache, boolean useDetailed);

    /**
     * Shows result image.
     * @param image result image
     */
    void setSuccessImage(Bitmap image, int scale);

    /**
     * Handles image loading failure.
     */
    void setFailedImage();

    /**
     * Requests loading manager to stop downloading the image.
     */
    void stopDownload();

    /**
     * Bitmap with search image to be laid over result image.
     * @param overlay Bitmap with search image
     */
    void setOverlay(Bitmap overlay);

    void createSketchPosition(float xCenter, float yCenter, float scale, boolean isFlipped);

}

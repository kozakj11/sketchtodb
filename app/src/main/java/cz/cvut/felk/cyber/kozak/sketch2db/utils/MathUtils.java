package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.util.Log;

/**
 * Class holding some math functions.
 */
public final class MathUtils {

    private MathUtils() {
    }

    /**
     * Calculates squared distance of two points in 2-D space.
     * @param x1 x coordinates of the first point
     * @param y1 y coordinates of the first point
     * @param x2 x coordinates of the second point
     * @param y2 y coordinates of the second point
     * @return squared distance of the points
     */
    public static float distSq(float x1, float y1, float x2, float y2) {
        float xDiff = x1 - x2;
        float yDiff = y1 - y2;
        return xDiff * xDiff + yDiff * yDiff;
    }

    /**
     * Calculates squared distance of a point to a line in 2-D space
     * @param startX x coordinates of the start of the line
     * @param startY y coordinates of the start of the line
     * @param endX x coordinates of the end of the line
     * @param endY y coordinates of the end of the line
     * @param x x coordinates of the point to which distance is calculated
     * @param y y coordinates of the point to which distance is calculated
     * @return squared distance of the point to the line
     */
    //TODO - too many args
    public static float lineDistSq(float startX, float startY, float endX, float endY, float x, float y) {
        float a = startY - endY;
        float b = endX - startX;
        float c = - a * startX - b * startY;
        float f = (a * x + b * y + c);
        return (f * f) / (a * a + b * b);
    }

    /**
     * Calculates squared distance of a point to a line segment in 2-D space
     * @param startX x coordinates of the start of the line segment
     * @param startY y coordinates of the start of the line segment
     * @param endX x coordinates of the end of the line segment
     * @param endY y coordinates of the end of the line segment
     * @param x x coordinates of the point to which distance is calculated
     * @param y y coordinates of the point to which distance is calculated
     * @return squared distance of the point to the line segment
     */
    //TODO - too many args
    public static float segmentDistSq(float startX, float startY, float endX, float endY, float x, float y) {
        float startDist = distSq(startX, startY, x, y);
        float endDist = distSq(endX, endY, x, y);
        float nearestDist = startDist < endDist ? startDist : endDist;
        float segDist =  lineDistSq(startX, startY, endX, endY, x,y);
        return nearestDist < segDist ? nearestDist : segDist;
    }
}

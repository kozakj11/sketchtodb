package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import java.lang.ref.WeakReference;
import java.util.*;

/**
 * Created by kozack on 7. 9. 2016.
 */
public class OverlayManager {

    private Set<WeakReference<Listener>> observers = new HashSet<>();

    public interface Listener {
        void showOverlay(boolean show);
        void showRectangle(boolean show);
    }

    public void registerListener(OverlayManager.Listener listener) {
        WeakReference<Listener> reference = new WeakReference<>(listener);
        observers.add(reference);
    }

    public void notifyListeners(boolean showOverlay, boolean showRectangle) {
        for (WeakReference<Listener> ref: observers) {
            Listener listener = ref.get();
            if (listener == null) {
                observers.remove(ref);
            } else {
                listener.showOverlay(showOverlay);
                listener.showRectangle(showRectangle);
            }
        }
    }


}

package cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs;

import android.app.DialogFragment;

import java.io.File;

/**
 * Created by kozack on 5. 9. 2016.
 */
public interface FileExplorerDialogListener {

    void onPositiveReply(FileExplorerDialogs type, File file, String input);

    void onNegativeReply(FileExplorerDialogs type);

    enum FileExplorerDialogs {
        CREATE_NEW_DIRECTORY,
        OVERWRITE_FILE
    }

}

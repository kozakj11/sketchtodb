package cz.cvut.felk.cyber.kozak.sketch2db;

import android.content.Context;
import android.graphics.*;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.AppStorage;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.DrawInput;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.*;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Extended view class for drawing input.
 */
public class SketchingView extends View {

    public interface PaneVisibilityListener {

        void makeSketchingPaneVisible(boolean isVisible);

        void changeOrientation(boolean portrait);

    }

    public interface QuickSearchStarterListener {

        void startQuickSearchActivity(boolean success, File tmpFile);

    }

    public enum Mode {
        SVG_MODE, FREE_MODE
    }

    private Canvas canvas;
    private Paint paint;
    private Bitmap bitmap;
    private DrawInput drawInput;
    private Mode mode;
    private PaneVisibilityListener paneVisibilityListener;
    private QuickSearchStarterListener quickSearchListener;
    private ImageDimensionHolder holder;

    public static final String SAVED_PATH_INFO = "sketch_activity_saved_path_info";
    public static final String SAVED_HOLDER_INFO = "skecth_activity_holder_info";
    public static final String SAVED_REDO_PATH_INFO = "sketch_activity_saved_redo_path_info";

    public SketchingView(Context context) {
        super(context);
        init(context);
    }

    public SketchingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setPaneVisibilityListener(PaneVisibilityListener listener) {
        this.paneVisibilityListener = listener;
    }

    public void setQuickSearchListener(QuickSearchStarterListener listener) {
        this.quickSearchListener = listener;
    }

    private void init(Context context) {
        paint = new Paint(Paint.DITHER_FLAG);
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);
        paint.setStrokeWidth(4f);
        this.mode = Mode.FREE_MODE;

        this.drawInput = new DrawInput(null , paint, context, mode);
    }

    public void eraseAll() {
        drawInput.eraseAll();
        this.bitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        this.canvas = new Canvas(bitmap);
        drawInput.onStart(canvas, bitmap);
        invalidate();
    }

    public void redo() {
        drawInput.redo();
        invalidate();
    }

    public boolean canChangeOrientation() {
        return drawInput.isEmpty();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(bitmap, 0, 0, null);
        drawInput.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        drawInput.onStart(canvas, bitmap);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        drawInput.onTouch(event);
        invalidate();
        return true;
    }

    public void handleBundle(Bundle bundle) {
        if (bundle != null) {
            paneVisibilityListener.makeSketchingPaneVisible(false);

            int keyInt =  bundle.getInt(SketchActivity.SAVED_COLOR);
            paint.setColor(keyInt);

            Mode mode = (Mode) bundle.getSerializable(SketchActivity.SAVED_MODE);
            changeMode(mode);

            int holderKey = bundle.getInt(SAVED_HOLDER_INFO);
            this.holder = AppStorage.getHolder(holderKey);

            keyInt = bundle.getInt(SAVED_PATH_INFO);
            int keyInt2 = bundle.getInt(SAVED_REDO_PATH_INFO);

            new LoadPathsFromBundle().execute(keyInt, keyInt2);
        }
    }

    public int savePathInfos() {
        return drawInput.savePathInfos();
    }

    public int saveRedoPathInfos() {
        return drawInput.saveRedoPathInfos();
    }

    public void saveViewState(Bundle outState) {
        int key = savePathInfos();
        if (holder != null) {
            outState.putInt(SAVED_HOLDER_INFO, AppStorage.saveHolder(holder));
        }
        outState.putInt(SAVED_PATH_INFO, key);
        outState.putInt(SketchActivity.SAVED_COLOR, paint.getColor());
        outState.putSerializable(SketchActivity.SAVED_MODE, mode);

        key = saveRedoPathInfos();
        outState.putInt(SAVED_REDO_PATH_INFO, key);
    }

    public void changeMode(Mode mode) {
        if (mode != this.mode) {
            this.mode = mode;
            this.mode = mode;
            drawInput.setMode(mode, this.getContext());
        }
    }

    public void clearSketchingView() {
        // don't create new bitmap and redraw paths if there is nothing to undo
        if (drawInput.isEmpty()) {
            Log.d("SketchingView", "clearSketchingView - drawInput isEmpty.");
            return;
        }
        Log.d("SketchingView", "clearSketchingView - drawInput is not empty...");
        this.bitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        this.canvas = new Canvas(bitmap);
        drawInput.onStart(canvas, bitmap);
        drawInput.undoLast();
        invalidate();
    }

    public void changeColor(int color) {
        Paint pp = new Paint(this.paint);
        pp.setColor(color);
        this.paint = pp;
        drawInput.setPaint(paint);
    }

    public void saveImage(File file, AcceptableImageType ait, boolean quickSearch) {
        // TODO - check if we can access that from Task thread and don't need to copy it here...
        Bitmap underlying = bitmap.copy(Bitmap.Config.ARGB_8888, false);
        Map<Path, PathInfo> paths = new LinkedHashMap<>();
        if (ait == AcceptableImageType.SVG) {
            paths.putAll(drawInput.getPaths());
        }
        new SaveImageTask(underlying, paths, ait, quickSearch, file).execute(file);
    }

    private void showSaveToast(boolean success) {
        int msg = success ? R.string.image_save_success : R.string.image_save_fail;
        int length = success ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG;
        Toast.makeText(this.getContext(), msg, length).show();
    }

    public void openXml(List<PathInfo> pinfos, Integer result, ImageDimensionHolder holder) {
        if (result <= SvgReader.PARSING_OK_UNKNOWN_VALUES) {
            this.holder = holder;
            new LoadXmlFromFile(holder).execute(pinfos);
        } else {
            this.holder = null;
            paneVisibilityListener.makeSketchingPaneVisible(true);
            Toast.makeText(this.getContext(), R.string.svg_parsed_unknown_type, Toast.LENGTH_SHORT).show();
        }
    }

    public void openBitmap(Bitmap bmp, Integer result, ImageDimensionHolder holder) {
        if (result == ImageLoader.LOAD_OK) {
            this.holder = holder;
            Toast.makeText(this.getContext(), "Unsupported file format.", Toast.LENGTH_SHORT).show();
            // TODO - load bitmap
        } else {
            this.holder = null;
            paneVisibilityListener.makeSketchingPaneVisible(true);
            Toast.makeText(this.getContext(), R.string.image_loaded_unsuccessfully, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Draws paths after recreating the view.
     */
    public void drawPaths() {
        this.bitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        this.canvas = new Canvas(bitmap);
        drawInput.onStart(canvas, bitmap);
        drawInput.drawPaths();
        paneVisibilityListener.makeSketchingPaneVisible(true);
        invalidate();
    }

    private void startQuickSearchActivity(boolean success, File tmpFile) {
        quickSearchListener.startQuickSearchActivity(success, tmpFile);
    }

    private class LoadPathsFromBundle extends AsyncTask<Integer, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Integer... params) {
            drawInput.loadPathInfos(params[0]);
            drawInput.loadRedoPathInfos(params[1]);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            drawPaths();
        }
    }

    private class LoadXmlFromFile extends AsyncTask<List<PathInfo> , Integer, Boolean> {

        private final ImageDimensionHolder holder;

        private LoadXmlFromFile(ImageDimensionHolder holder) {
            this.holder = holder;
        }

        @Override
        protected Boolean doInBackground(List<PathInfo>... params) {
            List<PathInfo> pinfos = params[0];
            drawInput.loadXml(pinfos);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (holder.needsRotation()) {
                Log.d("Sketching-view-load-xml", "needs rotation");
                paneVisibilityListener.changeOrientation(holder.isImagePortrat());
                drawPaths();
            } else {
                Log.d("Sketching-view-load-xml", "doesnt need rotation");
                drawPaths();
            }
        }
    }


    private class SaveImageTask extends AsyncTask<File, Integer, Boolean> {

        private final Bitmap outerBitmap;
        private final Map<Path, PathInfo> paths;
        private final AcceptableImageType imageType;
        private final boolean quickSearch;
        private final File fl;

        private SaveImageTask(Bitmap bitmap, Map<Path, PathInfo> paths, AcceptableImageType imageType, boolean quickSearch, File fl) {
            this.outerBitmap = bitmap;
            this.paths = paths;
            this.imageType = imageType;
            this.quickSearch = quickSearch;
            this.fl = fl;
        }

        @Override
        protected Boolean doInBackground(File... params) {
            File file = params[0];
            ImageSaver saver = new ImageSaver(outerBitmap, file, paths);
            return saver.saveImage(imageType);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (quickSearch) {
                startQuickSearchActivity(aBoolean, fl);
            } else {
                showSaveToast(aBoolean);
            }
        }
    }
}

package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.graphics.Bitmap;

/**
 * Class holding a bitmap and information about its scaling (for overlays in results grid).
 * Created by kozack on 26. 9. 2016.
 */
public class ScaledBitmap {

    private Bitmap bitmap;
    private float scaling;

    /**
     * Creates an instance with given bitmap and scaling
     * @param bitmap Overlay bitmap
     * @param scaling original size / overlay bitmap size ratio
     */
    public ScaledBitmap(Bitmap bitmap, float scaling) {
        this.bitmap = bitmap;
        this.scaling = scaling;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public float getScaling() {
        return scaling;
    }

    public void setScaling(float scaling) {
        this.scaling = scaling;
    }

}

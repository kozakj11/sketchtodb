package cz.cvut.felk.cyber.kozak.sketch2db;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Main menu activity.
 */
public class MainMenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sketch_menu);
    }

    /**
     * Start sketching activity
     * @param view
     */
    public void startSketching(View view) {
        Intent intent = new Intent(this, SketchActivity.class);
        startActivity(intent);
    }

    /**
     * Start finding activity
     * @param view
     */
    public void startFinding(View view) {
        Intent intent = new Intent(this, ActivitySearch.class);
        startActivity(intent);
    }

    /**
     * Probabily only dummy placeholder
     * @param view
     */
    public void startEditing(View view) {
        //TODO - jen docasne!!!
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

}

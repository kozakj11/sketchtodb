package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.graphics.Canvas;

/**
 * Inteface for showing sketch position in the image.
 */
public interface SketchPosition {

    /**
     *  * Draw sketch onto image.
     * @param canvas Canvas to draw onto
     * @param showOverlay draw sketch image
     * @param showRect draw matching area rectangle
     */
    void drawPosition(Canvas canvas, boolean showOverlay, boolean showRect);

    void rescale(int scale);

    void setMirrored(boolean isMirrored);
}

package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.content.Context;
import android.graphics.*;
import android.view.MotionEvent;
import cz.cvut.felk.cyber.kozak.sketch2db.SketchingView;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.ImageDimensionHolder;

import java.util.*;

/**
 * Abstract class for UI draw input.
 */

public class DrawInput {

    protected Map<Path, PathInfo> paths;
    // It would be much better to work only with Map, but using a list concurrently makes it easier to find last path
    // for undo
    protected Deque<Path> pathList;

    protected Deque<List<Path>> redoPathList;
    protected Map<Path, PathInfo> redoPaths;

    protected Paint paint;
    protected Canvas canvas;

    protected Path path;

    protected int order = 0;

    private DrawModeListener listener;

    public DrawInput(Canvas canvas, Paint paint, Context context, SketchingView.Mode mode) {
        this.canvas = canvas;
        this.paint = paint;
        this.paths = new LinkedHashMap<>();
        this.path = new Path();
        this.pathList = new LinkedList<>();

        this.redoPathList = new LinkedList<>();
        this.redoPaths = new LinkedHashMap<>();

        setMode(mode, context);
    }

    public final void setMode(SketchingView.Mode mode, Context context) {
        switch(mode) {
            case FREE_MODE:
                listener = new FreeDraw(this);
                break;
            case SVG_MODE:
                listener = new PathDraw(context , this);
                break;
        }
    }

    /**
     * Forwarding of View onTouch method.
     * @param event
     */
    public void onTouch(MotionEvent event) {
        listener.onTouch(event);
    }

    /**
     * Map of drawn paths and their paints in draw order.
     * @return Map of paths and paints assigned to them in paths' draw order.
     */
    public Map<Path, PathInfo> getPaths() {
        return paths;
    };

    /**
     * Forwarding of View onDraw method.
     * @param canvas canvas to draw onto
     */
    public void onDraw(Canvas canvas) {
        canvas.drawPath(path, paint);
    }

    /**
     * Set paint used for next path.
     * @param paint
     */
    public void setPaint(Paint paint) {
        this.paint = paint;
    };

    /**
     * Forwarding of View onStart method.
     * @param canvas
     * @param bitmap
     */
    public void onStart(Canvas canvas, Bitmap bitmap) {
        this.canvas = canvas;
    }

    /**
     * Undo last action.
     *
     */
     public void undoLast() {
         //naive implementation - remove from pathMap and redraw all (is there any important performance consequence?)
         Path path = pathList.peekLast();
         if (path != null) {
             pathList.removeLast();
             PathInfo pInfo = paths.remove(path);
             Paint pnt;
             // is it safe to use paths from list or not? But it's better to have them in order...
             for (Path p: pathList) {
                 pnt = paths.get(p).getPaint();
                 canvas.drawPath(p, pnt);
             }

             List<Path> soCalledPathList = new ArrayList<>();
             soCalledPathList.add(path);
             redoPathList.addLast(soCalledPathList);
             redoPaths.put(path, pInfo);

             listener.undoLast();
         }
     }

    /**
     * Redo last undone action (if any).
     */
    public void redo() {
        List<Path> redo = redoPathList.peekLast();
        if (redo != null) {
            redoPathList.removeLast();
            for (Path redonePath: redo) {
                PathInfo pi = redoPaths.remove(redonePath);
                pathList.addLast(redonePath);
                paths.put(redonePath, pi);
                canvas.drawPath(redonePath, pi.getPaint());
            }
        }
    }

    /**
     * Indicates whether there are registered paths drawn on canvas or not
     * @return true if there are no paths drawn on canvas, false otherwise
     */
    public boolean isEmpty() {
        return paths.isEmpty();
    };

    /**
     * Saves path info to storage.
     * @return key to storage
     */
    public int savePathInfos() {
        Collection<PathInfo> col = paths.values();
        return AppStorage.savePathInfos(col);
    }

    public int saveRedoPathInfos() {
        Collection<List<PathInfo>> col = new ArrayList<>();

        for (List<Path> rList: redoPathList) {
            List<PathInfo> piList = new ArrayList<>();
            for (Path p : rList) {
                PathInfo pi = redoPaths.get(p);
                piList.add(pi);
            }
            col.add(piList);
        }

        return AppStorage.saveRedoPathsInfos(col);
    }

    public void loadPathInfos(int key){
        Collection<PathInfo> pinfos = AppStorage.getPathInfos(key);
        if (pinfos == null) {
            return;
        }
        PathInfo[] toArr = new PathInfo[pinfos.size()];
        pinfos.toArray(toArr);

        Arrays.sort(toArr, new Comparator<PathInfo>() {
            @Override
            public int compare(PathInfo lhs, PathInfo rhs) {
                return lhs.getOrder() - rhs.getOrder();
            }
        });

        loadXml(Arrays.asList(toArr));
    }

    public void loadRedoPathInfos(int key) {
        Collection<List<PathInfo>> pinfos = AppStorage.getRedoPathsInfos(key);
        if (pinfos == null) {
            return;
        }
        for (List<PathInfo> piList : pinfos) {
            List<Path> pathsLst = new ArrayList<>();
            for (PathInfo pathInfo : piList) {
                Path p = new Path();
                pathInfo.pathInfoDraw(p);

                redoPaths.put(p, pathInfo);
                pathsLst.add(p);
            }
            redoPathList.addLast(pathsLst);
        }
    }

    public void loadXml(List<PathInfo> infos) {
        this.paths = new LinkedHashMap<>();
        this.path = new Path();
        this.pathList = new LinkedList<>();

        for (PathInfo pathIfno : infos) {
            Path p = new Path();
            pathIfno.pathInfoDraw(p);

            paths.put(p, pathIfno);
            pathList.push(p);

            order = pathIfno.getOrder() + 1;
        }
    }

    public void eraseAll() {
        this.redoPaths = this.paths;
        // not best practice but will work as long as we use LinkedList as pathList
        // More general option seems to be copy Paths one by one from Deque to new List, but why?
        List<Path> list = (LinkedList<Path>) this.pathList;
        this.redoPathList.add(list);

        this.paths = new LinkedHashMap<>();
        this.path = new Path();
        this.pathList = new LinkedList<>();
    }

    public void drawPaths() {
        for (Path path : pathList) {
            PathInfo pi = paths.get(path);
            canvas.drawPath(path, pi.getPaint());
        }
    }

    public Path getPath() {
        return path;
    }

    public Paint getPaint() {
        return paint;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public Deque<Path> getPathList() {
        return pathList;
    }

    public int getOrder() {
        return order;
    }

    public void incrementOrder() {
        this.order++;
    }

    public void resetPathAndPaint() {
        this.path = new Path();
        this.paint = new Paint(this.paint);
    }

}

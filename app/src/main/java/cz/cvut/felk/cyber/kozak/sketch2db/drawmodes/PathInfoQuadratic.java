package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;

import java.util.Locale;

/**
 * Created by kozack on 10. 7. 2016.
 */
public class PathInfoQuadratic implements PathInfo {

    private PointF start;
    private PointF end;
    private PointF controlPoint;
    private int order;
    private Paint paint;

    /**
     * Creates an instance with empty fields.
     */
    public PathInfoQuadratic() {

    }

    /**
     * Creates an instance and sets its order field.
     * @param order order of referenced path in drawing
     */
    public PathInfoQuadratic(int order) {
        this.order = order;
    }

    @Override
    public PointF getControlPoint() {
        return controlPoint;
    }

    @Override
    public void setControlPoint(PointF controlPoint) {
        this.controlPoint = controlPoint;
    }

    /**
     * Returns end point of the referenced Path.
     * @return end point (might be null)
     */
    @Override
    public PointF getEnd() {
        return end;
    }

    @Override
    public void setEnd(PointF end) {
        this.end = end;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * Getter for Paint property of referenced Path.
     * @return Paint object (might be null)
     */
    @Override
    public Paint getPaint() {
        return paint;
    }

    @Override
    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    /**
     * Returns start point of the referenced Path.
     * @return start point (might be null)
     */
    @Override
    public PointF getStart() {
        return start;
    }

    @Override
    public void setStart(PointF start) {
        this.start = start;
    }

    @Override
    public void pathInfoDraw(Path src) {
        src.moveTo(start.x, start.y);
        src.quadTo(controlPoint.x, controlPoint.y, end.x, end.y);
    }

    private static final String FORMAT = "%.2f";
    private static final String HEX_FORMAT = "%06x";
    private static final String PATH_START = "<path d=\"M ";
    private static final String PATH_CENTER_Q = " Q ";
    private static final String SPACE = " ";
    private static final String STROKE = "\" stroke=\"";
    private static final String STROKE_WIDTH = "\" stroke-width=\"4px"; // doesn't matter right now
    private static final String FILL_END = "\" fill=\"none\" />\n";
    private static final String HASHTAG = "#";
    private static final int COLOR_WITHOUT_ALPHA_MASK = 0x00FFFFFF; // ARGB in Android

    @Override
    public String getSvgRepresentation() {
        StringBuilder builder = new StringBuilder();

        if (controlPoint == null) {
            // set control point to average
            float x2 = start.x + end.x, y2 = start.y + end.y;
            controlPoint = new PointF(x2 * 0.5f, y2 * 0.5f);
        }

        String s1 = String.format(Locale.US, FORMAT, start.x);

        builder.append(PATH_START);
        builder.append(s1);
        builder.append(SPACE); // <path d="M startX

        s1 = String.format(Locale.US, FORMAT, start.y);
        builder.append(s1); // <path d="M startX startY

        builder.append(PATH_CENTER_Q);
        s1 = String.format(Locale.US, FORMAT, controlPoint.x);
        builder.append(s1);
        builder.append(SPACE);

        s1 = String.format(Locale.US, FORMAT, controlPoint.y);
        builder.append(s1);
        builder.append(SPACE); // <path d="M startX startY Q controlX controlY

        s1 = String.format(Locale.US, FORMAT, end.x);
        builder.append(s1);
        builder.append(SPACE);

        s1 = String.format(Locale.US, FORMAT, end.y);
        builder.append(s1); // <path d="M startX startY Q controlX controlY endX endY

        int color = paint.getColor() & COLOR_WITHOUT_ALPHA_MASK;
        s1 = String.format(HEX_FORMAT, color);
        builder.append(STROKE);
        builder.append(HASHTAG);
        builder.append(s1); // <path d="M startX startY Q controlX controlY endX endY" stroke="hexcolor"

        builder.append(STROKE_WIDTH);
        builder.append(FILL_END);

        return builder.toString();
    }
}

package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * Class for loading a preview of selected image in first view.
 */
public abstract class ImageLoader extends AsyncTask<File, Integer, Integer> {

    // codes for non-svg image loading
    public static final int LOAD_FAILED = -1;
    public static final int LOAD_OK = 0;

    protected Rect boundaries;

    protected boolean isXml;
    protected int dispWidth, dispHeight;

    protected Bitmap bmp;
    protected List<PathInfo> pinfos;
    protected float xmlWidth, xmlHeight;

    private static final float MAX_RATIO = 0.75f;

    public ImageLoader(boolean isXml, int width, int height) {
        this.isXml = isXml;
        this.dispHeight = height;
        this.dispWidth = width;
    }

    @Override
    protected Integer doInBackground(File... params) {
        File img = params[0];
        if (isXml) {

            try (FileInputStream fis = new FileInputStream(img);
                 BufferedInputStream bis = new BufferedInputStream(fis)) {
                SvgReader reader = new SvgReader(bis);
                reader.parse();
                int state = reader.getState();
                if (state < SvgReader.PARSING_UNKNOWN_SVG && state != SvgReader.PARSING_NOT_STARTED) {
                    boundaries = reader.getBoundingBox();
                    pinfos = reader.getPathInfos();
                    xmlHeight = reader.getHeight();
                    xmlWidth = reader.getWidth();
                    doLastInBackground();
                }
                return state;
            } catch (IOException ex) {
                return SvgReader.PARSING_IO_EXCEPTION; // not really but it doesn't matter here
            }

        } else {
            // first just decode the image and check it's size (so we can scale it if it is too big)
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inJustDecodeBounds = true;
            try (FileInputStream fis = new FileInputStream(img);
                 BufferedInputStream bis = new BufferedInputStream(fis)){
                BitmapFactory.decodeStream(bis, null, opt);
            } catch (IOException ex) {
                return LOAD_FAILED;
            }

            if (opt.outHeight == 0 || opt.outWidth == 0 || isCancelled()) {
                return LOAD_FAILED;
            }

            xmlHeight = opt.outHeight;
            xmlWidth = opt.outWidth;

            opt.inJustDecodeBounds = false;
            opt.inSampleSize = calcSampleSize(opt);

            // now load image and scale it
            try (FileInputStream fis = new FileInputStream(img);
                 BufferedInputStream bis = new BufferedInputStream(fis)){
                this.bmp = BitmapFactory.decodeStream(bis, null, opt);
            } catch (IOException ex) {
                return LOAD_FAILED;
            }
            if (bmp == null) {
                return LOAD_FAILED;
            } else {
                doLastInBackground();
                return LOAD_OK;
            }
        }
    }

    @Override
    protected abstract void onPostExecute(Integer result);

    private int calcSampleSize(BitmapFactory.Options opt) {
        return PtakLoadDecoder.calculateInSampleSize(opt, dispHeight, dispWidth);
    }

    public ImageDimensionHolder getHolder() {
        return new ImageDimensionHolder(dispHeight, dispWidth, (int) xmlHeight, (int) xmlWidth);
    }

    /**
     * Called last in doInBackground if loading is successful so it's possible to do some other things in background
     * thread.
     */
    protected void doLastInBackground() {

    }

}
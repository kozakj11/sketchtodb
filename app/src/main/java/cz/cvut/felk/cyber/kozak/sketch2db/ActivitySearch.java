package cz.cvut.felk.cyber.kozak.sketch2db;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.AppStorage;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;
import cz.cvut.felk.cyber.kozak.sketch2db.gallery.*;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.*;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.NoWifiDialog;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.UseMobilDataDialog;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Activity for searching the results from a base image and also for showing the results (using ViewFlipper to
 * accommodate more views together).
 */
public class ActivitySearch extends Activity
                            implements UseMobilDataDialog.UseMobilDataDialogListener {

    private ViewFlipper flipper;
    private XmlImageView xmlView;
    private Button editButton, chooseAnother, findBackButton;
    private TextView chosenFileTextView, infoNotice;
    private Display display;
    private GridView resultsGrid;
    private PtakImageGrid fullSizeDetailImage;
    private ProgressBar progressBarResultsGrid;

    private OverlayManager manager;

    private QueryTask currentTask;

    // TODO - move to properties/user settings
    private static final String LOCALHOST = "10.0.2.2";
    private static final int LOCALHOST_PORT = 3122;
    private static final String PTAK = "http://ptak.felk.cvut.cz/G2G/data_img2/";
    private static final int RESULTS = 10;

    private List<String> listStrings = null;

    private int gridResultWidth = 0;

    // for show-case, when Matlab cannot be connected
    private static final boolean USE_HARDCODED_FILE_LINKS = false;
    private static final String[] HARDCODED_FILES_LINKS = {
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_3550.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_15215.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_1798.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_15338.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_11226.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_12752.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_2939.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_6742.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_5657.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_6209.jpg"};

    private static final boolean USE_FILES_INTERNAL = true;

    /**
     * Minimal constraint on number of resultsGrid columns
     */
    private static final int MINIMAL_RESULT_COLUMNS = 3;

    /**
     * Maximal width of resultsGrid column in px.
     */
    private static final int MAXIMAL_RESULT_WIDTH = 400;

    /**
     * Fake resource id for no message in infoNotice TextView.
     */
    private static final int NO_MESSAGE = -100;
    /**
     * Currently used resource id in infoNotice view (default - no message).
     */
    private int infoNoticeResid = NO_MESSAGE;

    /**
     * Indicates visibility of Edit button (shown only if chosen image is SVG).
     */
    private boolean editable = false;
    /**
     * File object representing selected image.
     */
    private File selectedSearchFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search);

        manager = new OverlayManager();

        // for calculating of wanted sizes
        this.display = getWindowManager().getDefaultDisplay();

        findViews();
        setGridViewParams();
        initFromBundle(savedInstanceState);
        setViews();
    }

    /**
     * Finds all views that needs to be handled programmatically in this class.
     */
    private void findViews() {
        flipper = (ViewFlipper) findViewById(R.id.search_view_flipper);
        xmlView = (XmlImageView) findViewById(R.id.search_image_preview);
        editButton = (Button) findViewById(R.id.search_image_edit_button);
        chosenFileTextView = (TextView) findViewById(R.id.search_chosen_file_name);
        chooseAnother = (Button) findViewById(R.id.search_image_change_image);
        infoNotice = (TextView) findViewById(R.id.search_image_info_text);
        resultsGrid = (GridView) findViewById(R.id.search_results_grid);
        findBackButton = (Button) findViewById(R.id.search_find_button);
        fullSizeDetailImage = (PtakImageGrid) findViewById(R.id.search_image_detail_view);
        progressBarResultsGrid = (ProgressBar) findViewById(R.id.search_results_first_progress);
    }

    /**
     * Call to update some of used views.
     */
    private void setViews() {
        setEditableButtonVisibility();
        updateChosenFileTextView();
    }

    /**
     * Starts querying from image.
     * @param view Calling view, obligatory parameter
     */
    public void findFromImage(View view) {
        if (selectedSearchFile == null) {
            Toast t = Toast.makeText(this, R.string.please_select_a_file, Toast.LENGTH_LONG);
            t.show();
        } else {
            Log.d("activitysearch", "findFromImage");
            int displayedChild = flipper.getDisplayedChild();

            switch (displayedChild) {
                case 0:
                    checkConnectivity();
                    break;
                case 1:
                    if (currentTask != null) {
                        currentTask.cancel(false);
                    }
                    findBackButton.setText(R.string.find);
                case 2:
                    fullSizeDetailImage.stopDownload();
                    flipper.setInAnimation(this, R.anim.show_from_left);
                    flipper.setOutAnimation(this, R.anim.hide_to_right);
                    flipper.showPrevious();
            }
        }
    }

    private void checkConnectivity() {
        ConnectivityChecker checker = new ConnectivityChecker(this);
        ConnectivityChecker.ConnectivityState state = checker.checkConnectivity();

        switch (state) {
            case ASK_MOBILE_DATA:
                UseMobilDataDialog ofd = new UseMobilDataDialog();
                ofd.show(getFragmentManager(), "mobil_data_dialog");
                break;
            case NOT_CONNECTED:
                NoWifiDialog nwd = new NoWifiDialog();
                nwd.show(getFragmentManager(), "no_connection");
                break;
            default:
                transitionFromFirstToSecondChild();
        }
    }

    @Override
    public void onNegativeReply(DialogFragment dialog) {
        // do nothing so far
    }

    @Override
    public void onPositiveReply(DialogFragment dialog) {
        if (flipper.getDisplayedChild() == 0 && selectedSearchFile != null) {
            transitionFromFirstToSecondChild();
        }
    }

    private void transitionFromFirstToSecondChild() {
        currentTask = new QueryTask();
        currentTask.execute(selectedSearchFile);
        findBackButton.setText(R.string.back);

        resultsGrid.setVisibility(View.GONE);
        progressBarResultsGrid.setAlpha(1.0f);
        progressBarResultsGrid.setVisibility(View.VISIBLE);

        flipper.setInAnimation(this, R.anim.show_from_right);
        flipper.setOutAnimation(this, R.anim.hide_to_left);
        flipper.showNext();
    }

    /**
     * Updates infoNotice textView with given message from resources.
     * @param message Resource id of message to be displayed
     * @param show Indicates if the view should be visible (true) or hidden (false)
     */
    private void updateInfoNotice(int message, boolean show) {
        if (message != NO_MESSAGE) {
            infoNotice.setText(message);
        }
        infoNoticeResid = message;
        if (show) {
            infoNotice.setVisibility(View.VISIBLE);
        } else {
            infoNotice.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Checks dimensions of display and sets number of columns in resultsGrid.
     */
    private void setGridViewParams() {
        Point sz = new Point();
        display.getSize(sz);
        int width = sz.x, maxWidthColumns = width / MAXIMAL_RESULT_WIDTH, cols = MINIMAL_RESULT_COLUMNS;
        if (maxWidthColumns >= MINIMAL_RESULT_COLUMNS) {
            // if it's at least same as minimal count and it's bigger at least by 1/5, then add one more column
            if (width % MAXIMAL_RESULT_WIDTH > MAXIMAL_RESULT_WIDTH / 5) {
                cols = maxWidthColumns + 1;
            } else {
                // else use the calculated number (so the column will be a little bit wider than claimed maximum)
                cols = maxWidthColumns;
            }
        }
        resultsGrid.setNumColumns(cols);
        gridResultWidth = width / cols;
    }

    /**
     * Hides/unhides Edit button below the image. This button should be visible only when SVG file is chosen.
     */
    private void setEditableButtonVisibility() {
        if (editable) {
            editButton.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.GONE);
        }
    }

    /**
     * Updates the text view with selected image filename.
     */
    private void updateChosenFileTextView() {
        if (selectedSearchFile == null) {
            chosenFileTextView.setText(R.string.choose_file);
            chosenFileTextView.setTextColor(Color.GRAY);
            chosenFileTextView.setTypeface(null, Typeface.ITALIC);
        } else {
            String filename = selectedSearchFile.getName();
            chosenFileTextView.setText(filename);
            chosenFileTextView.setTextColor(Color.BLACK);
            chosenFileTextView.setTypeface(null, Typeface.NORMAL);
        }
    }

    /**
     * Start FileExplorer activity for selecting destination where to save an image.
     * Called from third page after hitting Save button.
     * @param view Button triggering the event
     */
    public void saveDetailImage(View view) {
        Log.d("activity-search", "Save detailed image.");
        Intent intent = new Intent(this, FileExplorer.class);

        // create bundle with open request
        String bundleKey = FileExplorer.BundleKey.SAVE_FILE.getKey();
        Bundle bundle = new Bundle();
        bundle.putString(bundleKey, "save");

        bundleKey = FileExplorer.BundleKey.NAME_OF_SAVED_IMG.getKey();
        bundle.putString(bundleKey, fullSizeDetailImage.getPath());

        bundleKey = FileExplorer.BundleKey.DONT_SHOW_AIT.getKey();
        bundle.putString(bundleKey, "dont");

        intent.putExtras(bundle);

        startActivityForResult(intent, SAVE_DETAILED_IMAGE);
    }

    /**
     * Saves detailed image from third page after selecting destination in FileExplorer.
     * @param intent Intent object with path where to save the image.
     */
    private void saveDetailImageIntent(Intent intent) {
        Bundle bun = intent.getExtras();
        String savedImagePath = bun.getString(FileExplorer.DETAILED_TO_SAVE);
        Uri saveLocation = intent.getData();
        File whereToSave = new File(saveLocation.getPath());

        new DetailedImageSaver(savedImagePath).execute(whereToSave);
    }

    /**
     * Request code for selecting an image through FileExplorer activity.
     */
    private static final int IMAGE_OPEN = 1;
    private static final int SAVE_DETAILED_IMAGE = 2;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMAGE_OPEN:
                    showImagePreview(data);
                    break;
                case SAVE_DETAILED_IMAGE:
                    saveDetailImageIntent(data);
            }
        }
    }

    /**
     * Starts FileExplorer activity to select an image.
     * @param view Caller view, obligatory parameter.
     */
    public void chooseFile(View view) {
        Log.d("activity-search", "Choose file");
        Intent intent = new Intent(this, FileExplorer.class);

        // create bundle with open request
        String bundleKey = FileExplorer.BundleKey.OPEN_FILE.getKey();
        Bundle bundle = new Bundle();
        bundle.putString(bundleKey, "open");

        bundleKey = FileExplorer.BundleKey.DONT_SHOW_AIT.getKey();
        bundle.putString(bundleKey, "dont");

        // if a file is already selected, start from its directory
        if(selectedSearchFile != null) {
            String dir = selectedSearchFile.getParent();
            bundle.putString(FileExplorer.BundleKey.START_DIRECTORY.getKey(), dir);
        }

        intent.putExtras(bundle);

        startActivityForResult(intent, IMAGE_OPEN);
    }

    /**
     * Shows image preview from result of FileExplorer activity.
     * @param intent result of FileExplorer activity
     */
    private void showImagePreview(Intent intent) {
        Uri fileUri = intent.getData();
        File file = new File(fileUri.getPath());
        boolean isXml = intent.getBooleanExtra(FileExplorer.IS_SVG_BOOLEAN_EXTRA, false);
        showImagePreview(file, isXml);
    }

    /**
     * Shows image from its File object and isXml indication.
     * @param file selected image
     * @param isXml indication if is xml/svg or not
     */
    private void showImagePreview(File file, boolean isXml) {
        this.selectedSearchFile = file;
        this.updateChosenFileTextView();
        Point sz = new Point();
        display.getSize(sz);
        ImageLoader iml = new ActivitySearchImageLoader(isXml, sz.x, sz.y);
        iml.execute(file);
    }

    private void showFullSizeImage(String path) {
        fullSizeDetailImage.stopDownload();

        Point sz = new Point();
        display.getSize(sz);

        fullSizeDetailImage.setStartWidth(sz.x);
        fullSizeDetailImage.setStartHeight(sz.y);
        fullSizeDetailImage.setPath(path);

        fullSizeDetailImage.startDownload(true, true);
    }

    // bundle keys for saving instance state
    private static final String FILENAME = "activity_search_bundle_filename";
    private static final String EDITABLE = "activity_search_bundle_editable";
    private static final String NOTICE_INFO = "activity_search_bundle_notice";
    private static final String SHOWN_PAGE = "activity_search_shown_page";
    private static final String IMAGES_LIST_ID = "activity_search_images_list";
    private static final String SELECTED_IMAGE = "activity_search_selected_image_third_page";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EDITABLE, Boolean.toString(editable));
        if (selectedSearchFile != null) {
            outState.putString(FILENAME, selectedSearchFile.getAbsolutePath());
        }
        outState.putString(NOTICE_INFO, Integer.toString(infoNoticeResid));
        outState.putInt(SHOWN_PAGE, flipper.getDisplayedChild());
        outState.putString(SELECTED_IMAGE, fullSizeDetailImage.getPath());
        if (listStrings != null && !listStrings.isEmpty()) {
            //outState.putInt(IMAGES_LIST_ID, AppStorage.saveStringList(listStrings));
        }
    }

    /**
     * Restores state of the activity according to bundle.
     * @param savedInstanceState bundle with saved info
     */
    private void initFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            String edb = savedInstanceState.getString(EDITABLE);
            if (edb != null) {
                this.editable = Boolean.parseBoolean(edb);
            }
            String notice = savedInstanceState.getString(NOTICE_INFO);
            if (notice == null) {
                updateInfoNotice(NO_MESSAGE, false);
            } else {
                updateInfoNotice(Integer.parseInt(notice), true);
            }
            String filename = savedInstanceState.getString(FILENAME);
            if (filename != null) {
                this.selectedSearchFile = new File(filename);
                boolean isXml = selectedSearchFile.getName().endsWith(AcceptableImageType.SVG.getSuffix());
                showImagePreview(selectedSearchFile, isXml);
            }
            int selectedPage = savedInstanceState.getInt(SHOWN_PAGE);
            String path = savedInstanceState.getString(SELECTED_IMAGE);
            int id = savedInstanceState.getInt(IMAGES_LIST_ID);
            flipper.setDisplayedChild(selectedPage);

            //List<String> strings = AppStorage.getStringList(id);
            //listStrings = strings;

            if (selectedPage == 1 && listStrings != null) {
                showGridView();

            }
            if (selectedPage == 2) {
                fullSizeDetailImage.setPath(path);
                if (path != null) {
                    fullSizeDetailImage.startDownload(true, true);
                }
            }
        }
    }

    /**
     * Calls xmlImageView to render the SVG file.
     * @param pinfos List of PathInfos from given SVG
     * @param height height of SVG
     * @param width width of SVG
     * @param result result code of SvgReader after calling its parse method
     */
    private void displayXml(List<PathInfo> pinfos, float height, float width, Integer result) {
        int infoString = NO_MESSAGE;
        boolean showNotice = false;
        switch (result) {
            case SvgReader.PARSING_OK_UNKNOWN_VALUES:
                infoString = R.string.svg_parsed_omitting_values;
                showNotice = true;
                // and the rest can go with PARSED_SUCCESSFULLY result
            case SvgReader.PARSED_SUCCESSFULLY:
                this.editable = true;
                xmlView.setIsXml(true);
                xmlView.setPathInfos(pinfos);
                xmlView.setXml(height, width, display);
                xmlView.invalidate();
                break;
            case SvgReader.PARSING_UNKNOWN_SVG:
                this.editable = false;
                this.selectedSearchFile = null;
                infoString = R.string.svg_parsed_unknown_type;
                showNotice = true;
                break;
            default:
                this.editable = false;
                this.selectedSearchFile = null;
                infoString = R.string.image_loaded_unsuccessfully;
                showNotice = true;
                updateChosenFileTextView();
        }
        updateInfoNotice(infoString, showNotice);
        setEditableButtonVisibility();
        this.updateChosenFileTextView();
    }

    /**
     * Calls XmlImageView to draw an image.
     * @param bmp (Scaled) Bitmap representing the image.
     * @param result Result of input reading
     */
    private void displayImage(Bitmap bmp, Integer result) {
        this.editable = false;
        int infoString = NO_MESSAGE;
        boolean show = false;
        if (result == ImageLoader.LOAD_OK) {
            xmlView.setIsXml(false);
            xmlView.setImageBitmap(bmp);
            xmlView.invalidate();
        } else {
            this.selectedSearchFile = null;
            this.updateChosenFileTextView();
            show = true;
            infoString = R.string.image_loaded_unsuccessfully;
        }
        setEditableButtonVisibility();
        updateInfoNotice(infoString, show);
    }

    private static final long CROSSFADE_DURATION = 2000;

    private void showGridView() {
        if (listStrings.isEmpty()) {
            Toast.makeText(this, R.string.no_results_found, Toast.LENGTH_LONG).show();
            fadeOutView(progressBarResultsGrid);
        } else {
            resultsGrid.setAdapter(new GalleryImageAdapter(ActivitySearch.this, listStrings, gridResultWidth, manager, null));
            resultsGrid.setOnItemClickListener(new ResultImageOnClickListener(ActivitySearch.this));
            crossfade(resultsGrid, progressBarResultsGrid);
        }
    }

    private void crossfade(final View in, final View out) {
        in.setAlpha(0.0f);
        in.setVisibility(View.VISIBLE);

        in.animate().alpha(1.0f).setDuration(CROSSFADE_DURATION).setListener(null);

        fadeOutView(out);
    }

    private void fadeOutView(final View out) {
        out.animate().alpha(0.0f).setDuration(CROSSFADE_DURATION).setListener(new AnimatorListenerAdapter() {
                                                                                  @Override
                                                                                  public void onAnimationEnd(Animator animation) {
                                                                                      out.setVisibility(View.GONE);
                                                                                  }
                                                                              }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.main_menu:
                Intent intent = new Intent(ActivitySearch.this , MainMenuActivity.class);
                startActivity(intent, null);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private class DetailedImageSaver extends AsyncTask<File, Integer, Boolean> {

        private final String ptakPath;
        private Object monitor = new Object();

        private DetailedImageSaver(String ptakPath) {
            this.ptakPath = ptakPath;
        }

        @Override
        protected Boolean doInBackground(File... params) {
            File location = params[0];
            File cacheCheck = new File(getCacheDir(), ptakPath);
            if (cacheCheck.exists()) {
                return copyFile(location, cacheCheck);
            } else {
                return loadAndSaveFile(location);
            }
        }

        private boolean loadAndSaveFile(File dest) {
            // Experimental
            SavedPtakImage spi = new SavedPtakImage(ptakPath, monitor);
            synchronized(monitor) {
                try {
                    Log.d("ActivitySearch", "starting downlad and waiting...");
                    spi.startDownload(false, true);
                    monitor.wait();
                } catch (InterruptedException ex) {
                    Log.d("ActivitySearch", "loadAndSaveFile - InterruptedException");
                    spi.stopDownload();
                }
            }

            if (spi.isSuccess()) {
                Log.d("ActivitySearch", "saving in LoadNdSaveFile");
                Log.d("ActivitySearch", "dest: " + dest);
                return spi.saveFile(dest);
            } else {
                return false;
            }
        }

        private boolean copyFile(File dest, File src) {
            try (FileInputStream in = new FileInputStream(src);
                 FileOutputStream out = new FileOutputStream(dest)) {

                FileChannel inChannel = in.getChannel();
                FileChannel outChannel = out.getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);

            } catch (Exception ex) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success == null) {
                Log.d("ActivitySearch", "saveTaskPostExecute null suxess");
            }
            if (success) {
                Toast.makeText(ActivitySearch.this, R.string.image_save_success, Toast.LENGTH_SHORT);
                Log.d("ActivitySearch", "saveTaskPostExecute suxess");
            } else {
                Toast.makeText(ActivitySearch.this, R.string.image_save_fail, Toast.LENGTH_LONG);
                Log.d("ActivitySearch", "saveTaskPostExecute fail");
            }
        }
    }

    private class ActivitySearchImageLoader extends ImageLoader {

        public ActivitySearchImageLoader(boolean isXml, int width, int height) {
            super(isXml, width, height);
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (isXml) {
                displayXml(pinfos, xmlHeight, xmlWidth, result);
            } else {
                displayImage(bmp, result);
            }
        }
    }

    private class ResultImageOnClickListener implements AdapterView.OnItemClickListener {

        private final Context cont;

        private ResultImageOnClickListener(Context cont) {
            this.cont = cont;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PtakImage ptak = (PtakImage) view;
            String path = ptak.getPath();

            showFullSizeImage(path);

            flipper.setInAnimation(cont, R.anim.show_from_right);
            flipper.setOutAnimation(cont, R.anim.hide_to_left);
            flipper.showNext();
        }
    }
    /**
     *
     */
    private class QueryTask extends AsyncTask<File, Integer, List<String>> {

        private final PtakPacketBuilder builder;

        private boolean isConnectionError = true;

        private QueryTask() {
            this.builder = new PtakPacketBuilder(ActivitySearch.this);
        }

        @Override
        protected List<String> doInBackground(File... params) {
            if (!USE_HARDCODED_FILE_LINKS) {
                List<String> result = new ArrayList<>();
                List<String> input = new ArrayList<>();
                BufferedInputStream bis = null;
                BufferedOutputStream bos = null;
                try (Socket socket = new Socket()) {
                    socket.bind(null);
                    //socket.connect(new InetSocketAddress(LOCALHOST, LOCALHOST_PORT), 20000);
                    socket.connect(new InetSocketAddress("ptak.felk.cvut.cz", 3122), 20000);
                    socket.setSoTimeout(15000);
                    bos = new BufferedOutputStream(socket.getOutputStream());
                    builder.uploadImage(params[0], socket, bos);

                    bis = new BufferedInputStream(socket.getInputStream());
                    byte[] bytes = new byte[socket.getReceiveBufferSize()];
                    boolean end = false, header = true;
                    int read;
                    while (!end) {
                        Log.d("queryserver", "gonna read");
                        read = bis.read(bytes, 0, bytes.length);
                        if (read != -1) {
                            String s = builder.readInput(bytes, read, header);
                            input.add(s);
                            header = false;
                            end = isCancelled();
                        } else {
                            end = true;
                            Log.d("queryserver", "end it");
                        }
                    }

                } catch (IllegalArgumentException ex) {
                    return null;
                } catch (IOException ex) {
                    return null;
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException ex) {

                        }
                    }

                    if (bos != null) {
                        try {
                            bos.close();
                        } catch (IOException ex) {

                        }
                    }
                }
                StringBuilder stringBuilder = new StringBuilder();
                for (String str : input) {
                    stringBuilder.append(str);
                }

                try {
                    JSONObject obj = new JSONObject(stringBuilder.toString());
                    JSONArray arr = obj.getJSONArray("res_fn");
                    String link;
                    for (int i = 0; i < arr.length(); i++) {
                        link = arr.getString(i);
                        result.add(link);
                    }
                } catch (org.json.JSONException ex) {
                    isConnectionError = false;
                    return null;
                }

                return result;
            } else {
                List<String> names = Arrays.asList(HARDCODED_FILES_LINKS);
                return names;
            }
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            listStrings = strings;
            if (strings == null) {
                Toast.makeText(
                        ActivitySearch.this,
                        isConnectionError ? R.string.connection_error : R.string.query_error,
                        Toast.LENGTH_LONG
                ).show();
                ActivitySearch.this.fadeOutView(progressBarResultsGrid);
            } else {
                showGridView();
            }
        }
    }

}

package cz.cvut.felk.cyber.kozak.sketch2db.gallery.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cz.cvut.felk.cyber.kozak.sketch2db.ActivityQuickSearch;
import cz.cvut.felk.cyber.kozak.sketch2db.FileExplorer;
import cz.cvut.felk.cyber.kozak.sketch2db.R;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.AppStorage;
import cz.cvut.felk.cyber.kozak.sketch2db.gallery.*;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozack on 13. 9. 2016.
 */
public class GalleryFragment extends Fragment {

    // TODO - move to properties/user settings
    private static final String LOCALHOST = "10.0.2.2";
    private static final int LOCALHOST_PORT = 3122;
    private static final String PTAK = "http://ptak.felk.cvut.cz/G2G/data_img2/";
    private static final int RESULTS = 10;

    private static final String DEBUG_TAG = "GalleryFragment";

    private File searchFile;

    private ScaledBitmap overlay;

    private Rect overlayBoundaries;

    /**
     * Minimal constraint on number of resultsGrid columns
     */
    private static final int MINIMAL_RESULT_COLUMNS = 3;

    /**
     * Maximal width of resultsGrid column in px.
     */
    private static final int MAXIMAL_RESULT_COLUMN_WIDTH = 250;

    private static final int OVERLAY_DEFAULT_SIZE = 200;

    private List<PtakImageInfoHolder> listImageInfos = null;

    private int gridResultWidth = 0;

    private ViewFlipper flipper;
    private GridView resultsGrid;
    private PtakImageGrid fullSizeDetailImage;
    private ProgressBar progressBarResultsGrid;
    private OverlayManager manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.manager = new OverlayManager();
        Bundle args = getArguments();
        this.searchFile = (File) args.getSerializable(ActivityQuickSearch.TMP_FILE_KEY);
    }

    private void saveReferences(View view) {
        // store references to important views
        this.flipper = (ViewFlipper) view.findViewById(R.id.search_view_flipper);
        this.resultsGrid = (GridView) view.findViewById(R.id.search_results_grid);
        this.progressBarResultsGrid = (ProgressBar) view.findViewById(R.id.search_results_first_progress);
        this.fullSizeDetailImage = (PtakImageGrid) view.findViewById(R.id.search_image_detail_view);
    }

    // bundle keys for saving instance state
    public static final String SHOWN_PAGE = "activity_search_shown_page";
    public static final String IMAGES_LIST_ID = "activity_search_images_list";
    public static final String SELECTED_IMAGE = "activity_search_selected_image_third_page";
    public static final String OVERLAY_RECTANGLE_BOUNDS_W = "activity_search_overlay_boundaries_width";
    public static final String OVERLAY_RECTANGLE_BOUNDS_H = "activity_search_overlay_boundaries_height";
    private static final String SVG_SUFFIX = ".svg";


    private void init(boolean restart) {
        Bundle bundle = getArguments();
        int selectedPage = 0;
        String shownImage = null;

        if (!restart) {
            if (bundle != null) {
                Log.d(DEBUG_TAG, "init - Initing from bundle");
                selectedPage = bundle.getInt(SHOWN_PAGE); // if no found, returns 0 -> shows first page
                shownImage = bundle.getString(SELECTED_IMAGE); // if no found returns null

                int id = bundle.getInt(IMAGES_LIST_ID);
                listImageInfos = AppStorage.getStringList(id);

                int wdth = bundle.getInt(OVERLAY_RECTANGLE_BOUNDS_W);
                int hght = bundle.getInt(OVERLAY_RECTANGLE_BOUNDS_H);

                overlayBoundaries = new Rect(0, 0, wdth, hght);
            }

            if (selectedPage == 1 && shownImage != null) {
                flipper.setDisplayedChild(selectedPage);
                showFullSizeImage(shownImage);
            }
        }

        boolean isXml = searchFile.getName().toLowerCase().endsWith(SVG_SUFFIX);
        resultsGrid.setVisibility(View.GONE);
        progressBarResultsGrid.setVisibility(View.VISIBLE);
        new LoadImageBitmapTask(isXml).execute(searchFile);

    }

    /**
     *
     */
    private void init() {
        init(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.result_grid_layout, container, false);

        saveReferences(view);
        setGridViewParams();
        init();

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(DEBUG_TAG, "onSaveInstanceState called");
        outState.putInt(SHOWN_PAGE, flipper.getDisplayedChild());
        outState.putString(SELECTED_IMAGE, fullSizeDetailImage.getPath());
        if (listImageInfos != null && !listImageInfos.isEmpty()) {
            outState.putInt(IMAGES_LIST_ID, AppStorage.saveStringList(listImageInfos));
        }
        if (overlayBoundaries != null) {
            outState.putInt(OVERLAY_RECTANGLE_BOUNDS_W, overlayBoundaries.width());
            outState.putInt(OVERLAY_RECTANGLE_BOUNDS_H, overlayBoundaries.height());
        }
    }

    public void backToResults() {
        fullSizeDetailImage.stopDownload();
        flipper.setInAnimation(getActivity(), R.anim.show_from_left);
        flipper.setOutAnimation(getActivity(), R.anim.hide_to_right);
        flipper.showPrevious();
    }

    public boolean isDetailShown() {
        return flipper.getDisplayedChild() == 1;
    }

    /**
     * Checks dimensions of display and sets number of columns in resultsGrid.
     */
    private void setGridViewParams() {
        Point sz = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(sz);
        int width = sz.x, maxWidthColumns = width / MAXIMAL_RESULT_COLUMN_WIDTH, cols = MINIMAL_RESULT_COLUMNS;
        if (maxWidthColumns >= MINIMAL_RESULT_COLUMNS) {
            // if it's at least same as minimal count and it's bigger at least by 1/5, then add one more column
            if (width % MAXIMAL_RESULT_COLUMN_WIDTH > MAXIMAL_RESULT_COLUMN_WIDTH / 5) {
                cols = maxWidthColumns + 1;
            } else {
                // else use the calculated number (so the column will be a little bit wider than claimed maximum)
                cols = maxWidthColumns;
            }
        }
        resultsGrid.setNumColumns(cols);
        gridResultWidth = width / cols;
    }

    /**
     * Sets resultsGrid adapter to show images or shows Failed toast if no results are found.
     */
    private void showGridView() {
        if (listImageInfos.isEmpty()) {
            Toast.makeText(getActivity(), R.string.no_results_found, Toast.LENGTH_LONG).show();
            fadeOutView(progressBarResultsGrid);
        } else {
            // tady by se pripadne brala cast z puvodniho overlaye,
            resultsGrid.setAdapter(new GalleryImageAdapter(getActivity(), gridResultWidth,
                    listImageInfos, manager, overlay));
            resultsGrid.setOnItemClickListener(new ResultImageOnClickListener(getActivity()));
            crossfade(resultsGrid, progressBarResultsGrid);
        }
    }

    private static final int CROSSFADE_DURATION = 2000;

    /**
     * Crossfade between views. They should overlap so it looks good.
     * @param in View coming into focus
     * @param out View going out from focus
     */
    private void crossfade(final View in, final View out) {
        in.setAlpha(0.0f);
        in.setVisibility(View.VISIBLE);

        in.animate().alpha(1.0f).setDuration(CROSSFADE_DURATION).setListener(null);

        fadeOutView(out);
    }

    /**
     * Sets Animator that sets View.GONE on a view that has just faded out.
     * @param out View going out from focus
     */
    private void fadeOutView(final View out) {
        out.animate().alpha(0.0f).setDuration(CROSSFADE_DURATION).
                setListener(new AnimatorListenerAdapter() {
                                                                                  @Override
                                                                                  public void onAnimationEnd(Animator animation) {
                                                                                      out.setVisibility(View.GONE);
                                                                                  }
                                                                              }
        );
    }

    /**
     * Starts download of full-size image when a result is clicked on.
     * @param path Path of image on Ptak server
     */
    private void showFullSizeImage(String path) {
        Log.d(DEBUG_TAG, "showFullSizeImage: " + path);
        fullSizeDetailImage.stopDownload();
        fullSizeDetailImage.setOverlay(null);

        Point sz = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(sz);

        fullSizeDetailImage.setStartWidth(sz.x);
        fullSizeDetailImage.setStartHeight(sz.y);
        fullSizeDetailImage.setPath(path);

        fullSizeDetailImage.startDownload(true, true);
    }

    private static final int SAVE_DETAILED_IMAGE = 2;

    /**
     * Start FileExplorer activity for selecting destination where to save an image.
     * Called from third page after hitting Save button.
     * @param view Button triggering the event
     */
    public void saveDetailImage(View view) {
        Log.d(DEBUG_TAG, "Save detailed image.");
        Intent intent = new Intent(getActivity(), FileExplorer.class);

        // create bundle with open request
        String bundleKey = FileExplorer.BundleKey.SAVE_FILE.getKey();
        Bundle bundle = new Bundle();
        bundle.putString(bundleKey, "save");

        bundleKey = FileExplorer.BundleKey.NAME_OF_SAVED_IMG.getKey();
        bundle.putString(bundleKey, fullSizeDetailImage.getPath());

        bundleKey = FileExplorer.BundleKey.DONT_SHOW_AIT.getKey();
        bundle.putString(bundleKey, "dont");

        intent.putExtras(bundle);

        startActivityForResult(intent, SAVE_DETAILED_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case SAVE_DETAILED_IMAGE:
                    saveDetailImageIntent(data);
            }
        }
    }

    /**
     * Saves detailed image from third page after selecting destination in FileExplorer.
     * @param intent Intent object with path where to save the image.
     */
    private void saveDetailImageIntent(Intent intent) {
        Bundle bun = intent.getExtras();
        String savedImagePath = bun.getString(FileExplorer.DETAILED_TO_SAVE);
        Uri saveLocation = intent.getData();
        File whereToSave = new File(saveLocation.getPath());

        new DetailedImageSaver(savedImagePath).execute(whereToSave);
    }

    public void restartQuery() {
        progressBarResultsGrid.animate().alpha(1.0f).setDuration(CROSSFADE_DURATION).setListener(null);
        flipper.setDisplayedChild(0);
        init(true);
    }

    private class DetailedImageSaver extends AsyncTask<File, Integer, Boolean> {

        private final String ptakPath;
        private Object monitor = new Object();

        private DetailedImageSaver(String ptakPath) {
            this.ptakPath = ptakPath;
        }

        @Override
        protected Boolean doInBackground(File... params) {
            File location = params[0];
            File cacheCheck = new File(getActivity().getCacheDir(), ptakPath);
            if (cacheCheck.exists()) {
                return copyFile(location, cacheCheck);
            } else {
                return loadAndSaveFile(location);
            }
        }

        private boolean loadAndSaveFile(File dest) {
            // Experimental
            SavedPtakImage spi = new SavedPtakImage(ptakPath, monitor);
            synchronized(monitor) {
                try {
                    Log.d(DEBUG_TAG, "DetailedImageSaver/loadAndSaveFile - starting downlad and waiting...");
                    spi.startDownload(false, true);
                    monitor.wait();
                } catch (InterruptedException ex) {
                    Log.d(DEBUG_TAG, "DetailedImageSaver/loadAndSaveFile - InterruptedException");
                    spi.stopDownload();
                }
            }

            if (spi.isSuccess()) {
                Log.d(DEBUG_TAG, "DetailedImageSaver/loadAndSaveFile - saving in LoadNdSaveFile");
                Log.d(DEBUG_TAG, "DetailedImageSaver/loadAndSaveFile - dest: " + dest);
                return spi.saveFile(dest);
            } else {
                return false;
            }
        }

        private boolean copyFile(File dest, File src) {
            try (FileInputStream in = new FileInputStream(src);
                 FileOutputStream out = new FileOutputStream(dest)) {

                FileChannel inChannel = in.getChannel();
                FileChannel outChannel = out.getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);

            } catch (Exception ex) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success == null) {
                Log.d(DEBUG_TAG, "DetailedImageSaver/onPostExecute - success is null");
            }
            if (success) {
                Toast.makeText(GalleryFragment.this.getActivity(), R.string.image_save_success, Toast.LENGTH_SHORT).show();
                Log.d(DEBUG_TAG, "DetailedImageSaver/onPostExecute - success = true");
            } else {
                Toast.makeText(GalleryFragment.this.getActivity(), R.string.image_save_fail, Toast.LENGTH_LONG).show();
                Log.d(DEBUG_TAG, "DetailedImageSaver/onPostExecute - saving failed");
            }
        }
    }

    private class LoadImageBitmapTask extends ImageLoader {

        private Bitmap drawn;
        private float scale;

        private LoadImageBitmapTask(boolean isXml) {
            super(isXml, OVERLAY_DEFAULT_SIZE, OVERLAY_DEFAULT_SIZE);
        }

        @Override
        protected void onPostExecute(Integer result) {
            overlay = new ScaledBitmap(this.drawn, this.scale);
            new QueryTask().execute(searchFile);
        }

        @Override
        protected void doLastInBackground() {
            XmlBasicDrawer drawer = new XmlBasicDrawer(super.pinfos, super.xmlWidth, super.xmlHeight);
            drawer.setIsOverlayDrawing(true);
            Bitmap bmp1 = drawer.getBitmap(OVERLAY_DEFAULT_SIZE);
            this.scale = drawer.getScale();
            if (super.boundaries != null) {
                Rect ob = recalculateRect(bmp1.getWidth(), bmp1.getHeight());
                bmp1 = Bitmap.createBitmap(bmp1, ob.left, ob.top, ob.right - ob.left, ob.bottom - ob.top);
                this.drawn = bmp1.copy(bmp1.getConfig(), true);
            } else {
                this.drawn = bmp1.copy(bmp1.getConfig(), true);
            }
        }

        private Rect recalculateRect(int bmpWidth, int bmpHeight) {
            Rect sup = super.boundaries;
            int left = (int) ((sup.left - 5) * scale);
            int right = (int) ((sup.right + 5) * scale);
            int top = (int) ((sup.top - 5) * scale);
            int bottom = (int) ((sup.bottom + 5) * scale);
            return new Rect(Math.max(left, 0), Math.max(top, 0),
                    Math.min(right, bmpWidth), Math.min(bottom, bmpHeight));
        }
    }

    private class QueryTask extends AsyncTask<File, Integer, List<PtakImageInfoHolder>> {

        private final PtakPacketBuilder builder;
        private Rect boundaries = null;

        private boolean isConnectionError = true;

        private QueryTask() {
            this.builder = new PtakPacketBuilder(GalleryFragment.this.getActivity());
        }

        @Override
        protected List<PtakImageInfoHolder> doInBackground(File... params) {
            if (!USE_HARDCODED_FILE_LINKS && listImageInfos == null) {
                List<PtakImageInfoHolder> result = new ArrayList<>();
                List<String> input = new ArrayList<>();
                BufferedInputStream bis = null;
                BufferedOutputStream bos = null;
                try (Socket socket = new Socket()) {
                    socket.bind(null);
                    //socket.connect(new InetSocketAddress(LOCALHOST, LOCALHOST_PORT), 20000);
                    Log.d(DEBUG_TAG, "QueryTask/doInBackground - initiating connection to server.");
                    socket.connect(new InetSocketAddress("ptak.felk.cvut.cz", 3122), 30000); // 147.32.84.14

                    Log.d(DEBUG_TAG, "QueryTask/doInBackground - connection established.");

                    socket.setSoTimeout(30000);
                    bos = new BufferedOutputStream(socket.getOutputStream());

                    Log.d(DEBUG_TAG, "QueryTask/doInBackground - opening stream");
                    builder.uploadImage(params[0], socket, bos);

                    bis = new BufferedInputStream(socket.getInputStream());
                    byte[] bytes = new byte[socket.getReceiveBufferSize()];
                    boolean end = false, header = true;
                    int read;
                    while (!end) {
                        Log.d(DEBUG_TAG, "QueryTask/doInBackground - reading stream");
                        read = bis.read(bytes, 0, bytes.length);
                        if (read != -1) {
                            String s = builder.readInput(bytes, read, header);
                            input.add(s);
                            header = false;
                            end = isCancelled();
                        } else {
                            end = true;
                            Log.d(DEBUG_TAG, "QueryTask/doInBackground - stream was successfully read.");
                        }
                    }

                } catch (IllegalArgumentException ex) {
                    Log.e(DEBUG_TAG, "QueryTask/doInBackground - IllegalArgumentException: " + ex.getMessage());
                    return null;
                } catch (IOException ex) {
                    Log.e(DEBUG_TAG, "QueryTask/doInBackground - IOException: " + ex.getMessage());
                    return null;
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException ex) {
                            Log.e(DEBUG_TAG, "QueryTask/doInBackground - Closing input stream: " + ex.getMessage());
                        }
                    }

                    if (bos != null) {
                        try {
                            bos.close();
                        } catch (IOException ex) {
                            Log.e(DEBUG_TAG, "QueryTask/doInBackground - Closing output stream: " + ex.getMessage());
                        }
                    }
                }
                StringBuilder stringBuilder = new StringBuilder();
                for (String str : input) {
                    stringBuilder.append(str);
                }

                Log.d(DEBUG_TAG, "QueryTask/doInBackground - JSONResponse: " + stringBuilder.toString());

                try {
                    JSONObject obj = new JSONObject(stringBuilder.toString());
                    JSONArray arrResults = obj.getJSONArray("res_fn");
                    JSONArray arrCenters = obj.getJSONArray("res_center");
                    JSONArray arrScales = obj.getJSONArray("res_scales");
                    JSONArray bnds = obj.getJSONArray("res_box");
                    JSONArray arrFlips = obj.getJSONArray("res_flip");
                    String link, sHeight, sWidth, scale, flip;

                    // after change
                    JSONArray arrXcenters = arrCenters.getJSONArray(0);
                    JSONArray arrYcenters = arrCenters.getJSONArray(1);

                    /*int left = bnds.getInt(0);
                    int top = bnds.getInt(1);
                    this.boundaries = new Rect(left, top, bnds.getInt(3) - left, bnds.getInt(2) - top);*/
                    int height = (int) bnds.getDouble(0);
                    int width = (int) bnds.getDouble(1);
                    this.boundaries = new Rect(0, 0, width, height);

                    if (arrResults.length() != arrXcenters.length() || arrResults.length() != arrScales.length()
                            || arrXcenters.length() != arrYcenters.length()) {
                        Log.e(DEBUG_TAG, "QueryTask/doInBackground - JSON result arrays size don't match!");
                        isConnectionError = false;
                        return null;
                    } else {
                        for //(int i = 0; i < arrResults.length(); i++) {
                                (int i = 0; i < arrResults.length(); i++) {
                            link = arrResults.getString(i);
                            sHeight = arrXcenters.getString(i);
                            sWidth = arrYcenters.getString(i);
                            scale = arrScales.getString(i);
                            flip = arrFlips.getString(i);
                            result.add(new PtakImageInfoHolder(sHeight, sWidth, link, scale, flip));
                        }
                    }
                } catch (org.json.JSONException ex) {
                    Log.e(DEBUG_TAG, "QueryTask/doInBackground - JSONException: " + ex.getMessage());
                    isConnectionError = false;
                    return null;
                }

                return result;
            } else if (USE_HARDCODED_FILE_LINKS){
                List<PtakImageInfoHolder> holder = new ArrayList<>();
                for (String hardcoded : HARDCODED_FILES_LINKS) {
                    PtakImageInfoHolder info = new PtakImageInfoHolder("0", "0", hardcoded, HARDCODED_SCALE, "0");
                    holder.add(info);
                }
                return holder;
            } else {
                boundaries = overlayBoundaries;
                return listImageInfos;
            }
        }

        @Override
        protected void onPostExecute(List<PtakImageInfoHolder> strings) {
            overlayBoundaries = boundaries;
            listImageInfos = strings;
            if (strings == null) {
                Toast.makeText(
                        GalleryFragment.this.getActivity(),
                        isConnectionError ? R.string.connection_error : R.string.query_error,
                        Toast.LENGTH_LONG
                ).show();
                GalleryFragment.this.fadeOutView(progressBarResultsGrid);
            } else {
                showGridView();
            }
        }
    }

    // for show-case, when Matlab cannot be connected
    private static final boolean USE_HARDCODED_FILE_LINKS = false;
    private static final String[] HARDCODED_FILES_LINKS = {
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_3550.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_15215.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_1798.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_15338.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_11226.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_12752.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_2939.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_6742.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_5657.jpg",
            "jpg/ImagenetFullRes2/images/cabinet/normalcabinet/n02933112/n02933112_6209.jpg"};
    private static final String HARDCODED_SCALE = "0.8";
    private static final String HARDCODED_POSITION = "[0,0]";
    private static final String HARDCODED_BOUNDARIES = "[1,1,100,100]";

    private class ResultImageOnClickListener implements AdapterView.OnItemClickListener {

        private final Context cont;

        private ResultImageOnClickListener(Context cont) {
            this.cont = cont;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PtakImage ptak = (PtakImage) view;
            String path = ptak.getPath();

            showFullSizeImage(path);

            flipper.setInAnimation(cont, R.anim.show_from_right);
            flipper.setOutAnimation(cont, R.anim.hide_to_left);
            flipper.showNext();
        }
    }

}

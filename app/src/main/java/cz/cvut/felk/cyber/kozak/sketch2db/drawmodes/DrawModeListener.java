package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.view.MotionEvent;

/**
 * Created by kozack on 10. 7. 2016.
 */
public interface DrawModeListener {

    void onTouch(MotionEvent event);

    void undoLast();
}

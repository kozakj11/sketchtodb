package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.graphics.*;

/**
 * Created by kozack on 6. 9. 2016.
 */
public class RandomSketchPosition implements SketchPosition {

    PointF leftUpper, leftDown, rightUpper, rightDown;

    public RandomSketchPosition() {
        leftUpper = new PointF(0.25f, 0.25f);
        rightUpper = new PointF(0.75f, 0.15f);
        leftDown = new PointF(0.375f, 0.75f);
        rightDown = new PointF(0.875f, 0.875f);
    }

    @Override
    public void drawPosition(Canvas canvas, boolean showOverlay, boolean showRectangle) {
        Path path = connectPoints(canvas.getWidth(), canvas.getHeight());
        canvas.drawPath(path, createPaint());
    }

    private Path connectPoints(int width, int height) {
        Path p = new Path();
        p.moveTo(getCoordinates(width, leftUpper.x), getCoordinates(height, leftUpper.y));
        p.lineTo(getCoordinates(width,rightUpper.x), getCoordinates(height, rightUpper.y));
        p.lineTo(getCoordinates(width,rightDown.x), getCoordinates(height, rightDown.y));
        p.lineTo(getCoordinates(width,leftDown.x), getCoordinates(height, leftDown.y));
        p.lineTo(getCoordinates(width,leftUpper.x), getCoordinates(height, leftUpper.y));
        return p;
    }

    private int getCoordinates(int dimension, float relative) {
        return (int) (dimension * relative);
    }

    private Paint createPaint() {
        Paint paint = new Paint(Paint.DITHER_FLAG);
        paint.setAntiAlias(true);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);
        paint.setStrokeWidth(4f);
        return paint;
    }

    @Override
    public void rescale(int scale) {

    }

    @Override
    public void setMirrored(boolean isMirrored) {

    }
}

package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;

/**
 * Class for logging info about used paths in drawing classes.
 * Expecting that all paths are only quadratic curves (as it is in the application).
 */
public interface PathInfo {

    PointF getControlPoint();

    void setControlPoint(PointF controlPoint);

    /**
     * Returns end point of the referenced Path.
     * @return end point (might be null)
     */
    PointF getEnd();

    void setEnd(PointF end);

    int getOrder();

    void setOrder(int order);

    /**
     * Getter for Paint property of referenced Path.
     * @return Paint object (might be null)
     */
    Paint getPaint();

    void setPaint(Paint paint);

    /**
     * Retunrs start point of the referenced Path.
     * @return start point (might be null)
     */
    PointF getStart();

    void setStart(PointF start);

    String getSvgRepresentation();

    void pathInfoDraw(Path src);

}

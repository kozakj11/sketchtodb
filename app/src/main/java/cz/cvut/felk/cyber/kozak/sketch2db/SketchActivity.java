package cz.cvut.felk.cyber.kozak.sketch2db;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.*;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.NoWifiDialog;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.UseMobilDataDialog;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sketching activity.
 */
public class SketchActivity extends Activity
    implements SketchingView.PaneVisibilityListener, SketchingView.QuickSearchStarterListener,
        UseMobilDataDialog.UseMobilDataDialogListener {

    // reference to currently used sketching view
    private SketchingView sketchingView;
    // location of currently used image if it has been already saved (or opened from filesystem)
    private File saveLocation;
    private AcceptableImageType saveType;

    private ImageDimensionHolder holder;

    private static final int IMAGE_OPEN = 1;
    private static final int IMAGE_SAVE_LOCATION_KNOWN = 3; //TODO - probably unnecessary
    private static final int IMAGE_SAVE_AS_CHOOSE_LOCATION = 2;

    public static final int COLOR_BLACK = 0xFF000000;
    public static final int COLOR_BLUE = 0xFF0000FF;
    public static final int COLOR_RED = 0xFFFF0000;
    public static final int COLOR_GREEN = 0xFF00A000;
    public static final int COLOR_YELLOW = 0xFFFFFF00;
    public static final int COLOR_ORANGE = 0xFFFFA000;
    public static final int COLOR_PURPLE = 0xFFA000A0;
    public static final int COLOR_GREY = 0xFF808080;

    private Menu menu;

    public static final String SAVED_COLOR = "sketch_activity_saved_color";
    public static final String SAVED_MODE = "sketch_activity_saved_mode";

    private SketchingView.Mode tempMode;
    private int tempColor = -1;

    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sketch);

        checkBundle(savedInstanceState);

        progressBar = (ProgressBar) findViewById(R.id.sketch_progress_bar);
        progressBar.setVisibility(View.GONE);

        sketchingView = (SketchingView) findViewById(R.id.sketch_view);
        sketchingView.setPaneVisibilityListener(this);
        sketchingView.setQuickSearchListener(this);
        sketchingView.handleBundle(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sketch, menu);
        this.menu = menu;
        setIcons();
        return true;
    }

    public static final String OUT_STATE_ORIENTATION = "sketch_activity_oreintation";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(OUT_STATE_ORIENTATION, getResources().getConfiguration().orientation);
        sketchingView.saveViewState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.mode_free:
                sketchingView = (SketchingView) findViewById(R.id.sketch_view);
                sketchingView.changeMode(SketchingView.Mode.FREE_MODE);
                menu.findItem(R.id.sketch_mode).setIcon(R.drawable.icon_free_mode_32dp);
                return true;
            case R.id.mode_paths:
                sketchingView = (SketchingView) findViewById(R.id.sketch_view);
                sketchingView.changeMode(SketchingView.Mode.SVG_MODE);
                menu.findItem(R.id.sketch_mode).setIcon(R.drawable.icon_quadratic_mode_32dp);
                return true;
            case R.id.saveas:
                showFileChooserSaveAs();
                return true;
            case R.id.save:
                saveImage();
                return true;
            case R.id.open:
                showFileChooserOpenFile();
                return true;
            case R.id.quick_search:
                checkConnectivity();
                return true;
            case R.id.erase_all:
                eraseAll();
                return true;
            case R.id.redo:
                redo();
                return true;
            default:
                return checkColorAndModes(item, id);
        }
    }

    private void eraseAll() {
        sketchingView.eraseAll();
    }

    private void redo() {
        sketchingView.redo();
    }

    private void checkBundle(Bundle bundle) {
        if (bundle != null) {
            int keyInt = bundle.getInt(SketchActivity.SAVED_COLOR);
            tempColor = keyInt;

            SketchingView.Mode mode = (SketchingView.Mode) bundle.getSerializable(SketchActivity.SAVED_MODE);
            tempMode = mode;

            /*
            int or = bundle.getInt(OUT_STATE_ORIENTATION);
            // looks weird but giving just variable "or" prevents building...
            if (or != getResources().getConfiguration().orientation) {
                if (or == Configuration.ORIENTATION_LANDSCAPE) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
                } else if (or == Configuration.ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
                }
            }*/
        }
    }

    private void setIcons() {
        if (tempMode != null) {
            if (tempMode == SketchingView.Mode.FREE_MODE) {
                menu.findItem(R.id.sketch_mode).setIcon(R.drawable.icon_free_mode_32dp);
            } else {
                menu.findItem(R.id.sketch_mode).setIcon(R.drawable.icon_quadratic_mode_32dp);
            }
        } /* Originally where color chooser was
        if (tempColor != -1) {
            for (Map.Entry<Integer, Integer> entry : COLOR_SELECTION_TO_ARGB.entrySet()) {
                if (entry.getValue().equals(tempColor)) {
                    Integer key = entry.getKey();
                    Integer resId = COLOR_SELECTION_TO_IMG.get(key);
                    if (resId != null) {
                        menu.findItem(R.id.color_selection).setIcon(resId);
                    }
                    break;
                }
            }
        } */
    }

    private boolean checkColorAndModes(MenuItem item, int id) {
        // well, there must be better way to do it, but let it be for now...
        /*Integer color = COLOR_SELECTION_TO_ARGB.get(id);
        if (color == null) {
            return super.onOptionsItemSelected(item);
        }
        sketchingView = (SketchingView) findViewById(R.id.sketch_view);
        sketchingView.changeColor(color);

        MenuItem mi = menu.findItem(R.id.color_selection);
        Integer resource = COLOR_SELECTION_TO_IMG.get(id);
        if (resource != null) {
            mi.setIcon(resource);
        }*/

        return true;
    }

    private void saveImage() {
        if (saveLocation == null) {
            showFileChooserSaveAs();
        } else {
            sketchingView.saveImage(saveLocation, saveType, false);
        }
    }

    @Override
    public void makeSketchingPaneVisible(boolean isVisible) {
        if (isVisible) {
            sketchingView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        } else {
            sketchingView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNegativeReply(DialogFragment dialog) {
        // well, do nothing
    }

    @Override
    public void onPositiveReply(DialogFragment dialog) {
        startQuickSearchActivity();
    }

    private void checkConnectivity() {
        ConnectivityChecker checker = new ConnectivityChecker(this);
        ConnectivityChecker.ConnectivityState state = checker.checkConnectivity();

        switch (state) {
            case ASK_MOBILE_DATA:
                UseMobilDataDialog ofd = new UseMobilDataDialog();
                ofd.show(getFragmentManager(), "mobil_data_dialog");
                break;
            case NOT_CONNECTED:
                NoWifiDialog nwd = new NoWifiDialog();
                nwd.show(getFragmentManager(), "no_connection");
                break;
            default:
                startQuickSearchActivity();
        }
    }

    private void startQuickSearchActivity() {
        // Temporarily save current image
        File tmp = null;
        try {
            tmp = File.createTempFile("tmpimg", ".svg", getFilesDir());
        } catch (IOException ex) {
            Log.d("SketchActivity - search", ex.getMessage());
        }
        if (tmp != null) {
            sketchingView.saveImage(tmp, AcceptableImageType.SVG, true);
        } else {
            Toast.makeText(this, R.string.image_save_fail, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void startQuickSearchActivity(boolean success, File tmpFile) {
        if (success) {
            Intent intent = new Intent(this, ActivityQuickSearch.class);

            Bundle bundle = new Bundle();
            bundle.putSerializable(ActivityQuickSearch.TMP_FILE_KEY, tmpFile);

            intent.putExtras(bundle);

            startActivity(intent);
        } else {
            Toast.makeText(this, R.string.image_save_fail, Toast.LENGTH_SHORT).show();
        }
    }

    //TODO - possible merge with showFileChooserSaveAs
    private void showFileChooserOpenFile() {
        Intent intent = new Intent(this, FileExplorer.class);
        String bundleKey = FileExplorer.BundleKey.OPEN_FILE.getKey();
        Bundle bundle = new Bundle();
        bundle.putString(bundleKey, "open");

        bundleKey = FileExplorer.BundleKey.DONT_SHOW_AIT.getKey();
        bundle.putString(bundleKey, "dont");

        intent.putExtras(bundle);

        startActivityForResult(intent, IMAGE_OPEN);
    }

    private void showFileChooserSaveAs() {
        Intent intent = new Intent(this, FileExplorer.class);
        String bundleKey = FileExplorer.BundleKey.SAVE_FILE.getKey();
        Bundle bundle = new Bundle();
        bundle.putString(bundleKey, "save");
        if (saveLocation != null) {
            bundleKey = FileExplorer.BundleKey.START_DIRECTORY.getKey();
            // saveLocation should never be a directory, but for sure
            String startDir = saveLocation.isDirectory() ? saveLocation.getAbsolutePath() : saveLocation.getParent();
            bundle.putString(bundleKey, startDir);
        }

        intent.putExtras(bundle);
        startActivityForResult(intent, IMAGE_SAVE_AS_CHOOSE_LOCATION);
    }

    public void changeOrientation(boolean portrait) {
        if (portrait) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
        }
        sketchingView.drawPaths();
    }

    private void tryOpen() {
        boolean isSvg = saveType == AcceptableImageType.SVG;
        int width = sketchingView.getWidth();
        int height = sketchingView.getHeight();

        makeSketchingPaneVisible(false);

        ImageLoader iml = new SketchActivityImageLoader(isSvg, width, height);
        iml.execute(saveLocation);
    }

    private void openXml(List<PathInfo> pinfos, Integer result, ImageDimensionHolder holder) {
        sketchingView.openXml(pinfos, result, holder);
    }

    private void openBitmap(Bitmap bitmap, Integer result, ImageDimensionHolder holder) {
        sketchingView.openBitmap(bitmap, result, holder);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case IMAGE_OPEN:
                    Uri chosen = data.getData();
                    Log.d("Sketcha/onActivityRes", chosen.toString());
                    saveLocation = new File(chosen.getPath());
                    saveType = AcceptableImageType.PNG;
                    AcceptableImageType ait = (AcceptableImageType)
                            data.getSerializableExtra(FileExplorer.FILE_EXTENSION_AIT);
                    if (ait != null) {
                        saveType = ait;
                    }
                    tryOpen();
                    break;
                case IMAGE_SAVE_AS_CHOOSE_LOCATION:
                    chosen = data.getData();
                    saveLocation = new File(chosen.getPath());
                    // TODO - get it directly from activity
                    saveType = AcceptableImageType.PNG;
                    ait = (AcceptableImageType)
                            data.getSerializableExtra(FileExplorer.FILE_EXTENSION_AIT);
                    if (ait != null) {
                        saveType = ait;
                    }
                    saveImage();
            }
        } else {
            // TODO - toast?
        }
    }

    public void bottomBarUndo(View button) {
        callUndo();
    }

    public void bottomBarSave(View button) {
        saveImage();
    }

    public void bottomBarSearch(View button) {
        checkConnectivity();
    }

    public void callUndo() {
        sketchingView = (SketchingView) findViewById(R.id.sketch_view);
        Log.d("SketchActivity", "Calling undo");
        sketchingView.clearSketchingView();
    }

    private class SketchActivityImageLoader extends ImageLoader {

        private SketchActivityImageLoader(boolean isXml, int width, int height) {
            super(isXml, width, height);
        }

        @Override
        protected void onPostExecute(Integer result) {
            Log.d("Sketcha/ImageLoader", "got to postExecute...");
            if ((result == LOAD_OK && !isXml) || (result < SvgReader.PARSING_UNKNOWN_SVG && isXml)) {
                Log.d("Sketcha/ImageLoader", "LoadOK");
                ImageDimensionHolder holder = getHolder();
                if (isXml) {
                    openXml(pinfos, result, holder);
                } else {
                    openBitmap(bmp, result, holder);
                }
            } else {
                saveLocation = null;
                Toast.makeText(SketchActivity.this, R.string.image_loaded_unsuccessfully , Toast.LENGTH_LONG).show();
                makeSketchingPaneVisible(true);
            }
        }
    }



    //private static final Map<Integer, Integer> COLOR_SELECTION_TO_IMG;
    //private static final Map<Integer, Integer> COLOR_SELECTION_TO_ARGB;
    /*
    static {
        COLOR_SELECTION_TO_IMG = new HashMap<>(8);
        COLOR_SELECTION_TO_ARGB = new HashMap<>(8);

        COLOR_SELECTION_TO_IMG.put(R.id.color_black, R.drawable.icon_color_black_32dp);
        COLOR_SELECTION_TO_IMG.put(R.id.color_yellow, R.drawable.icon_color_yellow_32dp);
        COLOR_SELECTION_TO_IMG.put(R.id.color_blue, R.drawable.icon_color_blue_32dp);
        COLOR_SELECTION_TO_IMG.put(R.id.color_orange, R.drawable.icon_color_orange_32dp);
        COLOR_SELECTION_TO_IMG.put(R.id.color_red, R.drawable.icon_color_red_32dp);
        COLOR_SELECTION_TO_IMG.put(R.id.color_green, R.drawable.icon_color_green_32dp);
        COLOR_SELECTION_TO_IMG.put(R.id.color_grey, R.drawable.icon_color_grey_32dp);
        COLOR_SELECTION_TO_IMG.put(R.id.color_purple, R.drawable.icon_color_purple_32dp);

        COLOR_SELECTION_TO_ARGB.put(R.id.color_black, COLOR_BLACK);
        COLOR_SELECTION_TO_ARGB.put(R.id.color_yellow, COLOR_YELLOW);
        COLOR_SELECTION_TO_ARGB.put(R.id.color_purple, COLOR_PURPLE);
        COLOR_SELECTION_TO_ARGB.put(R.id.color_grey, COLOR_GREY);
        COLOR_SELECTION_TO_ARGB.put(R.id.color_red, COLOR_RED);
        COLOR_SELECTION_TO_ARGB.put(R.id.color_blue, COLOR_BLUE);
        COLOR_SELECTION_TO_ARGB.put(R.id.color_orange, COLOR_ORANGE);
        COLOR_SELECTION_TO_ARGB.put(R.id.color_green, COLOR_GREEN);
    }*/

}

package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.Log;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;

import java.io.*;
import java.util.*;

/**
 * Class responsible for handling image saving.
 */
public class ImageSaver {

    private Bitmap bitmap;
    private Map<Path, PathInfo> paths;
    private File location;

    /**
     * Creates instance of image saver.
     * @param bitmap Bitmap underlying in calling View
     * @param location File where to save an image
     * @param paths user drawn paths
     */
    public ImageSaver(Bitmap bitmap, File location, Map<Path, PathInfo> paths) {
        this.bitmap = bitmap;
        this.location = location;
        this.paths = paths;
    }

    /**
     * Tries to save image into file provided in constructor.
     * @param imageType file type of saved image
     * @return true if image was successfully saved, false otherwise
     */
    public boolean saveImage(AcceptableImageType imageType) {

        if (!location.getName().toLowerCase().endsWith(imageType.getSuffix())) {
            Log.e("ImageSaver-saveImage", "Difference in suffixes: " + location.getName() + ", " + imageType.getSuffix());
            return false;
        }

        if (imageType.isSvg()) {

            try (FileWriter wr = new FileWriter(location);
                 BufferedWriter bwr = new BufferedWriter(wr)) {

                saveAsSvg(bwr);

            } catch(IOException ex) {
                Log.e("ImageSaver - saveImage", ex.getMessage());
                return false;
            }

        } else {
            try (FileOutputStream fos = new FileOutputStream(location);
                 BufferedOutputStream bos = new BufferedOutputStream(fos);) {
                Bitmap.CompressFormat compress = imageType.getCompressFormat();
                bitmap.compress(compress, 90, bos); // png ignores it, for jpeg should be 90 % enough

            } catch (IOException ex) {
                Log.e("ImageSaver - saveImage", ex.getMessage());
                return false;
            }
        }
        return true;
    }

    /*public static void testXmlWrite(Map<Path, PathInfo> pathInfo) {
        Collection<PathInfo> col = pathInfo.values();
        int sz = col.size();
        PathInfo[] arr = new PathInfo[sz];
        col.toArray(arr);
        Arrays.sort(arr, new Comparator<PathInfo>() {

            @Override
            public int compare(PathInfo lhs, PathInfo rhs) {
                return lhs.getOrder() - rhs.getOrder();
            }

        });

        StringBuilder sb = new StringBuilder();

        sb.append(SVG_START);
        sb.append(500);
        sb.append(HEIGHT);
        sb.append(500);
        sb.append(END);

        for (PathInfo pinfo : arr ) {
            String pathString = buildPathString(pinfo);
            sb.append(pathString);
        }
        sb.append(SVG_END);
        Log.d("XML test", sb.toString());

    }*/

    private void saveAsSvg(BufferedWriter bwr) throws IOException {
        StringBuilder sb = new StringBuilder();
        buildStart(sb);

        Collection<PathInfo> col = paths.values();
        int sz = col.size();
        PathInfo[] arr = new PathInfo[sz];
        col.toArray(arr);

        col.toArray(arr);
        Arrays.sort(arr, new Comparator<PathInfo>() {

            @Override
            public int compare(PathInfo lhs, PathInfo rhs) {
                return lhs.getOrder() - rhs.getOrder();
            }

        });

        for (PathInfo pinfo : arr ) {
            String pathString = pinfo.getSvgRepresentation();
            sb.append(pathString);
        }
        sb.append(SVG_END);
        bwr.write(sb.toString());
    }

    private static final String SVG_START = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"";
    private static final String SVG_END = "</svg>";
    private static final String HEIGHT = "\" height=\"";
    private static final String END = "\">\n";

    private void buildStart(StringBuilder sb) {
        sb.append(SVG_START);
        sb.append(bitmap.getWidth());
        sb.append(HEIGHT);
        sb.append(bitmap.getHeight());
        sb.append(END);
    }
}

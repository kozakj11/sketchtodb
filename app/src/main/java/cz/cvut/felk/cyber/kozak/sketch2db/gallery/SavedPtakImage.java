package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.graphics.Bitmap;
import android.util.Log;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakLoadManager;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakLoadTask;

import java.io.*;

/**
 * Created by kozack on 31. 8. 2016.
 */
public class SavedPtakImage implements PtakImage {

    private Bitmap bmp, overlay;
    private String path;
    private int startWidth = START_WIDTH_CONSTANT, startHeight = START_WIDTH_CONSTANT;
    private PtakLoadTask task;

    private volatile Object monitor;
    private final static int START_WIDTH_CONSTANT = 4096; // should be big enough

    public SavedPtakImage(String path, Object monitor) {
        this.path = path;
        this.monitor = monitor;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int getStartWidth() {
        return startWidth;
    }

    @Override
    public void setStartWidth(int startWidth) {
        this.startWidth = startWidth;
    }

    /**
     * Starts download of the image from Ptak server.
     * @param useOnlyFileCache "only cache" option is never used in this implementation
     */
    @Override
    public void startDownload(boolean useOnlyFileCache, boolean useDetailed) {
        this.task = PtakLoadManager.startDownload(this, true, false, useDetailed);
    }

    @Override
    public int getStartHeight() {
        return startHeight;
    }

    @Override
    public void setStartHeight(int startHeight) {
        this.startHeight = startHeight;
    }

    @Override
    public void setOverlay(Bitmap overlay) {
        this.overlay = overlay;
    }

    @Override
    public void setSuccessImage(Bitmap image, int scale) {
        Log.d("SavedptakImage", "Suxess");
        this.bmp = image;
        synchronized(monitor) {
            monitor.notify();
            Log.d("SavedptakImage", "Notified");
        }
    }

    @Override
    public void setFailedImage() {
        this.bmp = null;
        Log.d("SavedptakImage", "FailedImage");
        synchronized(monitor) {
            monitor.notify();
            Log.d("SavedptakImage", "Notify from failed.");
        }
    }

    @Override
    public void stopDownload() {
        if (this.task != null) {
            PtakLoadManager.removeDownload(task, path);
        }
    }

    public boolean isSuccess() {
        return bmp != null;
    }

    public boolean saveFile(File dest) {
        byte[] bytes = task.getBuffer();
        Log.d("SavedptakImage", "SaveFile");
        Log.d("SavedptakImage", "dest: " + dest.getAbsolutePath());
        if (bytes == null) {
            Log.d("SavedptakImage", "saveFile null bytes");
            return false;
        } else {
            try (OutputStream os = new FileOutputStream(dest)) {
                os.write(bytes);
            } catch (Exception ex) {
                Log.d("SavedptakImage", "saveFile exception");
                return false;
            }
            Log.d("SavedptakImage", "saveFile suxess");
            return true;
        }
    }

    @Override
    public void createSketchPosition(float xCenter, float yCenter, float scale, boolean isFlipped) {

    }
}

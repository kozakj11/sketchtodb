package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

/**
 * Class holding info about one result (result image, query image and its position in result image).
 */
public class ResultsImage {

    private Bitmap resultImage;
    private Bitmap sketch;
    //private SketchPosition sketchPosition;

    public ResultsImage(Bitmap resultImage, Bitmap sketch, SketchPosition sketchPosition) {
        this.resultImage = resultImage;
        //this.sketchPosition = sketchPosition;
        this.sketch = prepareSketch(sketch);
    }

    public Bitmap getResultImage() {
        return resultImage;
    }

    public Bitmap getSketch() {
        return sketch;
    }

    private Bitmap prepareSketch(Bitmap sketch) {
        float relWidth = 1.0f, relHeight = 1.0f;
        if (sketch.getWidth() != 0.0f && sketch.getHeight() != 0.0f) { // if any is zero then scale doesn't matter
        }
        Log.d("prepareSketch", "relwidth: " + relWidth + ", relheight: " + relHeight);

        Matrix matrix = new Matrix();
        //matrix.postTranslate(sketchPosition.getLeft() * resultImage.getxCenter(), sketchPosition.getTop() * resultImage.getxCenter());
        matrix.postScale(relWidth, relHeight);

        return Bitmap.createBitmap(sketch, 0, 0, sketch.getWidth(), sketch.getHeight(), matrix, true);
    }
}

package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kozack on 10. 7. 2016.
 */
public class PathInfoFree implements PathInfo {

    private List<PathInfo> subbies;
    private PathInfo current;
    private int order;

    public PathInfoFree() {
        this.subbies = new ArrayList<>();
    }

    public PathInfoFree(int order) {
        this.subbies = new ArrayList<>();
        this.order = order;
    }

    public void insertNew(PathInfo pinfo) {
        this.current = pinfo;
        subbies.add(pinfo);
    }

    @Override
    public PointF getControlPoint() {
        if (current != null) {
            return current.getControlPoint();
        } else {
            return null;
        }
    }

    @Override
    public void setControlPoint(PointF controlPoint) {
        if (current == null) {
            this.current = new PathInfoQuadratic();
        }
        current.setControlPoint(controlPoint);
    }

    @Override
    public PointF getEnd() {
        if (current != null) {
            return current.getEnd();
        } else {
            return null;
        }
    }

    @Override
    public void setEnd(PointF end) {
        if (current == null) {
            this.current = new PathInfoQuadratic();
        }
        current.setEnd(end);
    }

    @Override
    public int getOrder() {
        return this.order;
    }

    @Override
    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public Paint getPaint() {
        if (current != null) {
            return current.getPaint();
        } else {
            return null;
        }
    }

    @Override
    public void setPaint(Paint paint) {
        if (current == null) {
            this.current = new PathInfoQuadratic();
        }
        current.setPaint(paint);
    }

    @Override
    public PointF getStart() {
        if (current != null) {
            return current.getStart();
        } else {
            return null;
        }
    }

    @Override
    public void setStart(PointF start) {
        if (current == null) {
            this.current = new PathInfoQuadratic();
        }
        current.setStart(start);
    }

    @Override
    public String getSvgRepresentation() {
        StringBuilder sb = new StringBuilder();
        for (PathInfo pinfo : subbies) {
            sb.append(pinfo.getSvgRepresentation());
        }
        return sb.toString();
    }

    @Override
    public void pathInfoDraw(Path src) {
        for (PathInfo pinfo: subbies) {
            pinfo.pathInfoDraw(src);
        }
    }
}

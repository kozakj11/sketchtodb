package cz.cvut.felk.cyber.kozak.sketch2db;

/**
 * Created by kozack on 17. 7. 2016.
 */
public class SketchAndFind extends android.app.Application {

    private static SketchAndFind instance;

    public SketchAndFind() {
        instance = this;
    }

    public static SketchAndFind getContext() {
        return instance;
    }

}

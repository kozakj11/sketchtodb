package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakLoadManager;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakLoadTask;

import java.lang.ref.WeakReference;

/**
 * Created by kozack on 7. 9. 2016.
 */
public class PtakImageGridOverlay extends ViewGroup implements PtakImage, OverlayManager.Listener {

    private String path;
    private PtakLoadTask task;
    private int startWidth = -1, startHeight = -1;
    private SketchPosition position;

    private WeakReference<ImageView> imageView;
    private WeakReference<ImageView> overlayView;
    private WeakReference<ProgressBar> progressBar;
    private Bitmap overlay = null;
    private boolean showOverlay = true;

    public PtakImageGridOverlay(Context context) {
        super(context);
        init();
    }

    public PtakImageGridOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PtakImageGridOverlay(Context context, String path) {
        super(context);
        this.path = path;
        init();
    }

    public PtakImageGridOverlay(Context context, AttributeSet attrs, String path) {
        super(context, attrs);
        this.path = path;
        init();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int count = getChildCount();

        // These are the far left and right edges in which we are performing layout.
        int leftPos = getPaddingLeft();
        int rightPos = right - left - getPaddingRight();

        // These are the top and bottom edges in which we are performing layout.
        final int parentTop = getPaddingTop();
        final int parentBottom = bottom - top - getPaddingBottom();

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                child.layout(leftPos, parentTop, rightPos, parentBottom);
            }
        }

    }

    @Override
    protected void onDetachedFromWindow() {
        Log.d("PtakImageGrid", "onDetachedToWindow called");

        stopDownload();

        Log.d("PtakImageGrid", "stopDownload");

        if (imageView != null) {
            if (imageView.get() != null) {
                removeViewAt(1);
            }
            imageView.clear();
            imageView = null;
        }

        Log.d("PtakImageGrid", "imageView");

        if (progressBar != null) {
            if (progressBar.get() != null) {
                removeViewAt(0);
            }
            progressBar.clear();
            progressBar = null;
        }

        if (overlayView != null) {
            if (overlayView.get() != null) {
                removeViewAt(0);
            }
            overlayView.clear();
            overlayView = null;
        }

        Log.d("PtakImageGrid", "progressBar and now lets call super!");

        // trick
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException ex) {
            Log.d("PtakImageGrid", "IllegalArgEx: " + ex.getMessage());
        }
    }

    public ImageView getImageView() {
        checkImageView();
        return imageView.get();
    }

    public ImageView getOverlayView() {
        checkOverlayView();
        return overlayView.get();
    }

    private void initCheck() {
        checkProgressBar();
        checkImageView();
        checkOverlayView();
    }

    private void init() {
        if (this.progressBar == null) {
            ProgressBar pb = new ProgressBar(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            pb.setLayoutParams(lp);
            pb.setIndeterminate(true);

            this.progressBar = new WeakReference<>(pb);
            addView(pb, 0);
        }


        if (this.imageView == null) {
            ImageView iv = new ImageView(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            iv.setLayoutParams(lp);

            this.imageView = new WeakReference<>(iv);
            addView(iv, 1);
        }

        if (this.overlayView == null) {
            ImageView iv = new ImageView(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            iv.setLayoutParams(lp);

            this.overlayView = new WeakReference<>(iv);
            addView(iv, 2);
        }
    }

    private void checkProgressBar() {
        if (this.progressBar == null || progressBar.get() == null) {
            removeViewAt(0);
            ProgressBar pb = new ProgressBar(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            pb.setLayoutParams(lp);
            pb.setIndeterminate(true);

            this.progressBar = new WeakReference<>(pb);
            addView(pb, 0);
        }
    }

    private void checkImageView() {
        if (this.imageView == null || imageView.get() == null) {
            removeViewAt(1);
            ImageView iv = new ImageView(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            iv.setLayoutParams(lp);

            this.imageView = new WeakReference<>(iv);
            addView(iv, 1);
        }
    }

    private void checkOverlayView() {
        if (this.imageView == null || imageView.get() == null) {
            removeViewAt(2);
            ImageView iv = new ImageView(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            iv.setLayoutParams(lp);

            this.imageView = new WeakReference<>(iv);
            addView(iv, 2);
        }
    }

    @Override
    protected FrameLayout.LayoutParams generateDefaultLayoutParams() {
        return new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    /**
     * Switches between loading circle and showing image.
     * @param isProgressBarVisible true - shows progress bar, hides image; false - vice versa
     */
    public void progressBarVisible(boolean isProgressBarVisible) {
        initCheck();
        ProgressBar pb = progressBar.get();
        ImageView im = imageView.get();
        ImageView over = overlayView.get();
        if (pb != null) {
            if (isProgressBarVisible) {
                pb.setVisibility(View.VISIBLE);
            } else {
                pb.setVisibility(View.GONE);
            }
        }
        if (im != null) {
            if (isProgressBarVisible) {
                im.setVisibility(View.GONE);
            } else {
                im.setVisibility(View.VISIBLE);
            }
        }
        if (over != null) {
            if (isProgressBarVisible) {
                over.setVisibility(View.GONE);
            } else if (showOverlay) {
                over.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int getStartWidth() {
        return startWidth;
    }

    @Override
    public void setStartWidth(int startWidth) {
        this.startWidth = startWidth;
    }

    @Override
    public void startDownload(boolean useOnlyFileCache, boolean useDetailed) {
        this.task = PtakLoadManager.startDownload(this, true, useOnlyFileCache, useDetailed);
        this.showOverlay = false;
        progressBarVisible(true);
        Log.d("PtakImage - start", path);
    }

    @Override
    public void setSuccessImage(Bitmap image, int scale) {
        initCheck();
        if (imageView != null && imageView.get() != null) {
            drawPosition(image);
            imageView.get().setImageBitmap(image);
        }
        progressBarVisible(false);
    }

    @Override
    public void showOverlay(boolean show) {
        showOverlay = show;
        if (overlayView != null && overlayView.get() != null) {
            View view = overlayView.get();
            if (show && view.getVisibility() != View.VISIBLE) {
                view.setVisibility(View.VISIBLE);
            }
            if (!show && view.getVisibility() != View.GONE) {
                view.setVisibility(View.GONE);
            }
        }
    }

    private void drawPosition(Bitmap image) {
        if (overlayView != null && overlayView.get() != null && image != null) {
            Bitmap bmp = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
            overlayView.get().setImageBitmap(bmp);
            position.drawPosition(new Canvas(bmp), true, false);
        }
    }

    @Override
    public void setFailedImage() {
        //initCheck();
        progressBarVisible(false);
        Log.e("PtakImage - failed", path);
    }

    @Override
    public int getStartHeight() {
        return startHeight;
    }

    @Override
    public void setStartHeight(int startHeight) {
        this.startHeight = startHeight;
    }

    @Override
    public void stopDownload() {
        PtakLoadManager.removeDownload(task, path);
    }

    @Override
    public void setOverlay(Bitmap overlay) {
        this.overlay = overlay;
    }

    @Override
    public void showRectangle(boolean show) {

    }

    @Override
    public void createSketchPosition(float xCenter, float yCenter, float scale, boolean isFlipped) {

    }
}

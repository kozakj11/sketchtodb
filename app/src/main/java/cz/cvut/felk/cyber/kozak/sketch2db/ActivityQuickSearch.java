package cz.cvut.felk.cyber.kozak.sketch2db;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import cz.cvut.felk.cyber.kozak.sketch2db.gallery.fragments.GalleryFragment;

import java.io.File;

/**
 * Created by kozack on 14. 9. 2016.
 */
public class ActivityQuickSearch extends Activity {

    private File tmpFile;

    public static final String TMP_FILE_KEY = "cz.cvut.felk.cyber.kozak.sketch2db.ActivityQuickSearch.TMP_FILE_KEY";
    private static final String FRAGMENT_TAG = "Gallery_fragment";
    private static final String SAVE_FRAGMENT_TAG = "Save_fragment_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_quick_search);

        Fragment fragment;
        Bundle bundle = new Bundle();
        Log.d("ActivityQuickSearch", "onCreate called");

        if (savedInstanceState != null) {
            Log.d("ActivityQuickSearch", "savedInstanceState is not null");
            tmpFile = (File) savedInstanceState.getSerializable(TMP_FILE_KEY);

            int tmpInt = savedInstanceState.getInt(GalleryFragment.IMAGES_LIST_ID);
            bundle.putInt(GalleryFragment.IMAGES_LIST_ID, tmpInt);

            tmpInt = savedInstanceState.getInt(GalleryFragment.SHOWN_PAGE);
            bundle.putInt(GalleryFragment.SHOWN_PAGE, tmpInt);

            String tmpString = savedInstanceState.getString(GalleryFragment.SELECTED_IMAGE);
            bundle.putString(GalleryFragment.SELECTED_IMAGE, tmpString);

            tmpInt = savedInstanceState.getInt(GalleryFragment.OVERLAY_RECTANGLE_BOUNDS_H);
            bundle.putInt(GalleryFragment.OVERLAY_RECTANGLE_BOUNDS_H, tmpInt);

            tmpInt = savedInstanceState.getInt(GalleryFragment.OVERLAY_RECTANGLE_BOUNDS_W);
            bundle.putInt(GalleryFragment.OVERLAY_RECTANGLE_BOUNDS_W, tmpInt);
        } else {
            Log.d("ActivityQuickSearch", "savedInstanceState is null");
            tmpFile = (File) getIntent().getSerializableExtra(TMP_FILE_KEY);
        }

        tmpFile = (File) getIntent().getSerializableExtra(TMP_FILE_KEY);
        bundle.putSerializable(TMP_FILE_KEY, tmpFile);

        fragment = new GalleryFragment();
        fragment.setArguments(bundle);

        FragmentTransaction tran = getFragmentManager().beginTransaction();
        tran.add(R.id.quick_search_fragment_layout , fragment, FRAGMENT_TAG);
        tran.commit();

    }

    public void saveDetailImage(View view) {
        GalleryFragment frag = (GalleryFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        frag.saveDetailImage(view);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(TMP_FILE_KEY, tmpFile);
        Log.d("ActivityQuickSearch", "onSaveInstanceState called");

        GalleryFragment frag = (GalleryFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        frag.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Fragment frag = getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (frag == null) {
            tmpFile.delete();
            super.onBackPressed();
        } else {
            GalleryFragment gFrag = (GalleryFragment) frag;
            if (gFrag.isDetailShown()) {
                gFrag.backToResults();
            } else {
                tmpFile.delete();
                super.onBackPressed();
            }
        }
    }

    public void backToResults(View view) {
        GalleryFragment frag = (GalleryFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        frag.backToResults();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_quick_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_search_refresh:
                GalleryFragment frag = (GalleryFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
                frag.restartQuery();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

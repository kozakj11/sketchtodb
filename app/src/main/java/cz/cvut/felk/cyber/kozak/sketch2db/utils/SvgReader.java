package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.graphics.*;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Xml;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfoCircle;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfoQuadratic;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Class for reading XML/SVG files and extracting information from it so the application can draw parsed image.
 */
public class SvgReader {

    /**
     * Parsing of SVG hasn't been started yet (call read() method to start parsing).
     */
    public static final int PARSING_NOT_STARTED = 0;

    /**
     * SVG file was successfully parsed.
     */
    public static final int PARSED_SUCCESSFULLY = 1;

    /**
     * Error encountered while parsing the file.
     */
    public static final int PARSING_ERROR = 4;

    /**
     * SVG file was parsed successfully and application can draw it, however some values or tags were skipped.
     */
    public static final int PARSING_OK_UNKNOWN_VALUES = 2;

    /**
     * SVG file is (probably) valid, but the application can't handle it (e. g. cubic curves are specified...)
     */
    public static final int PARSING_UNKNOWN_SVG = 3;

    public static final int PARSING_IO_EXCEPTION = 5;

    private static final String SVG_TAG = "svg";
    private static final String PATH_TAG = "path";
    private static final String CIRCLE_TAG = "circle";

    private final InputStream is;
    private final XmlPullParser parser;
    private final List<PathInfo> pathInfos;
    private float width = -1.0f;
    private float height = -1.0f;
    private int state = PARSING_NOT_STARTED;

    // Values of path boundaries
    private float leftBound = Float.MAX_VALUE, bottomBound = 0.0f;
    private float rightBound = 0.0f, topBound = Float.MAX_VALUE;

    public SvgReader(InputStream is) {
        this.is = is;
        this.parser = Xml.newPullParser();
        this.pathInfos = new ArrayList<>();
    }

    /**
     * Parses provided InputStream and extracts information about image's height, width and used paths.
     * @throws IOException if IOException is thrown when parsing the stream
     */
    public void parse() throws IOException {
        try {
            parser.setInput(is, null);

            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, null, SVG_TAG);

            // Get width and height
            readStartSvgTag();

            int order = 0;

            while (parser.next() != XmlPullParser.END_TAG) {
                if (this.state >= PARSING_UNKNOWN_SVG) {
                    break;
                }

                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }

                String tagName = parser.getName();
                Log.d("svgr - parse", tagName);

                if (tagName.equalsIgnoreCase(PATH_TAG)) {
                    Log.d("svgr - parse", "path-tag");
                    PathInfo pi = readPathInfo();
                    pi.setOrder(order++);
                    this.pathInfos.add(pi);
                } else if (tagName.equalsIgnoreCase(CIRCLE_TAG)) {
                    Log.d("svgr - parse", "circle-tag");
                    PathInfo pi = readCircleInfo();
                    pi.setOrder(order++);
                    this.pathInfos.add(pi);
                } else {
                    skip();
                    Log.d("svgr - parse", "skip");
                }

            }
            Log.d("svgr - parse", "ende");
            this.setState(PARSED_SUCCESSFULLY);
        } catch (XmlPullParserException ex) {
            this.setState(PARSING_ERROR);
        } catch (IOException ex) {
            this.setState(PARSING_IO_EXCEPTION);
            throw ex;
        }
    }

    private static final String HEIGHT = "height";
    private static final String WIDTH = "width";
    private static final String XMLNS = "xmlns";
    private static final String VERSION = "version";

    private void readStartSvgTag() {
        int count = parser.getAttributeCount();
        boolean heightSet = false, widthSet = false;
        String attrName;
        for (int i = 0; i < count ; i++) {
            attrName = parser.getAttributeName(i);
            Log.d("svgr - readstart", attrName);
            if (attrName.equalsIgnoreCase(HEIGHT)) {
                String s = parser.getAttributeValue(i).toLowerCase();
                Log.d("svgr - readstart", "height" + s);
                heightSet = isParamAcceptable(s, true);
            } else if (attrName.equalsIgnoreCase(WIDTH)) {
                String s = parser.getAttributeValue(i);
                Log.d("svgr - readstart", "width " + s);
                widthSet = isParamAcceptable(s, false);
            } else if (!attrName.equalsIgnoreCase(XMLNS) && !attrName.equalsIgnoreCase(VERSION)) {
                Log.d("svgr - readStart", "unknown");
                setState(PARSING_OK_UNKNOWN_VALUES);
            }
        }
        if (!heightSet) {
            Log.d("svrg - readStart", "height not set");
            setState(PARSING_OK_UNKNOWN_VALUES);
        }
        if (!widthSet) {
            Log.d("svrg - readStart", "width not set");
            setState(PARSING_OK_UNKNOWN_VALUES);
        }
    }

    /**
     * Get bounding box of image.
     * @return bounding box if PARSED_SUCCESFULLY flag is set, null otherwise
     */
    public Rect getBoundingBox() {
        if (state != PARSED_SUCCESSFULLY) {
            return null;
        }
        int left = (int) (leftBound);
        int right = (int) (rightBound + 1.0f);
        int top = (int) topBound;
        int bottom = (int) (bottomBound + 1.0f);
        return new Rect(left > 0 ? left : 0, top > 0 ? top : 0, right < width ? right : (int) width,
                bottom < height ? bottom : (int) height);
    }

    // This is probably not in 100 % compliance with SVG definitions, but it works fine for basic checks.
    private static final String DIMENSION_ACCEPTABLE_REGEX = "^[0-9]+([ ]*|.[0-9]*[ ]*)(px|pc|pt|em|ex|cm|mm|in|%|)$";

    private boolean isParamAcceptable(String s, boolean isHeight) {
        if (s.isEmpty()) {
            return false;
        }

        if (!Pattern.matches(DIMENSION_ACCEPTABLE_REGEX, s)) {
            return false;
        }

        float dim = parseXmlFloat(s);

        if (isHeight) {
            this.height = dim;
        } else {
            this.width = dim;
        }
        return true;
    }

    /**
     * Parse string to float, parses only first number in the string (before any spaces), doesn't throw exceptions.
     * Strings like "100.52 cm" are valid.
     * @param in string to parse
     * @return float value of the string (without considering units)
     */
    private float parseXmlFloat(String in) {
        char[] byteArray = in.toCharArray();
        float dim = 0;
        int cnt = 0;
        for (int i = 0; i < byteArray.length; i++) {
            char c = byteArray[i];
            if (c > 47 && c < 58) {
                dim = 10 * dim + (c - 48);
                cnt++;
            } else {
                break;
            }
        }
        if (cnt < byteArray.length && byteArray[cnt] == 46) { //decimal point
            float scale = 10.0f;
            for (int i = cnt + 1; i < byteArray.length; i++) {
                char c = byteArray[i];
                if (c > 47 && c < 58) {
                    dim = dim + (c - 48) / scale;
                    scale *= 10.0f;
                } else {
                    break;
                }
            }
        }
        return dim;
    }

    private final static String DEFINITION = "d";
    private final static String STROKE_WIDTH = "stroke-width";
    private final static String STROKE = "stroke";
    private final static String FILL="fill";

    private final static String STYLE="style", CX = "cx", CY = "cy", RADIUS = "r";

    private PathInfo readCircleInfo() throws XmlPullParserException, IOException {
        PathInfoCircle cir = new PathInfoCircle();

        float cx = -1.0f, cy = -1.0f, radius = 1.0f;

        int count = parser.getAttributeCount();
        String attr, name;

        for (int i = 0; i < count; i++) {
            name = parser.getAttributeName(i);
            Log.d("readCircle", name);
            attr = parser.getAttributeValue(i);
            if (name.equalsIgnoreCase(CX)) {
                if (canTryParse(attr)) {
                    cx = parseXmlFloat(attr);
                } else {
                    throw new XmlPullParserException("Definition of circle's cx is wrong.");
                }
            } else if (name.equalsIgnoreCase(CY)) {
                if (canTryParse(attr)) {
                    cy = parseXmlFloat(attr);
                } else {
                    throw new XmlPullParserException("Definition of circle's cy is wrong.");
                }
            } else if (name.equalsIgnoreCase(RADIUS)) {
                if (canTryParse(attr)) {
                    radius = parseXmlFloat(attr);
                } else {
                    throw new XmlPullParserException("Definition of circle's radius is wrong.");
                }
            } else if (name.equalsIgnoreCase(STYLE)) {
                parseCircleStyle(attr, cir);
            } else {
                setState(PARSING_OK_UNKNOWN_VALUES);
                Log.d("readCircleInfo", "unknown_values");
            }
        }

        if (parser.next() != XmlPullParser.END_TAG ) {
            Log.d("readCircleInfo", "throwing error, not end_tag!");
            setState(PARSING_ERROR);
        }

        cir.setCenter(new PointF(cx, cy));
        cir.setRadius(radius);

        checkCircleForBoundaries(cx, cy, radius, cir.getPaint());

        return cir;
    }

    private void checkCircleForBoundaries(float cx, float cy, float radius, Paint p) {
        float width = 1.0f;
        if (p != null) {
            width = p.getStrokeWidth() * 0.5f;
        }
        float left, right, top, down;
        left = cx - radius * 0.5f + width;
        right = cx + radius * 0.5f - width;
        top = cy - radius * 0.5f - width;
        down = cy + radius * 0.5f + width;

        this.leftBound = Math.min(left, this.leftBound);
        this.rightBound = Math.max(right, this.rightBound);
        this.bottomBound = Math.max(down, this.bottomBound);
        this.topBound = Math.min(top, this.topBound);
    }

    private void parseCircleStyle(String val, PathInfoCircle pi) throws XmlPullParserException {
        String[] delims = val.split(";");

        Paint paint = new Paint(Paint.DITHER_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);

        for (String part : delims) {
            String[] del2 = part.split(":");
            if (del2.length != 2) {
                throw new XmlPullParserException("Circle's style is invalid");
            }
            String attName = del2[0];
            String attVal = del2[1];
            if (attName.equalsIgnoreCase(STROKE_WIDTH)) {
                if (canTryParse(attVal)) {
                    float width = parseXmlFloat(attVal);
                    paint.setStrokeWidth(width);
                }
            } else if (attName.equalsIgnoreCase(STROKE)) {
                int color = parseStroke(attVal);
                paint.setColor(color);
            } else if (attName.equalsIgnoreCase(FILL)) {
                if (!attVal.equalsIgnoreCase("none")) {
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);
                }
            } else {
                setState(PARSING_OK_UNKNOWN_VALUES);
                Log.d("parseCircleState", "unknown attName " + attName);
            }
        }

        pi.setPaint(paint);
    }

    private boolean canTryParse(String s1) {
        return Pattern.matches(IS_NUM, s1);
    }

    private PathInfo readPathInfo() throws XmlPullParserException, IOException {
        PathInfo pi = new PathInfoQuadratic();
        Paint paint = new Paint(Paint.DITHER_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);
        pi.setPaint(paint);

        int count = parser.getAttributeCount();
        String attr;
        String name;

        for (int i = 0; i < count; i++) {
            name = parser.getAttributeName(i).toLowerCase();
            Log.d("readPathInfo", name);
            attr = parser.getAttributeValue(i);
            if (name.equalsIgnoreCase(DEFINITION)) {
                parseDefinition(attr, pi);
            } else if (name.equalsIgnoreCase(STROKE)) {
                int color = parseStroke(attr);
                pi.getPaint().setColor(color);
            } else if (name.equalsIgnoreCase(STROKE_WIDTH)) {
                float width = parseStrokeWidth(attr);
                pi.getPaint().setStrokeWidth(width);
            } else if (name.equalsIgnoreCase(FILL)) {
                checkFill(attr);
            } else {
                setState(PARSING_OK_UNKNOWN_VALUES);
                Log.d("readPathInfo", "unknown_values");
            }
        }
        if (parser.next() != XmlPullParser.END_TAG ) {
            Log.d("readPathInfo", "throwing error, not end_tag!");
            setState(PARSING_ERROR);
        }
        return pi;
    }

    private static final String CURVE_REGEX = "[\\s,]+";
    private static final String IS_NUM = "^(-?[0-9]+|-?[0-9]*.[0-9]+)$";

    private void parseDefinition(String value, PathInfo pi) {
        Log.d("svgr - parseDefiniton", value);
        String[] parts = value.split(CURVE_REGEX);
        if (parts.length > 8 || parts.length < 6) {
            setState(PARSING_UNKNOWN_SVG);
            return;
        }

        int cnt = 0;
        String move = parts[cnt];
        Log.d("svgparser - move", move);
        String xS;
        String yS;

        cnt++;
        if (!move.startsWith("M")) {
            setState(PARSING_UNKNOWN_SVG);
            return;
        }
        if (move.length() > 1) {
            xS = move.substring(1);
        } else {
            xS = parts[cnt++];
        }

        yS = parts[cnt++];

        Log.d("svgparser -xs", xS);
        Log.d("svgparser -ys", yS);

        if (!canTryParse(xS, yS)) {
            setState(PARSING_ERROR);
            return;
        }
        PointF pStart, pControl, pEnd;
        pStart = parseNext(xS, yS);
        pi.setStart(pStart);

        String quadr = parts[cnt++];

        Log.d("svgaprsers -quadr", quadr);

        if (!quadr.startsWith("Q")) {
            Log.d("svgparser", "doesnt start with Q?");
            setState(PARSING_UNKNOWN_SVG);
            return;
        }

        if (quadr.length() > 1) {
            xS = quadr.substring(1);
        } else {
            xS = parts[cnt++];
        }

        yS = parts[cnt++];

        Log.d("svgaprsers -xs", xS);
        Log.d("svgparser - ys", yS);

        if (!canTryParse(xS, yS)) {
            setState(PARSING_ERROR);
            return;
        }

        pControl = parseNext(xS, yS);
        pi.setControlPoint(pControl);

        xS = parts[cnt++];
        yS = parts[cnt];

        Log.d("svgaprsers -xs", xS);
        Log.d("svgparser - ys", yS);

        if (!canTryParse(xS, yS)) {
            setState(PARSING_ERROR);
            return;
        } else {
            pEnd = parseNext(xS, yS);
            pi.setEnd(pEnd);
        }

        checkPointsForBoundaries(pStart, pControl, pEnd);
    }

    private void checkPointsForBoundaries(PointF pStart, PointF pControl, PointF pEnd) {
        // First work with x coords
        float left, right, top, bottom;
        if (liesBetween(pStart.x, pEnd.x, pControl.x)) {
            left = pStart.x < pEnd.x ? pStart.x : pEnd.x;
            right = pStart.x > pEnd.x ? pStart.x : pEnd.x;
        } else {
            float t = calculateQuadParam(pStart.x, pControl.x, pEnd.x);
            float tRest = 1 - t;
            float xtreme = tRest * tRest * pStart.x + 2 * tRest * t * pControl.x + t * t * pEnd.x;
            left = Math.min(xtreme, Math.min(pStart.x, pEnd.x));
            right = Math.max(xtreme, Math.max(pStart.x, pEnd.x));
        }

        // Now y coords
        if (liesBetween(pStart.y, pEnd.y, pControl.y)) {
            top = pStart.y < pEnd.y ? pStart.y : pEnd.y;
            bottom = pStart.y > pEnd.y ? pStart.y : pEnd.y;
        } else {
            float t = calculateQuadParam(pStart.y, pControl.y, pEnd.y);
            float tRest = 1 - t;
            float yXtreme = tRest * tRest * pStart.y + 2 * tRest * t * pControl.y + t * t * pEnd.y;
            top = Math.min(yXtreme, Math.min(pStart.y, pEnd.y));
            bottom = Math.max(yXtreme, Math.max(pStart.y, pEnd.y));
        }

        this.leftBound = Math.min(left, this.leftBound);
        this.rightBound = Math.max(right, this.rightBound);
        this.bottomBound = Math.max(bottom, this.bottomBound);
        this.topBound = Math.min(top, this.topBound);

        // And now compare with extrema found so far...
    }

    private float calculateQuadParam(float start, float control, float end) {
        return (start - control) / (start - 2 * control + end);
    }

    private boolean liesBetween(float a1, float a2, float isBetween) {
        float max = a1 > a2 ? a1 : a2;
        float min = a1 < a2 ? a1 : a2;
        return isBetween >= min && isBetween <= max;
    }

    private boolean canTryParse(String sx, String sy) {
        return Pattern.matches(IS_NUM, sx) && Pattern.matches(IS_NUM, sy);
    }

    private PointF parseNext(String sx, String sy) {
        float x = parseXmlFloat(sx);
        float y = parseXmlFloat(sy);
        return new PointF(x, y);
    }

    private static final String COLOR_REGEX = "^#?[0-9a-fA-F]{6}$";

    /**
     * Sets color of Paint as in SVG or sets it black if there's a problem with parsing it.
     * @param value
     */
    private int parseStroke(String value) {
        Log.d("svgr - parseStroke", value);
        int color = 0xff000000; // non-opaque black
        if (!Pattern.matches(COLOR_REGEX, value)) {
            /*
            Defining colors by name is possible but I don't want to handle it here, so just let this case
            be only an unknown value rather than total parsing error (which is however still possible).
             */
            setState(PARSING_OK_UNKNOWN_VALUES);
            return color;
        } else {
            if (value.length() == 7) {
                value = value.substring(1);
            }
            int bla = Integer.parseInt(value, 16);
            Log.d("parseStroke", "bla: " + bla + ", color: " + color);
            color += bla;
            Log.d("parseStroke", "together: " + color);
            return color;
        }
    }

    private float parseStrokeWidth(String value) {
        Log.d("svgr - strokeWidth", value);
        if (!Pattern.matches(DIMENSION_ACCEPTABLE_REGEX, value)) {
            setState(PARSING_ERROR);
            return 0.0f;
        } else {
            float width = parseXmlFloat(value);
            return width;
        }
    }


    private final static String FILL_NONE = "none";
    private final static String FILL_TRANSPARENT = "transparent";

    /**
     * Checks if fill is none (the only value that the app knows), else sets flag as PARSING_OK_UNKNOWN_VALUES
     * @param value svg path's fill attribute value
     */
    private void checkFill(String value) {
        Log.d("svgr - fill", value);
        if (!value.equalsIgnoreCase(FILL_NONE) && !value.equalsIgnoreCase(FILL_TRANSPARENT)) {
            Log.d("svgr - fill", "Unknown value");
            setState(PARSING_OK_UNKNOWN_VALUES);
        }
    }

    private void skip() throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }

        setState(PARSING_OK_UNKNOWN_VALUES);
    }

    /**
     * Getter for height of provided SVG file.
     * @return Height of image or -1 if it is unknown at the moment
     */
    public float getHeight() {
        return height;
    }

    public List<PathInfo> getPathInfos() {
        return pathInfos;
    }

    /**
     * Getter for state of SVG reader.
     * @return One of five specified constants.
     */
    public int getState() {
        return state;
    }

    /**
     * Getter for width of provided SVG file.
     * @return Width of image or -1 if it is unknown at the moment.
     */
    public float getWidth() {
        return width;
    }

    /**
     * Sets new state of the reader, if its precedency is higher than one of current state.
     * @param state State according to encountered event.
     */
    private void setState(int state) {
        if (this.state < state) {
            this.state = state;
        }
    }

}

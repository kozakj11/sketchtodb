package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

/**
 * Created by kozack on 28. 6. 2016.
 */
public class PtakLoadDecoder implements Runnable {

    private final PtakLoadTask task;

    public static final int DECODING_OK = 1;
    public static final int DECODING_FAIL = -1;

    public PtakLoadDecoder(PtakLoadTask task) {
        this.task = task;
    }

    @Override
    public void run() {
        task.setCurrentThread(Thread.currentThread());
        byte[] bytes = task.getBuffer();
        Bitmap bmp = null;
        int finalScaleFactor = 0;

        try {
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inJustDecodeBounds = true;

            if (Thread.interrupted()) {
                return; // flags and clean up in finally block
            }

            BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opt);
            int scaleFactor = calculateInSampleSize(opt, task.getReqWidth(), task.getReqHeight());

            if (Thread.interrupted()) {
                return; // flags and clean up in finally block
            }

            opt.inJustDecodeBounds = false;
            opt.inSampleSize = scaleFactor;
            bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opt);
            finalScaleFactor = scaleFactor;

        } catch (Throwable ex) {
            Log.e("PtakLoadDecoder", ex.getMessage());
        } finally {
            if (bmp == null) {
                task.handleDecodeState(DECODING_FAIL);
            } else {
                task.setResultImage(bmp, finalScaleFactor);
                task.handleDecodeState(DECODING_OK);
            }
            task.setCurrentThread(null);
            Thread.interrupted();
        }
    }

    /**
     * Calculates inSampleSize for scaling a bitmap.
     * As on https://developer.android.com/training/displaying-bitmaps/load-bitmap.html
     * @param options BitmapFactory options of the bitmap
     * @param reqWidth requested width
     * @param reqHeight requested height
     * @return inSampleSize to be used (a power of 2)
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        Log.d("calculateSampleSize", "outH: " + height + ", outW: " + width + ", reqWidth: " + reqWidth + ", reqH: " + reqHeight);

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = (int) (height * 1.375f);
            final int halfWidth = (int) (width * 1.375f);

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width at most by (roughly) 37.5 %
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        Log.d("calculateSampleSize", "finalSize: " + inSampleSize);
        return inSampleSize;
    }

}

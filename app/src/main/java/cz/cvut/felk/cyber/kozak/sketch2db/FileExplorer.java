package cz.cvut.felk.cyber.kozak.sketch2db;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.*;
import android.widget.*;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.AcceptableImageType;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.CreateNewDirectoryDialog;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.FileExplorerDialogListener;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.OverwriteFileDialog;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Basic file explorer activity for saving/opening files in the app.
 */
public class FileExplorer extends Activity
        implements FileExplorerDialogListener {

    public static final String INTENT_BUNDLE_NAME = "cz.cvut.felk.cyber.kozak.sketch2db.File_explorer_bundle";

    /**
     * Enumeration of keys used in saved instance bundle.
     */
    public enum BundleKey {
        START_DIRECTORY("cz.cvut.felk.cyber.kozak.sketch2db.Start_directory"),
        OPEN_FILE("cz.cvut.felk.cyber.kozak.sketch2db.Open_file"),
        SAVE_FILE("cz.cvut.felk.cyber.kozak.sketch2db.Save_file"),
        LAST_CHOSEN("cz.cvut.felk.cyber.kozak.sketch2db.Last_chosen"),
        SAVE_AS_FILETYPE("cz.cvut.felk.cyber.kozak.sketch2db.Save_as_filetype"),
        NAME_OF_SAVED_IMG("cz.cvut.felk.cyber.kozak.sketch2db.Name_of_saved_img"),
        DONT_SHOW_AIT("cz.cvut.felk.cyber.kozak.sketch2db.Dont_show_ait");

        private final String key;

        BundleKey(String s) {
            this.key = s;
        }

        /**
         * Key used in bundle for this enum value.
         * @return Key string as used in bundle.
         */
        public String getKey() {
            return key;
        }
    }

    /**
     * Key for File Explorer activity result used in result intent.
     */
    public static final String RESULT_ACTION = "cz.cvut.felk.cyber.kozak.sketch2db.RESULT_ACTION";
    /**
     * Key for File Explorer activity result informing if selected file is SVG or not, as used in result intent.
     */
    public static final String IS_SVG_BOOLEAN_EXTRA = "cz.cvut.felk.cyber.kozak.sketch2db.IS_SVG_BOOLEAN_EXTRA";
    /**
     * Key for File Explorer activity result that returns AcceptableImageType.
     */
    public static final String FILE_EXTENSION_AIT = "cz.cvut.felk.cyber.kozak.sketch2db.FILE_EXTENSION_AIT";
    /**
     * Key for File Explorer activity result that returns location of Ptak server image that is to be saved.
     */
    public static final String DETAILED_TO_SAVE = "cz.cvut.felk.cyber.kozak.sketch2db.DETAILED_TO_SAVE";

    private File currentDirectory;
    private EditText chosenFileTextView;
    private File chosenFileReference;
    private boolean isForOpening, showAit;
    private Spinner spinner;

    public static final String DIALOG_FILE = "modal_dialog_file";
    public static final String DIALOG_INPUT = "modal_dialog_input_string";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.file_explorer);
        if (savedInstanceState == null) {
            savedInstanceState = this.getIntent().getExtras();
        }
        inflateSpinner(savedInstanceState);
        inflateFromBundle(savedInstanceState);
        setButtonText(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (currentDirectory != null) {
            outState.putString(BundleKey.START_DIRECTORY.getKey(), currentDirectory.getAbsolutePath());
        }
        if (chosenFileReference != null) {
            outState.putString(BundleKey.LAST_CHOSEN.getKey(), //chosenFileReference.getAbsolutePath());
                                chosenFileTextView.getText().toString());
        }
        if (isForOpening) {
            outState.putString(BundleKey.OPEN_FILE.getKey(), "open");
        } else {
            outState.putString(BundleKey.SAVE_FILE.getKey(), "save");
        }

        if (showAit) {
            AcceptableImageType ait = (AcceptableImageType) spinner.getSelectedItem();
            if (ait != null) {
                outState.putSerializable(BundleKey.SAVE_AS_FILETYPE.getKey(), ait);
            }
        } else {
            outState.putString(BundleKey.DONT_SHOW_AIT.getKey(), "dont");
        }
    }

    private void setButtonText(Bundle b) {
        if (b != null && b.containsKey(BundleKey.OPEN_FILE.getKey())) {
            Button button = (Button) findViewById(R.id.fe_use_file_button);
            button.setText(R.string.open);
        }
    }

    private void inflateSpinner(Bundle bundle) {
        this.spinner = (Spinner) findViewById(R.id.fe_filetype_spinner);
        ArrayAdapter<AcceptableImageType> adapter = new ArrayAdapter<>(getBaseContext(),
                android.R.layout.simple_spinner_item, AcceptableImageType.values());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        if (bundle.getString(BundleKey.DONT_SHOW_AIT.getKey()) != null) {
            spinner.setVisibility(View.GONE);
            showAit = false;
        } else {
            showAit = true;
        }
    }

    /**
     * Creates bundle with activity result (stores file path and type of selected file into Intent object) and
     * finishes FileExplorer activity.
     * If nothing is selected, Toast is shown and activity continues.
     * @param view caller view/object
     */
    public void useFileButton(View view) {
        chosenFileTextView = (EditText) findViewById(R.id.fe_chosen_file);
        String input = chosenFileTextView.getText().toString();
        if (input == null || input.isEmpty()) {
            Toast t = Toast.makeText(this, R.string.please_select_a_file, Toast.LENGTH_LONG);
            t.show();
            return;
        }
        if (!isForOpening) {
            if (showAit) {
                AcceptableImageType ait = (AcceptableImageType) spinner.getSelectedItem();
                String suffix = ait.getSuffix();
                if (!input.endsWith(suffix)) {
                    input += suffix;
                }
            } else {
                // Get file extension from file on Ptak server
                String originalFilename = getIntent().getExtras().getString(BundleKey.NAME_OF_SAVED_IMG.getKey());
                int i = originalFilename.lastIndexOf('.');
                if (i > 0) {
                    String suffix = originalFilename.substring(i);
                    if (!input.endsWith(suffix)) {
                        input += suffix;
                    }
                }
            }
        }
        File toUri = new File(currentDirectory, input);
        if (toUri.exists() && !isForOpening) {
            startModal(toUri, input);
        } else {
            createResults(toUri, input);
        }
    }

    private void startModal(File file, String input) {
        Bundle b = new Bundle();
        b.putSerializable(DIALOG_FILE, file);
        b.putString(DIALOG_INPUT, input);

        OverwriteFileDialog ofd = new OverwriteFileDialog();
        ofd.setArguments(b);
        ofd.show(getFragmentManager(), "dialog");
    }

    @Override
    public void onNegativeReply(FileExplorerDialogListener.FileExplorerDialogs type) {
        // do nothing
    }

    @Override
    public void onPositiveReply(FileExplorerDialogListener.FileExplorerDialogs type, File file, String input) {
        // overwrite file
        switch (type) {
            case CREATE_NEW_DIRECTORY:
                createNewDirectory(file, input);
                break;
            case OVERWRITE_FILE:
                createResults(file, input);
        }
    }

    private void createResults(File toUri, String input) {
        Intent intent = new Intent(RESULT_ACTION, Uri.fromFile(toUri));
        intent.putExtra(IS_SVG_BOOLEAN_EXTRA, input.endsWith(AcceptableImageType.SVG.getSuffix()));
        AcceptableImageType ait2 = AcceptableImageType.getFromFileName(input);
        if (ait2 != null) {
            intent.putExtra(FILE_EXTENSION_AIT , ait2);
        }
        String nameOfSavedImg = getIntent().getExtras().getString(BundleKey.NAME_OF_SAVED_IMG.getKey());
        if (nameOfSavedImg != null) {
            intent.putExtra(DETAILED_TO_SAVE, nameOfSavedImg);
        }
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void inflateFromBundle(Bundle bundle) {
        //createFiles();
        Context context = this.getBaseContext();
        Log.d("FE", "After file...");

        chosenFileTextView = (EditText) findViewById(R.id.fe_chosen_file);
        chosenFileTextView.setText("");

        // read bundle and check if there are any extras saved
        if (bundle == null) {
            Log.d("FE", "saved bundle is null");
            bundle = getIntent().getExtras();
            if (bundle == null || bundle.getString(BundleKey.START_DIRECTORY.getKey()) == null) {
                Log.d("FE", "and again");
                new StartingDirectoryTask(context, null).execute(true);
                return;
            }
        }
        Log.d("FE", "Bundle is not null");
        String path = bundle.getString(BundleKey.START_DIRECTORY.getKey());

        String chosen = bundle.getString(BundleKey.LAST_CHOSEN.getKey());
        if (chosen != null) {
            chosenFileReference = new File(chosen);
            chosenFileTextView.setText(chosenFileReference.getName());
        }
        AcceptableImageType ait = (AcceptableImageType) bundle.getSerializable(BundleKey.SAVE_AS_FILETYPE.getKey());
        if (ait != null) {
            int selected = 0;
            for (int i = 0; i < spinner.getCount(); i++) {
                AcceptableImageType ait2 = (AcceptableImageType) spinner.getItemAtPosition(i);
                if (ait2 == ait) {
                    selected = i;
                    break;
                }
            }
            spinner.setSelection(selected);
        }
        if (bundle.getString(BundleKey.OPEN_FILE.getKey()) != null) {
            chosenFileTextView.setFocusable(false);
            chosenFileTextView.setCursorVisible(false);
            isForOpening = true;
        }

        new StartingDirectoryTask(context, path).execute(false);

    }

    /**
     * Called from Directory Up Button in UI. If we're on top of hierarchy,
     * @param view Caller view.
     */
    public void directoryUpButton(View view) {
        File parentDir = this.currentDirectory.getParentFile();
        if (parentDir != null) {
            changeDirectory(parentDir);
        } else {
            Toast.makeText(this, R.string.no_parent_directory, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Updates UI after opening new directory.
     * @param newDirectory File representing chosen directory
     */
    private void changeDirectory(File newDirectory) {
        // change name of directory shown in the UI
        if (newDirectory.canRead()) {

            this.currentDirectory = newDirectory;
            TextView tv = (TextView) findViewById(R.id.fe_current_dir);
            tv.setText(newDirectory.getName());

            // deselect current file (if any)
            this.chosenFileTextView.setText("");
            this.chosenFileReference = null;

            // show loading circle, hide list of files
            findViewById(R.id.fe_load_files_progress).setVisibility(View.VISIBLE);
            findViewById(R.id.file_explorer_list).setVisibility(View.GONE);

            if (!newDirectory.canWrite()) {
                Toast.makeText(this, R.string.cant_write_directory, Toast.LENGTH_LONG).show();
            }

            // start AsyncTask for getting files in the directory
            new ListFilesTask().execute(newDirectory);
        } else {
            Toast.makeText(this, R.string.cant_read_directory, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Inflates ListView of files in selected directory.
     * @param files List of files in selected directory
     */
    private void listFiles(List<File> files) {
        Log.d("Fexlprer", "listFiles");
        ListView lv = (ListView) findViewById(R.id.file_explorer_list);

        final File[] filesArray = new File[files.size()];
        files.toArray(filesArray);
        Arrays.sort(filesArray);

        lv.setAdapter(new FileListAdapter(this, filesArray));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File referenced = filesArray[position];
                if (referenced.isFile()) {
                    chosenFileTextView.setText(referenced.getName());
                    chosenFileReference = referenced;
                }
                if (referenced.isDirectory()) {
                    changeDirectory(referenced);
                }
            }
        });

        if (files.isEmpty() && isForOpening) {
            Toast.makeText(this, R.string.file_explorer_no_files_found, Toast.LENGTH_LONG).show();
        }

        // hide loading circle
        View progressBar = findViewById(R.id.fe_load_files_progress);
        progressBar.setVisibility(View.GONE);

        // show list
        lv.setVisibility(View.VISIBLE);


    }

    /**
     * Async task for finding actual directory
     */
    private class StartingDirectoryTask extends AsyncTask<Boolean, Integer, File> {

        private final String expectedPathname;
        private final Context context;

        /**
         * Creates AsyncTask and saves needed params
         * @param context from getContext of caller
         * @param expectedPathname expected filename of starting directory
         */
        private StartingDirectoryTask(Context context, String expectedPathname) {
            super();
            this.expectedPathname = expectedPathname;
            this.context = context;
        }

        @Override
        protected File doInBackground(Boolean... params) {
            boolean wantPublic = params[0].booleanValue(); // should be only one and if there are more, then just ignore them
            File filesDir = context.getFilesDir(); // internal directory of this application

            // if we want public external storage, then check it is mounted and that directory exists
            if (wantPublic && isExternalStorageWritable()) {
                File possible = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                if (possible.isDirectory()) {
                    return possible;
                } else {
                    // if it is file or we can't create the directory, return internal one
                    if (possible.isFile() || !possible.mkdirs()) {
                        return filesDir;
                    }
                    return possible;
                }
            }

            // if we want any storage and the path is not null
            if (!wantPublic && expectedPathname != null) {
                File fromPath = new File(expectedPathname);
                File externalRoot = Environment.getExternalStorageDirectory();
                // it is not a (or in) subdirectory of external storage or external storage is writable, we may use it
                if (!isSubdir(fromPath, externalRoot) || isExternalStorageWritable()) {
                    // but it must be a directory
                    if (fromPath.isDirectory()) {
                        return fromPath;
                    }
                    // or we must be able to create it
                    if (!fromPath.isFile() && fromPath.mkdirs()) {
                        return fromPath;
                    }
                }
            }

            // return internal storage directory as default
            return filesDir;
        }

        @Override
        protected void onPostExecute(File file) {
            currentDirectory = file;
            TextView tv = (TextView) findViewById(R.id.fe_current_dir);
            tv.setText(file.getName());

            new ListFilesTask().execute(file);
        }

        /**
         * Checks if external storage is mounted and writable.
         * @return true if external storage is mounted and writable
         */
        private boolean isExternalStorageWritable() {
            return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED); // vs. MEDIA_MOUNTED_READ_ONLY
        }

        private boolean isSubdir(File checked, File ofWhat) {
            String cPath = checked.getAbsolutePath();
            String owPath = ofWhat.getAbsolutePath();
            return cPath.length() > owPath.length() && cPath.startsWith(owPath);
        }

    }

    /**
     * AsyncTask for listing files in selected directory.
     */
    private class ListFilesTask extends AsyncTask<File, Integer, List<File>> {

        @Override
        protected List<File> doInBackground(File... params) {
            List<File> files = new ArrayList<>();
            int size = params.length;
            for (int i = 0; i < size; i++) {
                File[] subFiles = params[i].listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        if (pathname.isDirectory()) {
                            return true;
                        }
                        String path = pathname.getName().toLowerCase();
                        String sfx;
                        for (AcceptableImageType ait: AcceptableImageType.values() ) {
                            sfx = ait.getSuffix();
                            if (path.endsWith(sfx)) {
                                return true;
                            }
                        }
                        return false;
                    }
                });
                // listFiles doesn't throw Exceptions, but might return null if I/O problem occurs
                if (subFiles != null) {
                    for (File subFile : subFiles) {
                        files.add(subFile);
                    }
                }

                if (isCancelled()) {
                    break;
                }
            }

            return files;
        }

        @Override
        protected void onPostExecute(List<File> files) {
            listFiles(files);
        }
    }

    protected class FileListAdapter extends ArrayAdapter<File> {

        private File[] files;
        private Activity context;

        public FileListAdapter(Activity context, File[] files) {
            super(context, R.layout.icon_text_list, files);
            this.context = context;
            this.files = files;
            Log.d("Fexplorer", "constructor of FileListAdapter");
        }

        public void setFiles(File[] files) {
            this.files = files;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            Log.d("Fexlporer", "get view of custom adapter");
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView;

            if (view == null) {
                rowView = inflater.inflate(R.layout.icon_text_list, null, true);
            } else {
               rowView = view;
            }

            TextView txtTitle = (TextView) rowView.findViewById(R.id.item_name);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

            Log.d("fexlprer","after inflation found");

            txtTitle.setText(files[position].getName());
            int resource = files[position].isDirectory() ? R.drawable.folder_icon : R.drawable.image_icon;
            imageView.setImageResource(resource);
            Log.d("fexlprer","going return it");
            return rowView;
        }

    }

    //IntelliJ default method stubs...
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_file_explorer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id) {
            case R.id.action_settings:
                return true;
            case R.id.app_directory:
                File app = getFilesDir();
                if (!app.equals(currentDirectory)) {
                    changeDirectory(app);
                } else {
                    Toast.makeText(this, R.string.you_are_browsing_it, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.external_images:
                File external = Environment.getExternalStorageDirectory();
                if (!external.equals(currentDirectory)) {
                    changeDirectory(external);
                } else {
                    Toast.makeText(this, R.string.you_are_browsing_it, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_directory_up:
                directoryUpButton(null);
                return true;
            case R.id.new_directory:
                createNewDirectory();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void createNewDirectory() {
        Bundle b = new Bundle();
        b.putSerializable(DIALOG_FILE, currentDirectory);

        CreateNewDirectoryDialog cnd = new CreateNewDirectoryDialog();
        cnd.setArguments(b);
        cnd.show(getFragmentManager(), "cnd_dialog");
    }

    private void createNewDirectory(File file, String input) {
        new CreateNewDirectoryTask(input).execute(file);
    }

    private class CreateNewDirectoryTask extends AsyncTask<File, Integer, Integer> {

        private final String dirName;
        private File parent;

        private CreateNewDirectoryTask(String dirName) {
            this.dirName = dirName;
        }

        @Override
        protected Integer doInBackground(File... params) {
            parent = params[0];
            File nextDir = new File(parent, dirName);
            if (nextDir.exists()) {
                return -1;
            }
            if (nextDir.mkdir()) {
                return 1; // Success
            } else {
                return 0;
            }
        }

        @Override
        protected void onPostExecute(Integer aBoolean) {
            switch (aBoolean) {
                case -1: //Already exists
                    Toast.makeText(FileExplorer.this, R.string.directory_exists, Toast.LENGTH_SHORT);
                    break;
                case 0: // Failed
                    Toast.makeText(FileExplorer.this, R.string.new_dir_failed, Toast.LENGTH_SHORT);
                    break;
                case 1: // Success
                    Toast.makeText(FileExplorer.this, R.string.new_dir_success, Toast.LENGTH_SHORT);
                    if (parent.equals(currentDirectory)) {
                        new ListFilesTask().execute(currentDirectory);
                    }
            }
        }
    }

}

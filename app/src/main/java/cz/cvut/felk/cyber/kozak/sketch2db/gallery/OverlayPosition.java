package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.graphics.*;
import android.util.Log;

/**
 * Created by kozack on 20. 9. 2016.
 */
public class OverlayPosition implements SketchPosition {

    private Bitmap overlay;
    private float imageScale = 1.0f, centerScale = 1.0f;
    private int xCenter, yCenter;
    private boolean isMirrored = false;

    private Paint paint;

    public OverlayPosition(String xCenter, String yCenter, String scale, Bitmap overlay) {
        this.imageScale = Float.parseFloat(scale);
        this.overlay = overlay;
        setCenters(xCenter, yCenter);
        this.paint = createPaint();
    }

    public OverlayPosition(float xCenter, float yCenter, float scale, Bitmap overlay) {
        this.overlay = overlay;
        this.imageScale = 200 * scale / Math.max(overlay.getWidth(), overlay.getHeight());
        this.xCenter = (int) xCenter;
        this.yCenter = (int) yCenter;
        this.paint = createPaint();
    }

    private void setCenters(String xCenter, String yCenter) {
        this.xCenter = (int) Float.parseFloat(xCenter);
        this.yCenter = (int) Float.parseFloat(yCenter);
    }

    @Override
    public void setMirrored(boolean isMirrored) {
        this.isMirrored = isMirrored;
    }

    @Override
    public void drawPosition(Canvas canvas, boolean showOverlay, boolean showRect) {
        if (overlay != null) {
            float canvasMovedXcenter = canvas.getWidth() * 0.5f + xCenter;
            float canvasMovedYcenter = canvas.getHeight() * 0.5f + yCenter;

            float scaledXsize = overlay.getWidth() * 0.5f * imageScale;
            float scaledYsize = overlay.getHeight() * 0.5f * imageScale;

            Rect destination = new Rect();
            destination.left = (int) (canvasMovedXcenter - scaledXsize);
            destination.top = (int) (canvasMovedYcenter - scaledYsize);
            destination.right = (int) (2 * scaledXsize) + destination.left;
            destination.bottom = (int) (2 * scaledYsize) + destination.top;

            //Log.d("OverlayPosition", "left: " + destination.left + ", right" + destination.right + ", top:" + destination.right + ", bottom: " + destination.bottom);
            //Log.d("OverlayPosition", "scale: " + imageScale);
            if (showOverlay) {
                if (isMirrored) {
                    Matrix matrix = new Matrix();
                    matrix.preScale(-1.0f, 1.0f);
                    Bitmap help = Bitmap.createBitmap(overlay, 0, 0, overlay.getWidth(), overlay.getHeight(), matrix, false);
                    canvas.drawBitmap(help, null, destination, null);
                } else {
                    canvas.drawBitmap(overlay, null, destination, null);
                }
            }
            if (showRect) {
                canvas.drawRect(destination, paint);
            }
        }
    }

    private Paint createPaint() {
        Paint paint = new Paint(Paint.DITHER_FLAG);
        paint.setAntiAlias(true);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);
        paint.setStrokeWidth(4f);
        return paint;
    }

    @Override
    public void rescale(int scale) {
        this.imageScale /= scale;
        this.xCenter /= scale;
        this.yCenter /= scale;
    }
}

package cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import cz.cvut.felk.cyber.kozak.sketch2db.FileExplorer;
import cz.cvut.felk.cyber.kozak.sketch2db.R;

import java.io.File;

/**
 * Created by kozack on 15. 7. 2016.
 */
public class UseMobilDataDialog extends DialogFragment {

    public interface UseMobilDataDialogListener {
        void onPositiveReply(DialogFragment dialog);
        void onNegativeReply(DialogFragment dialog);
    }

    private UseMobilDataDialogListener listener;
    private CheckBox rememberAnswer;

    public UseMobilDataDialog() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.listener = (UseMobilDataDialogListener) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inf = getActivity().getLayoutInflater();
        View dialogText = inf.inflate(R.layout.mobile_data_dialog, null);
        rememberAnswer = (CheckBox) dialogText.findViewById(R.id.mobile_dialog_checkbox);

        builder.setView(dialogText)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onPositiveReply(UseMobilDataDialog.this);
                        if (rememberAnswer.isChecked()) {
                            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString(getResources().getString(R.string.preference_use_mobil_data),
                                    getResources().getString(R.string.yes));
                            editor.apply();
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onNegativeReply(UseMobilDataDialog.this);
                        if (rememberAnswer.isChecked()) {
                            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString(getResources().getString(R.string.preference_use_mobil_data),
                                    getResources().getString(R.string.never));
                            editor.apply();
                        }
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.graphics.*;
import android.util.Log;

/**
 * Created by kozack on 15. 9. 2016.
 */
public class PositionTry implements SketchPosition {

    private Bitmap overlay;

    private float relHeight, relWidth, relRight, relDown;

    private Paint paint;

    public PositionTry(Bitmap overlay) {
        this.overlay = overlay;
        relRight = (float) Math.random();
        relDown = (float) Math.random();
        relHeight = relDown * (float) Math.random();
        relWidth = relRight * (float) Math.random();
        this.paint = createPaint();
    }

    private Paint createPaint() {
        Paint paint = new Paint(Paint.DITHER_FLAG);
        paint.setAntiAlias(true);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);
        paint.setStrokeWidth(2f);
        return paint;
    }

    @Override
    public void drawPosition(Canvas canvas, boolean showOverlay, boolean showRectangle) {
        Rect destination = new Rect();
        destination.bottom = (int) (relDown * canvas.getHeight());
        destination.top = destination.bottom - (int) (relHeight * canvas.getHeight());
        destination.right = (int) (relRight * canvas.getWidth());
        destination.left = destination.right - (int) (relWidth * canvas.getWidth());
        //Log.d("PositonyTry", "bootom: " + destination.bottom + ", top: " + destination.top + ", right: " + destination.right
        //+ ", left: " + destination.left);

        //Rect src = new Rect(0, 0, overlay.getxCenter(), overlay.getyCenter());
        canvas.drawBitmap(overlay, null, destination, null);
        canvas.drawRect(destination, paint);
    }

    public void rescale(int rescale) {

    }

    @Override
    public void setMirrored(boolean isMirrored) {

    }
}

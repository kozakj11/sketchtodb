package cz.cvut.felk.cyber.kozak.sketch2db;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;

import java.util.List;

/**
 * Created by kozack on 18. 6. 2016.
 */
public class XmlImageView extends ImageView {

    private List<PathInfo> pinfos;
    private boolean isXml;
    private float xmlHeight, xmlWidth;
    private int bmpX, bmpY;

    private static final float MAX_IMAGE_DIM = 0.75f;

    public XmlImageView(Context context) {
        super(context);
    }

    public XmlImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d("xmlimage", "ondrawcalled");
        if (isXml) {
            float xScale = bmpX / xmlWidth;
            float yScale = bmpY / xmlHeight;
            canvas.scale(xScale, yScale);
            drawSvg(canvas);
        } /*else {
            Drawable d = getDrawable();
            if (d != null) {
                float xScale = d.getBounds()
            }
        }*/
    }

    private void drawSvg(Canvas canvas) {
        for (PathInfo pi: pinfos) {
            Path p = new Path();
            pi.pathInfoDraw(p);
            canvas.drawPath(p, pi.getPaint());
        }
    }

    public void setIsXml(boolean isXml) {
        this.isXml = isXml;
    }

    public void setPathInfos(List<PathInfo> pinfos) {
        this.pinfos = pinfos;
    }

    public void setXml(float height, float width, Display disp) {
        this.xmlHeight = height;
        this.xmlWidth = width;
        Bitmap bmp;
        Point pnt = new Point();
        disp.getSize(pnt);
        int bigger = pnt.x > pnt.y ? pnt.x : pnt.y;
        int xSize, ySize;
        if (height > width) {
            ySize = (int) (bigger * MAX_IMAGE_DIM);
            xSize = (int) (bigger * MAX_IMAGE_DIM * width / height);
        } else {
            // landscape images shouldn't be wider than screen, so pnt.x is used in both display orientations
            xSize = (int) (pnt.x * MAX_IMAGE_DIM);
            ySize = (int) (pnt.x* MAX_IMAGE_DIM * height / width);
        }
        bmp = Bitmap.createBitmap(xSize, ySize, Bitmap.Config.ARGB_8888);
        setImageBitmap(bmp);

        this.bmpX = xSize;
        this.bmpY = ySize;

        invalidate();
    }

}

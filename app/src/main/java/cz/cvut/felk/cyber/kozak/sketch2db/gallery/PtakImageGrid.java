package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.Image;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;
import cz.cvut.felk.cyber.kozak.sketch2db.R;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakLoadManager;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakLoadTask;

import java.lang.ref.WeakReference;

/**
 * Created by kozack on 28. 6. 2016.
 */
public class PtakImageGrid extends ViewFlipper implements PtakImage {

    private String path;
    private PtakLoadTask task;
    private int startWidth = -1;
    private int startHeight = -1;
    private SketchPosition position = null;
    private Bitmap overlay = null;

    private WeakReference<ImageView> imageView;
    private WeakReference<ProgressBar> progressBar;

    public PtakImageGrid(Context context) {
        super(context);
        init();
    }

    public PtakImageGrid(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PtakImageGrid(Context context, String path) {
        super(context);
        this.path = path;
        init();
    }

    public PtakImageGrid(Context context, AttributeSet attrs, String path) {
        super(context, attrs);
        this.path = path;
        init();
    }

    @Override
    protected void onDetachedFromWindow() {
        Log.d("PtakImageGrid", "onDetachedToWindow called");

        stopDownload();

        Log.d("PtakImageGrid", "stopDownload");

        if (imageView != null) {
            if (imageView.get() != null) {
                removeViewAt(1);
            }
            imageView.clear();
            imageView = null;
        }

        Log.d("PtakImageGrid", "imageView");

        if (progressBar != null) {
            if (progressBar.get() != null) {
                removeViewAt(0);
            }
            progressBar.clear();
            progressBar = null;
        }

        Log.d("PtakImageGrid", "progressBar and now lets call super!");

        // trick
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException ex) {
            Log.d("PtakImageGrid", "IllegalArgEx: " + ex.getMessage());
        } finally {
            stopFlipping();
        }
    }

    public ImageView getImageView() {
        checkImageView();
        return imageView.get();
    }

    private void initCheck() {
        checkProgressBar();
        checkImageView();
    }

    private void init() {
        if (this.progressBar == null) {
            ProgressBar pb = new ProgressBar(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            pb.setLayoutParams(lp);
            pb.setIndeterminate(true);

            this.progressBar = new WeakReference<>(pb);
            addView(pb, 0);
        }


        if (this.imageView == null) {
            ImageView iv = new ImageView(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            iv.setLayoutParams(lp);

            this.imageView = new WeakReference<>(iv);
            addView(iv, 1);
        }
    }

    private void checkProgressBar() {
        if (this.progressBar == null || progressBar.get() == null) {
            removeViewAt(0);
            ProgressBar pb = new ProgressBar(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            pb.setLayoutParams(lp);
            pb.setIndeterminate(true);

            this.progressBar = new WeakReference<>(pb);
            addView(pb, 0);
        }
    }

    private void checkImageView() {
        if (this.imageView == null || imageView.get() == null) {
            removeViewAt(1);
            ImageView iv = new ImageView(getContext());

            FrameLayout.LayoutParams lp = this.generateDefaultLayoutParams();
            iv.setLayoutParams(lp);

            this.imageView = new WeakReference<>(iv);
            addView(iv, 1);
        }
    }

    /**
     * Switches between loading circle and showing image.
     * @param isProgressBarVisible true - shows progress bar, hides image; false - vice versa
     */
    public void progressBarVisible(boolean isProgressBarVisible) {
        //initCheck();
        if (isProgressBarVisible) {

            setDisplayedChild(0);
            /*
            if (getDisplayedChild() == 1) {
                //his.setInAnimation(this.getContext(), R.anim.fade_in);
               // this.setOutAnimation(this.getContext(), R.anim.fade_out);
                this.showPrevious();
            } else {
                Log.d("PtakImageGrid", "displayed child is not 1");
            }*/

        } else {

            setDisplayedChild(1);

            /*
            if (getDisplayedChild() == 0) {
               // this.setInAnimation(this.getContext(), R.anim.fade_in);
               // this.setOutAnimation(this.getContext(), R.anim.fade_out);
                this.showNext();
            } else {
                Log.d("PtakImageGrid", "displayed child is not 0");
            }*/
        }
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int getStartWidth() {
        return startWidth;
    }

    @Override
    public void setStartWidth(int startWidth) {
        this.startWidth = startWidth;
    }

    @Override
    public int getStartHeight() {
        return startHeight;
    }

    @Override
    public void setStartHeight(int startHeight) {
        this.startHeight = startHeight;
    }

    @Override
    public void startDownload(boolean useOnlyFileCache, boolean useDetailed) {
        this.task = PtakLoadManager.startDownload(this, true, useOnlyFileCache, useDetailed);
        progressBarVisible(true);
        Log.d("PtakImage - start", path);
    }

    @Override
    public void setSuccessImage(Bitmap image, int scale) {
        Log.d("PtakImageGrid", "setSuccessImage");
        //initCheck();

        if (imageView != null && imageView.get() != null) {
            if (position != null) {
                position.rescale(scale);
            }
            trySetImage(image);
        } else {
            progressBarVisible(false);
        }
    }

    @Override
    public void createSketchPosition(float xCenter, float yCenter, float scale, boolean isFlipped) {
        this.position = new OverlayPosition(xCenter, yCenter, scale, overlay);
        this.position.setMirrored(isFlipped);
    }

    private void trySetImage(Bitmap image) {
        if (overlay != null) {
            Log.d("PtakImageGrid", "trySetImage - not null overlay");
            // TODO - to async task ?
            Bitmap mut = image.copy(Bitmap.Config.ARGB_8888, true);
            Canvas canvas = new Canvas(mut);
            if (position != null) {
                position.drawPosition(canvas, true, false);
            }
            Log.d("PtakImageGrid", "before drawing");
            imageView.get().setImageBitmap(mut);
            Log.d("PtakImageGrid", "set progress bar invisible");
            progressBarVisible(false);
        } else {
            Log.d("PtakImageGrid", "trySetImage - null overlay");
            imageView.get().setImageBitmap(image);
            progressBarVisible(false);
        }
    }

    @Override
    public void setFailedImage() {
        //initCheck();
        progressBarVisible(false);
        Log.e("PtakImage - failed", path);
    }

    @Override
    public void stopDownload() {
        PtakLoadManager.removeDownload(task, path);
    }

    @Override
    public void setOverlay(Bitmap overlay) {
        this.overlay = overlay;
    }
}

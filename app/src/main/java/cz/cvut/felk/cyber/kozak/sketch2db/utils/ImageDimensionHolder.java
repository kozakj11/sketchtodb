package cz.cvut.felk.cyber.kozak.sketch2db.utils;

/**
 * Created by kozack on 5. 9. 2016.
 */
public class ImageDimensionHolder {

    private final int imgWidth;

    private final int imgHeight;

    private final int displayWidth;

    private final int displayHeight;

    ImageDimensionHolder(int displayHeight, int displayWidth, int imgHeight, int imgWidth) {
        this.displayHeight = displayHeight;
        this.imgWidth = imgWidth;
        this.imgHeight = imgHeight;
        this.displayWidth = displayWidth;
    }

    public int getDisplayHeight() {
        return displayHeight;
    }

    public int getDisplayWidth() {
        return displayWidth;
    }

    public int getImgHeight() {
        return imgHeight;
    }

    public int getImgWidth() {
        return imgWidth;
    }

    public boolean needsRotation() {
        boolean portraitDisp = displayHeight > displayWidth;
        boolean portraitImg = imgHeight > imgWidth;
        return portraitDisp ^ portraitImg;
    }

    public boolean isImagePortrat() {
        return imgHeight > imgWidth;
    }

}

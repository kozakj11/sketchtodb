package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import cz.cvut.felk.cyber.kozak.sketch2db.R;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.NoWifiDialog;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs.UseMobilDataDialog;

/**
 * Created by kozack on 14. 9. 2016.
 */
public class ConnectivityChecker {

    public enum ConnectivityState {
        NOT_CONNECTED,
        OK,
        ASK_MOBILE_DATA
    }

    private Activity activity;

    public ConnectivityChecker(Activity activity) {
        this.activity = activity;
    }

    public ConnectivityState checkConnectivity() {
        ConnectivityManager cm =
                (ConnectivityManager) activity.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        String defaultValue = activity.getResources().getString(R.string.ask_in_dialog);
        String preference = PreferenceManager.getDefaultSharedPreferences(activity).
                getString("pref_use_mobile_data", defaultValue);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (!isConnected) {
            return ConnectivityState.NOT_CONNECTED;
        } else {
            // if mobile-data, ask user if he wants to continue
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI
                    || preference.equals(activity.getResources().getString(R.string.yes))) {
                return ConnectivityState.OK;
            } else {
                if (preference.equals(defaultValue)) {
                    return ConnectivityState.ASK_MOBILE_DATA;
                }
                if (preference.equals(activity.getResources().getString(R.string.never))) {
                    return ConnectivityState.NOT_CONNECTED;
                }
            }
        }

        return ConnectivityState.OK;
    }

}

package cz.cvut.felk.cyber.kozak.sketch2db;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import cz.cvut.felk.cyber.kozak.sketch2db.gallery.GalleryImageAdapter;
import cz.cvut.felk.cyber.kozak.sketch2db.gallery.OverlayManager;

import java.util.ArrayList;

/**
 * Activity for showing search results.
 */
public class ResultsGallery extends Activity {

    private GridView resultsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_gallery);
        resultsView = (GridView) findViewById(R.id.results_grid);
        resultsView.setAdapter(new GalleryImageAdapter(this, new ArrayList<String>(), 7, new OverlayManager(), null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

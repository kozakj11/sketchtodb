package cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cz.cvut.felk.cyber.kozak.sketch2db.FileExplorer;
import cz.cvut.felk.cyber.kozak.sketch2db.R;

import java.io.File;

/**
 * Created by kozack on 5. 9. 2016.
 */
public class CreateNewDirectoryDialog extends DialogFragment {

    private FileExplorerDialogListener listener;
    private EditText textView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.listener = (FileExplorerDialogListener) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inf = getActivity().getLayoutInflater();
        View dialogText = inf.inflate(R.layout.create_newdir_dialog, null);
        textView = (EditText) dialogText.findViewById(R.id.create_newdir_edit_text);

        builder.setTitle(R.string.enter_name)
                .setView(dialogText)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String txt = textView.getText().toString();

                        if (txt != null && !txt.isEmpty()) {
                            Bundle bun = CreateNewDirectoryDialog.this.getArguments();
                            File file = (File) bun.getSerializable(FileExplorer.DIALOG_FILE);
                            listener.onPositiveReply(FileExplorerDialogListener.FileExplorerDialogs.CREATE_NEW_DIRECTORY, file, txt);
                        } else {
                            Toast.makeText(getActivity(), R.string.enter_name, Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onNegativeReply(FileExplorerDialogListener.FileExplorerDialogs.CREATE_NEW_DIRECTORY);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}

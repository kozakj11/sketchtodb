package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.Log;

import java.util.Locale;

/**
 *
 * Created by Jan Kozak on 29. 11. 2016.
 */
public class PathInfoCircle implements PathInfo {

    private int order;

    private PointF center, radius;
    private Paint paint;

    public PathInfoCircle() {

    }

    public PathInfoCircle(PointF center, float radius) {
        this.center = center;
        this.radius = new PointF(radius, radius);
    }


    public PointF getCenter() {
        return center;
    }

    public void setCenter(PointF center) {
        this.center = center;
    }

    public void setRadius(float radius) {
        this.radius = new PointF(radius, radius);
    }

    public float getRadius() {
        return this.radius.x;
    }

    /**
     *
     * @return radius of circle (in both dimensions)
     */
    @Override
    public PointF getControlPoint() {
        return radius;
    }

    /**
     * Set radius of circle...
     * @param radius Radius of circle (in both dimensions)
     */
    @Override
    public void setControlPoint(PointF radius) {
        this.radius = radius;
    }

    /**
     *
     * @return coordinates of center
     */
    @Override
    public PointF getEnd() {
        return center;
    }

    /**
     *
     * @param center coordinates of center
     */
    @Override
    public void setEnd(PointF center) {
        this.center = center;
    }

    @Override
    public int getOrder() {
        return this.order;
    }

    @Override
    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public Paint getPaint() {
        return paint;
    }

    @Override
    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    /**
     *
     * @return coordinates of center
     */
    @Override
    public PointF getStart() {
        return center;
    }

    /**
     *
     * @param center coordinates of center
     */
    @Override
    public void setStart(PointF center) {
        this.center = center;
    }

    private static final String DEF_START = "<circle cx=\"";
    private static final String DEF_MIDDLE = "\" cy=\"";
    private static final String DEF_RADIUS = "\" r=\"";
    private static final String DEF_STYLE = "\" style=\"stroke: #";
    private static final String DEF_FILL = "; fill:#";
    private static final String DEF_END = "; stroke-width: 4px\" />";
    private static final String FORMAT = "%.2f";
    private static final String HEX_FORMAT = "%06x";
    private static final int COLOR_WITHOUT_ALPHA_MASK = 0x00FFFFFF;

    @Override
    public String getSvgRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append(DEF_START);

        String appended = String.format(Locale.US, FORMAT, center.x);
        sb.append(appended);

        appended = String.format(Locale.US, FORMAT, center.y);
        sb.append(DEF_MIDDLE);
        sb.append(appended);

        appended = String.format(Locale.US, FORMAT, radius.x);
        sb.append(DEF_RADIUS);
        sb.append(appended);

        sb.append(DEF_STYLE);
        appended = String.format(HEX_FORMAT, paint.getColor() & COLOR_WITHOUT_ALPHA_MASK);
        sb.append(appended);

        appended = String.format(HEX_FORMAT, paint.getColor() & COLOR_WITHOUT_ALPHA_MASK);
        sb.append(DEF_FILL);
        sb.append(appended);

        sb.append(DEF_END);

        String result = sb.toString();
        Log.d("PathInfoCircle - svg", result);
        return result;
    }

    @Override
    public void pathInfoDraw(Path src) {
        src.addCircle(center.x, center.y, radius.x, Path.Direction.CW);
    }
}

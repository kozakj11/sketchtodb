package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by kozack on 4. 10. 2016.
 */
public class LayeredImageView extends ImageView {

    private SketchPosition position;
    private boolean showOverlay, showRectangle;

    public LayeredImageView(Context context) {
        super(context);
    }

    public LayeredImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (position != null) {
            position.drawPosition(canvas, showOverlay, showRectangle);
        } else {
            Log.d("LayeredImageView", "onDraw - position is null");
        }
    }

    public void setPosition(SketchPosition position) {
        this.position = position;
    }

    public void setShowOverlay(boolean showOverlay) {
        this.showOverlay = showOverlay;
    }

    public void setShowRectangle(boolean showRectangle) {
        this.showRectangle = showRectangle;
    }
}

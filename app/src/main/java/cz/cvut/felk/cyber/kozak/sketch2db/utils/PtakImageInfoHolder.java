package cz.cvut.felk.cyber.kozak.sketch2db.utils;

/**
 * Created by kozack on 20. 9. 2016.
 */
public class PtakImageInfoHolder {

    private String link;
    private float scale;
    private float xCenter;
    private float yCenter;
    private boolean isFlipped;

    public PtakImageInfoHolder(String yCenter, String xCenter, String link, String scale, String flip) {
        this.yCenter = Float.parseFloat(yCenter) * 0.5f;
        this.link = link;
        this.scale = Float.parseFloat(scale);
        this.xCenter = Float.parseFloat(xCenter) * 0.5f;
        this.isFlipped = Integer.parseInt(flip) == 1;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public void setScale(String scale) {
        this.scale = Float.parseFloat(scale);
    }

    public float getYcenter() {
        return yCenter;
    }

    public void setYcenter(String yCenter) {
        this.yCenter = Float.parseFloat(yCenter);
    }

    public float getXcenter() {
        return xCenter;
    }

    public void setXcenter(String xCenter) {
        this.xCenter = Float.parseFloat(xCenter);
    }

    public void setxCenter(float xCenter) {
        this.xCenter = xCenter;
    }

    public void setyCenter(float yCenter) {
        this.yCenter = yCenter;
    }

    public boolean isFlipped() {
        return isFlipped;
    }

    public void setFlipped(boolean flipped) {
        isFlipped = flipped;
    }
}

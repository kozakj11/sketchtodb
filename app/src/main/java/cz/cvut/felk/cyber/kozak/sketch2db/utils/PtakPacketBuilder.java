package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import cz.cvut.felk.cyber.kozak.sketch2db.R;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * Created by kozack on 30. 6. 2016.
 */
public class PtakPacketBuilder {

    /*public static final int MAT_CHAR = 2;
    public static final int MAT_REAL = 0;
    public static final int MAT_UINT8 = 4;*/

    private static final int BUFFER_SIZE = 256 * 1024;
    private Context caller;

    private final static byte[] HEADER_QUERY =
            {2,0,0,0,
                    0,0,0,0,
                    2,0,0,0,
                    1,0,0,0};

    public PtakPacketBuilder(Context caller) {
        this.caller = caller;
    }

    public String readInput(byte[] bytes, int len, boolean isHeader) {
        int start = 0;
        byte[] help = new byte[(bytes.length / 2) + 1];
        byte helperByte;
        if (isHeader) {
            start += 24;
        }
        int pos = 0;
        for (int i = start; i < len; i++) {
            helperByte = bytes[i];
            if (helperByte != 0) {
                help[pos] = helperByte;
                pos++;
            }
        }
        return new String(help, 0, pos, StandardCharsets.UTF_8);
    }

    /**
     *
     * @param json
     * @return
     */
    public byte[] serializeJSON(final JSONObject json) {
        String jsonString = json.toString();
        byte[] jsonBytes = jsonString.getBytes(StandardCharsets.UTF_8);

        int len = json.length();
        int msgLength = 8 + HEADER_QUERY.length + 2 * jsonBytes.length;

        // assign header fields (total length, matlab header, json length)
        byte[] toSend = new byte[msgLength];
        byte[] totalLength = intToFourBytes(msgLength - 4),
                stringLength = intToFourBytes(len);
        System.arraycopy(totalLength, 0, toSend, 0, totalLength.length);
        System.arraycopy(HEADER_QUERY, 0, toSend, totalLength.length, HEADER_QUERY.length);
        System.arraycopy(stringLength, 0, toSend, HEADER_QUERY.length + totalLength.length, stringLength.length);

        int startPos = HEADER_QUERY.length + totalLength.length + stringLength.length;
        for (int i = startPos; i < toSend.length; i+=2) {
            toSend[i] = jsonBytes[(i - startPos)/2];
            toSend[i + 1] = 0;
        }

        return toSend;
    }

    public void uploadImage(File img, Socket socket, BufferedOutputStream bos) throws IllegalArgumentException, IOException {
        if (!img.isFile() || !img.exists() || !img.canRead()) {
            throw new IllegalArgumentException("File doesn't exist or can't be read.");
        }

            byte[] imgBuf = byteJsonFromFile(img);

            int outputStreamLength = socket.getSendBufferSize();
            if (outputStreamLength % 2 == 1) {
                outputStreamLength--; // this would be very weird but we need even length
            }
            byte[] sendBuffer = new byte[outputStreamLength];

            int strLength = imgBuf.length;
            int msgLength = 8 + HEADER_QUERY.length + 2 * strLength;

            byte[] totalLength = intToFourBytes(msgLength - 4);
            byte[] stringLength = intToFourBytes(strLength);

            int sentBuf = 0;
            bos.write(totalLength);
            bos.write(HEADER_QUERY);
            bos.write(stringLength);

            Log.d("PacketBuilder", "gonna upload");

            while (sentBuf < strLength) {
                int next = Math.min(outputStreamLength/2, strLength - sentBuf);
                for (int i = 0; i < next; i++) {
                    sendBuffer[2 * i] = imgBuf[i + sentBuf];
                }
                bos.write(sendBuffer, 0, 2 * next);
                sentBuf += next;
            }

            bos.flush();
            Log.d("PacketBuilder", "upload sent");

    }

    private String buildStart(String filename) {
        Log.d("PtakPacket", "buildStart - beforePreferences");
        StringBuilder sb = new StringBuilder();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(caller);
        String param1 = sp.getString(caller.getResources().getString(R.string.param_1_key), "10");
        int param2 = sp.getInt(caller.getResources().getString(R.string.param_2_key), 10);
        String param3 = sp.getString(caller.getResources().getString(R.string.param_3_key), "v");
        sb.append("{\"command\":\"upload\",\"opt\":{\"param1\":\"");
        sb.append(param1);
        sb.append("\",\"param2\":\"");
        sb.append(param2);
        sb.append("\",\"param3\":\"");
        sb.append(param3);
        sb.append("\"},\"filename\":\"");
        sb.append(filename);
        sb.append("\",\"val\":\"");
        return sb.toString();
    }

    private byte[] byteJsonFromFile(File img) throws IllegalArgumentException, IOException {
        byte[] imageBuffer = new byte[BUFFER_SIZE];
        int size = BUFFER_SIZE, start = 0;
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(img))) {
            while (true) {
                int read = bis.read(imageBuffer, start, size - start);
                if (read == -1) {
                    break;
                }
                start += read;
                if (start == size) {
                    size += BUFFER_SIZE;
                    byte[] newBuffer = new byte[size];
                    System.arraycopy(imageBuffer, 0, newBuffer, 0, start);
                    imageBuffer = newBuffer;
                }
            }
        } catch (FileNotFoundException ex) {
            throw new IllegalArgumentException("File doesn't exist or can't be read.");
        }

        byte[] base64img = Base64.encode(imageBuffer, 0, start, Base64.DEFAULT);

        String jStart = buildStart(img.getName());
        Log.d("PtakPacket - byte", jStart);
        String end = "\"}";

        Log.d("PtakPacket", jStart);

        byte[] startB = jStart.getBytes(StandardCharsets.UTF_8);
        byte[] endB = end.getBytes(StandardCharsets.UTF_8);
        byte[] finalBuffer = new byte[base64img.length + startB.length + endB.length];

        System.arraycopy(startB, 0, finalBuffer, 0, startB.length);
        System.arraycopy(base64img, 0, finalBuffer, startB.length, base64img.length);
        System.arraycopy(endB, 0, finalBuffer, base64img.length + startB.length, endB.length);

        return finalBuffer;
    }

    private byte[] intToFourBytes(int num) {
        byte[] ret = {
                (byte) num,
                (byte) ( (num & 0xFF00) >> 8),
                (byte) ( (num & 0xFF0000) >> 16),
                (byte) ( (num & 0xFF000000) >> 24)
        };
        return ret;
    }

}

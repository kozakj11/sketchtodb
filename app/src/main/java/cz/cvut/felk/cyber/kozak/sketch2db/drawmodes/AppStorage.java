package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import cz.cvut.felk.cyber.kozak.sketch2db.utils.ImageDimensionHolder;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakImageInfoHolder;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Package private class for storing PathInfos from Draw classes (stored when activity is stopped).
 */
public class AppStorage {

    private static final Map<Integer, Collection<PathInfo>> PATH_INFO_MAP = new HashMap<>();
    private static final Map<Integer, List<PtakImageInfoHolder>> IMAGE_LIST = new HashMap<>();
    private static int COUNTER = 0;
    private static final Map<Integer, ImageDimensionHolder> HOLDERS = new HashMap<>();
    public static final Map<Integer, Collection<List<PathInfo>>> REDO_LISTS = new HashMap<>();

    private AppStorage() {
        // no need to instantiate this class
    }

    public static List<PtakImageInfoHolder> getStringList(int key) {
        return IMAGE_LIST.remove(key);
    }

    public static int saveStringList(List<PtakImageInfoHolder> infoHolders) {
        IMAGE_LIST.put(COUNTER, infoHolders);
        return COUNTER++;
    }

    public static int saveHolder(ImageDimensionHolder holder) {
        HOLDERS.put(COUNTER, holder);
        return COUNTER++;
    }

    public static ImageDimensionHolder getHolder(int key) {
        ImageDimensionHolder holder = HOLDERS.remove(key);
        return holder;
    }

    static int saveRedoPathsInfos(Collection<List<PathInfo>> lst) {
        REDO_LISTS.put(COUNTER, lst);
        return COUNTER++;
    }

    static Collection<List<PathInfo>> getRedoPathsInfos(int key) {
        return REDO_LISTS.remove(key);
    }

    /**
     * Stores collection with PathInfos.
     * @param pathInfos collection of PathInfos
     * @return key to retrieve the collection from the storage
     */
    static int savePathInfos(Collection<PathInfo> pathInfos) {
        PATH_INFO_MAP.put(COUNTER, pathInfos);
        return COUNTER++;
    }

    /**
     * Retrieves and removes collection with PathInfos from the storage.
     * @param key key from savePathInfos method
     * @return collection of PathInfos stored with given key, or null if there's no such key in the storage
     */
    static Collection<PathInfo> getPathInfos(int key) {
        Collection<PathInfo> toReturn = PATH_INFO_MAP.remove(key);
        return toReturn;
    }

}

package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import cz.cvut.felk.cyber.kozak.sketch2db.SketchAndFind;

import java.io.*;

/**
 * Created by kozack on 17. 7. 2016.
 */
public class PtakLoadCacher implements Runnable {

    private final PtakLoadTask task;
    private boolean isWriting;

    private final static int LOAD_SUCCESS = 0, LOAD_FAIL = 1;
    private int loadState = LOAD_FAIL;

    public PtakLoadCacher(PtakLoadTask task, boolean isWriting) {
        this.task = task;
        this.isWriting = isWriting;
    }

    public void setIsWriting(boolean isWriting) {
        this.isWriting = isWriting;
    }

    @Override
    public void run() {
        task.setCurrentThread(Thread.currentThread());

        try {
            if (isWriting) {
                saveFileToCacheFolder();
            } else {
                loadFileFromCacheFolder();
            }
        } catch (Throwable ex) {
            Log.d("fail", ex.getMessage());
        } finally {
            task.setCurrentThread(null);
            if (!isWriting) {
                if (loadState == LOAD_SUCCESS) {
                    task.handleCacheCheckState(PtakLoadManager.APP_CACHE_HIT);
                } else {
                    task.handleCacheCheckState(PtakLoadManager.APP_CACHE_MISS);
                }
            }
        }
    }

    private void saveFileToCacheFolder() throws IOException {
        File cacheFolder = SketchAndFind.getContext().getCacheDir();
        File toSave = new File(cacheFolder, task.getPath());

        Log.d("cache_save??", toSave.getAbsolutePath());

        if (toSave.exists()) {
            Log.d("cache_save??", "exists");
            return;
        }

        Log.d("cache_save??", "not exists");

        try {
            File f = toSave.getParentFile();
            if (f != null) {
                if (!f.mkdirs()) {
                    Log.d("cache save??","mkdirs not exist");
                    return;
                }
            } else {
                Log.d("cache save??","file is null");
                return;
            }
        } catch (Throwable ex) {
            Log.d("cache_save??", "exception in mkdirs");
            return;
        }

        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(toSave))) {
            if (Thread.interrupted()) {
                return;
            }
            byte[] bytes = task.getBuffer();
            bos.write(bytes);
            bos.flush();

            Log.d("cache_save", "probably suxessful");
        }
    }

    private void loadFileFromCacheFolder() throws IOException {
        File cacheFolder = SketchAndFind.getContext().getCacheDir();
        File toLoad = new File(cacheFolder, task.getPath());

        Log.d("cache_hit??", toLoad.getAbsolutePath());

        if (!toLoad.exists()) {
            Log.d("cache_miss", "Cache miss.");
            return;
        }

        Log.d("cache_hit", "Image found in cache...");

        int len = 16384;

        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(toLoad))){
            if (Thread.interrupted()) {
                return;
            }
            byte[] bytes = new byte[len];
            int start = 0;
            Log.d("PtakLoadCacher", "loading from cache, expected length " + len);
            while (true) {
                Log.d("PtakLoadCacher", "start at " + start);
                if (Thread.interrupted()) {
                    Log.d("PtakLoadCacher", "interrupted");
                    return;
                }
                int read = bis.read(bytes, start, bytes.length - start);
                if (read == -1) {
                    Log.d("PtakLoadCacher", "Read all the file!");
                    break;
                }
                if (start > 0.9 * bytes.length) {
                    len += 32768;
                    byte[] nextBytes = new byte[len];
                    System.arraycopy(bytes, 0, nextBytes, 0, start + read);
                    bytes = nextBytes;
                }
                start += read;
            }
            Log.d("PtakLoadCacher", "loaded from cache");
            if (start != bytes.length) {
                byte[] nextBytes = new byte[start];
                System.arraycopy(bytes, 0, nextBytes, 0, start);
                bytes = nextBytes;
            }
            Log.d("PtakLoadCacher", "setBuffer");
            task.setBuffer(bytes);
            loadState = LOAD_SUCCESS;
        }
    }
}

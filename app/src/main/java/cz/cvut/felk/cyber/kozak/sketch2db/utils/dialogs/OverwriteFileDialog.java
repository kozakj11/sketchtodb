package cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import cz.cvut.felk.cyber.kozak.sketch2db.FileExplorer;
import cz.cvut.felk.cyber.kozak.sketch2db.R;

import java.io.File;

/**
 * Modal dialog for overwriting files in FileExplorer.
 */
public class OverwriteFileDialog extends DialogFragment {

    private FileExplorerDialogListener listener;

    public OverwriteFileDialog() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.listener = (FileExplorerDialogListener) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.file_already_exists_overwrite)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Bundle bun = OverwriteFileDialog.this.getArguments();
                        File file = (File) bun.getSerializable(FileExplorer.DIALOG_FILE);
                        String str = bun.getString(FileExplorer.DIALOG_INPUT);
                        if (file != null && str != null) {
                            listener.onPositiveReply(FileExplorerDialogListener.FileExplorerDialogs.OVERWRITE_FILE, file, str);
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onNegativeReply(FileExplorerDialogListener.FileExplorerDialogs.OVERWRITE_FILE);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
package cz.cvut.felk.cyber.kozak.sketch2db.gallery;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.ViewOverlay;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import cz.cvut.felk.cyber.kozak.sketch2db.R;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.PtakImageInfoHolder;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.ScaledBitmap;

import java.util.List;

/**
 * Image adapter for search results.
 */
public class GalleryImageAdapter extends BaseAdapter {

    private Context context;
    //private Bitmap foreground;
    private String[] filePaths;
    private PtakImageInfoHolder[] holders;
    private int width;
    private OverlayManager manager;
    private Bitmap overlay;
    private float scaling = 1.0f;

    private boolean testMode;

    /**
     * To be deleted when original activity will use fragments too.
     * @param context
     * @param filePaths
     * @param width
     * @param manager
     * @param overlay
     */
    @Deprecated
    public GalleryImageAdapter(Context context, List<String> filePaths, int width, OverlayManager manager, Bitmap overlay) {
        this.context = context;
        this.filePaths = new String[filePaths.size()];
        filePaths.toArray(this.filePaths);
        //this.foreground = BitmapFactory.decodeResource(context.getResources(), R.drawable.sample_foreground);
        this.width = width;
        this.manager = manager;
        this.overlay = overlay;
        testMode = true;
    }

    public GalleryImageAdapter(Context context, int width, List<PtakImageInfoHolder> holders, OverlayManager manager, Bitmap overlay) {
        this.context = context;
        this.holders = new PtakImageInfoHolder[holders.size()];
        holders.toArray(this.holders);
        //this.foreground = BitmapFactory.decodeResource(context.getResources(), R.drawable.sample_foreground);
        this.width = width;
        this.manager = manager;
        this.overlay = overlay;
        testMode = false;
    }

    public GalleryImageAdapter(Context context, int width, List<PtakImageInfoHolder> holders, OverlayManager manager,
                               ScaledBitmap overlay) {
        this.context = context;
        this.holders = new PtakImageInfoHolder[holders.size()];
        holders.toArray(this.holders);
        //this.foreground = BitmapFactory.decodeResource(context.getResources(), R.drawable.sample_foreground);
        this.width = width;
        this.manager = manager;
        this.overlay = overlay.getBitmap();
        this.scaling = overlay.getScaling();
        testMode = false;
    }

    public int getCount() {
        if (testMode) {
            return filePaths.length;
        } else {
            return holders.length;
        }
    }

    public Object getItem(int position) {
        if (testMode) {
            return filePaths[position];
        } else {
            return holders[position];
        }
    }

    public long getItemId(int position) {
        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        PtakImageGrid imageView;
        String pathName = testMode ? filePaths[position] : holders[position].getLink();

        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new PtakImageGrid(context, pathName);
            //manager.registerListener(imageView);
        } else {
            imageView = (PtakImageGrid) convertView;
        }

        imageView.stopDownload();
        imageView.setPath(pathName);

        imageView.setLayoutParams(new GridView.LayoutParams(width, width));
        imageView.progressBarVisible(true);

        ImageView sub = imageView.getImageView();
        sub.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        sub.setPadding(8, 8, 8, 8);

        imageView.setOverlay(overlay);
        imageView.setStartWidth(width);
        imageView.setStartHeight(width);

        if (!testMode) {
            PtakImageInfoHolder hold = holders[position];
            float resizedScale = hold.getScale();// / this.scaling;
            imageView.createSketchPosition(hold.getXcenter(), hold.getYcenter(), resizedScale, hold.isFlipped());
        }

        imageView.startDownload(false, false);
        return imageView;
    }

}

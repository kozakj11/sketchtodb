package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.graphics.*;
import android.util.Log;
import android.view.MotionEvent;
import cz.cvut.felk.cyber.kozak.sketch2db.SketchingView;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.MathUtils;

import java.util.Map;

/**
 * Class for sketching free mode.
 */
public class FreeDraw implements DrawModeListener {

    private float prevX, prevY;
    private static final float DIFFERENCE = 8.0f;
    private final DrawInput input;
    private boolean started = false;

    private PathInfoFree pinfo;
    private PathInfoQuadratic quadratic;

    public FreeDraw(DrawInput input) {
        this.input = input;
    }

    private void startSketch(float x, float y) {
        prevX = x;
        prevY = y;
        input.getPath().moveTo(x, y);
        this.pinfo = new PathInfoFree(input.getOrder());
        this.quadratic = new PathInfoQuadratic();
        this.started = true;
    }

    private void continueSketch(float x, float y) {
        if (MathUtils.distSq(x, y, prevX, prevY) > DIFFERENCE) {
            PointF control = calcControl(x, y);
            input.getPath().quadTo(control.x, control.y, x, y);

            Paint paint = input.getPaint();

            this.quadratic.setPaint(paint);
            this.quadratic.setStart(new PointF(prevX, prevY));
            this.quadratic.setControlPoint(control);
            this.quadratic.setEnd(new PointF(x, y));

            this.pinfo.insertNew(this.quadratic);
            this.quadratic = new PathInfoQuadratic();

            prevX = x;
            prevY = y;
            this.started = false;
        }
    }

    private static final float POINT_RADIUS = 1.0f;

    private void endSketch(float x, float y) {
        Log.d("FreeDraw", "endSketch");
        Path path = input.getPath();

        if (!started) {
            path.lineTo(x, y);
            input.getCanvas().drawPath(input.getPath(), input.getPaint());

            this.quadratic.setPaint(input.getPaint());
            this.quadratic.setStart(new PointF(prevX, prevY));
            this.quadratic.setControlPoint(calcControl(x, y));
            this.quadratic.setEnd(new PointF(x, y));

            this.pinfo.insertNew(this.quadratic);

            input.getPathList().add(path);
            input.getPaths().put(path, this.pinfo);

            input.resetPathAndPaint();
            input.incrementOrder();
            //path.reset();
        } else {
            path.addCircle(x, y, POINT_RADIUS, Path.Direction.CW);

            Paint p = new Paint(input.getPaint());
            p.setStyle(Paint.Style.FILL_AND_STROKE);
            input.getCanvas().drawPath(input.getPath(), p);

            PathInfoCircle cir = new PathInfoCircle(new PointF(x, y), POINT_RADIUS);
            cir.setPaint(p);
            this.pinfo.insertNew(cir);

            input.getPathList().add(path);
            input.getPaths().put(path, cir);

            input.resetPathAndPaint();
            input.incrementOrder();
        }
    }

    @Override
    public void onTouch(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startSketch(x, y);
                break;
            case MotionEvent.ACTION_MOVE:
                continueSketch(x, y);
                break;
            case MotionEvent.ACTION_UP:
                endSketch(x,y);
                break;
        }
    }

    @Override
    public void undoLast() {

    }

    private PointF calcControl(float x, float y) {
        PointF point = new PointF();
        point.x = (x + prevX) * 0.5f;
        point.y = (y + prevY) * 0.5f;
        return point;
    }
}

package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.LruCache;
import cz.cvut.felk.cyber.kozak.sketch2db.gallery.PtakImage;

import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by kozack on 27. 6. 2016.
 */
public class PtakLoadManager {

    private static final int MAX_CORES = Runtime.getRuntime().availableProcessors();
    private static final int CACHE_SIZE = 4 * 1024 * 1024;

    private static final int MAX_ALIVE = 5;
    private static final TimeUnit MAX_ALIVE_UNIT = TimeUnit.SECONDS;

    public static final int TASK_FAILED = -1,
                            DOWNLOAD_COMPLETED = 0,
                            TASK_FINISHED = 1,
                            APP_CACHE_HIT = 2,
                            APP_CACHE_MISS = 3;

    private static final int MIN_WIDTH = 5;

    /**
     * Reference to PtakLoadManager singleton instance.
     */
    // CAUTION - ORDER matters! - static fields used in constructor
    public static final PtakLoadManager PTAK_LOAD_MANAGER = new PtakLoadManager();

    /**
     * Executor, thread and task queues for downloading the images.
     */
    private final ThreadPoolExecutor downloadExecutor;
    private final BlockingQueue<Runnable> downloadThreads;

    private final ThreadPoolExecutor decodeExecutor;
    private final BlockingQueue<Runnable> decodeThreads;

    private final ThreadPoolExecutor cacheExecutor;
    private final BlockingQueue<Runnable> cacheThreads;

    private final Queue<PtakLoadTask> taskQueue;
    private final LruCache<String, byte[]> cache; // storing byte array should need less space than storing Bitmap

    /**
     * Handler to send results to UI thread.
     */
    private final Handler handler;

    private PtakLoadManager() {
        downloadThreads = new LinkedBlockingQueue<>();
        downloadExecutor = new ThreadPoolExecutor(MAX_CORES, MAX_CORES, MAX_ALIVE, MAX_ALIVE_UNIT, downloadThreads);

        decodeThreads = new LinkedBlockingQueue<>();
        decodeExecutor = new ThreadPoolExecutor(MAX_CORES, MAX_CORES, MAX_ALIVE, MAX_ALIVE_UNIT, decodeThreads);

        cacheThreads = new LinkedBlockingQueue<>();
        cacheExecutor = new ThreadPoolExecutor(MAX_CORES, MAX_CORES, MAX_ALIVE, MAX_ALIVE_UNIT, cacheThreads);

        handler = new HandlerImpl(Looper.getMainLooper()); // looper of UI thread
        taskQueue = new LinkedBlockingQueue<>();

        cache = new LruCache<String, byte[]>(CACHE_SIZE) {
            @Override
            protected int sizeOf(String key, byte[] value) {
                return value.length;
            }
        };
    }

    /**
     * Starts download of image and returns responsible PtakLoadTask
     * @param caller View (image) where the result will be shown
     * @param cacheEnabled allows saving image to internal cache
     * @param useOnlyFileCache requests using only cached version of file
     * @param useDetailed downloading detailed image (folder 3) or not (folder 2)
     * @return
     */
    public static PtakLoadTask startDownload(PtakImage caller, boolean cacheEnabled, boolean useOnlyFileCache, boolean useDetailed) {
        PtakLoadManager plm = PTAK_LOAD_MANAGER;
        PtakLoadTask task = plm.taskQueue.poll();
        if (task == null) {
            task = new PtakLoadTask();
        }

        int wantedWidth = Math.max(caller.getStartWidth(), MIN_WIDTH);
        int wantedHeight = Math.max(caller.getStartHeight(), MIN_WIDTH);

        task.initTask(caller, wantedWidth, // height might not be initialized, but we want "squares"
                wantedHeight, cacheEnabled, useDetailed);
        PtakLoadCacher cacher = task.getCacher();
        cacher.setIsWriting(false);
        if (useOnlyFileCache) {
            plm.cacheExecutor.execute(cacher);
        } else {
            task.setBuffer(plm.cache.get(caller.getPath()));
            if (task.getBuffer() == null) {
                plm.cacheExecutor.execute(cacher);
            } else {
                // cache hit
                task.handleDownloadState(PtakLoadDownloader.DOWNLOAD_OK);
            }
        }
        return task;
    }

    public void handleState(PtakLoadTask task, int state) {
        switch(state) {
            case APP_CACHE_HIT:
                // same as download completed
            case DOWNLOAD_COMPLETED: // start decoding
                decodeExecutor.execute(task.getLoadDecoder());
                break;
            case APP_CACHE_MISS:
                downloadExecutor.execute(task.getLoadDownloader());
                break;
            case TASK_FINISHED: // cache photo (if enabled), send it to result grid
                if (task.isCacheEnabled()) {
                    cache.put(task.getPath(), task.getBuffer());
                    PtakLoadCacher cacher = task.getCacher();
                    cacher.setIsWriting(true);
                    cacheExecutor.execute(cacher);
                }
                handler.obtainMessage(state, task).sendToTarget();
                break;
            default: // send the message to handler
                handler.obtainMessage(state, task).sendToTarget();
        }
    }

    private void recycleTask(PtakLoadTask task) {
        task.recycle();
        taskQueue.offer(task);
    }

    public static void cancelAll() {
        PtakLoadTask[] arr = new PtakLoadTask[PTAK_LOAD_MANAGER.taskQueue.size()];
        PTAK_LOAD_MANAGER.taskQueue.toArray(arr);

        synchronized(PTAK_LOAD_MANAGER) {

            for (PtakLoadTask task : arr) {
                Thread thread = task.getCurrentThread();

                if (thread != null) {
                    thread.interrupt();
                }

            }

        }
    }

    public static void removeDownload(PtakLoadTask task, String path) {
        if (task != null && task.getPath().equalsIgnoreCase(path)) {

            synchronized(PTAK_LOAD_MANAGER) {
                Thread t = task.getCurrentThread();

                if (t != null) {
                    t.interrupt();
                }
            }

            task.setCurrentThread(null);
            PTAK_LOAD_MANAGER.downloadThreads.remove(task.getLoadDownloader());
            PTAK_LOAD_MANAGER.decodeThreads.remove(task.getLoadDecoder());

        }
    }

    private class HandlerImpl extends Handler {

        private HandlerImpl(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            PtakLoadTask task = (PtakLoadTask) msg.obj;
            PtakImage view = task.getCallerView();
            String viewPath = null;

            if (view != null) {
                viewPath = view.getPath();
            }

            if (viewPath != null && viewPath.equalsIgnoreCase(task.getPath())) {
                int status = msg.what;
                switch (status) {
                    case TASK_FINISHED:
                        view.setSuccessImage(task.getResultImage(), task.getScale());
                        break;
                    case TASK_FAILED:
                        view.setFailedImage();
                        recycleTask(task);
                        break;
                }
            }
        }
    }

}

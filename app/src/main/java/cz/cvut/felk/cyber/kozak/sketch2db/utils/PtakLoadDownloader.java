package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.os.Process;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by kozack on 27. 6. 2016.
 */
public class PtakLoadDownloader implements Runnable {

    public static final int DEFAULT_TIMEOUT_MILIS = 15000;
    public static final int HTTP_OK = 200;

    public static final int DOWNLOAD_OK = 1;
    public static final int DOWNLOAD_FAIL = -1;

    private static final int BUFFER_SIZE = 4096;

    private boolean useDetailed = false;

    private final static String PTAK = "http://ptak.felk.cvut.cz/G2G/data_img2/";
    private final static String PTAK_DETAILED = "http://ptak.felk.cvut.cz/G2G/data_img3/";

    private final PtakLoadTask task;
    private final int timeout;

    public PtakLoadDownloader(PtakLoadTask task) {
        this(task, DEFAULT_TIMEOUT_MILIS);
    }

    public PtakLoadDownloader(PtakLoadTask task, int timeoutMilis) {
        this.task = task;
        this.timeout = timeoutMilis;
    }

    private void setConnectionParams(HttpURLConnection conn) {
        conn.setReadTimeout(timeout);
        conn.setConnectTimeout(timeout);
        conn.setDoInput(true);
    }

    public void useDetailed(boolean useDetailed) {
        this.useDetailed = useDetailed;
    }

    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        task.setCurrentThread(Thread.currentThread());

        BufferedInputStream bis = null;
        byte[] byteBuffer = task.getBuffer();

        try {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            if (null == byteBuffer) {
                // Defines a handle for the byte download stream
                BufferedInputStream byteStream = null;

                String folder = useDetailed ? PTAK_DETAILED : PTAK;

                // Downloads the image and catches IO errors
                try {
                    URL url = new URL(folder + task.getPath());
                    HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
                    setConnectionParams(httpConn);
                    httpConn.setRequestMethod("GET");

                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }

                    httpConn.connect();
                    int response = httpConn.getResponseCode();

                    if (response != HTTP_OK) {
                        task.handleDownloadState(DOWNLOAD_FAIL);
                        return;
                    }

                    byteStream = new BufferedInputStream(httpConn.getInputStream());
                    if (Thread.interrupted()) {

                        throw new InterruptedException();
                    }
                    int contentSize = httpConn.getContentLength();
                    if (-1 == contentSize) {
                        byte[] tempBuffer = new byte[BUFFER_SIZE];
                        int bufferLeft = tempBuffer.length;

                        int bufferOffset = 0;
                        int readResult = 0;

                        outer: do {
                            while (bufferLeft > 0) {
                                readResult = byteStream.read(tempBuffer, bufferOffset, bufferLeft);

                                if (readResult < 0) {
                                    break outer;
                                }

                                bufferOffset += readResult;
                                bufferLeft -= readResult;

                                if (Thread.interrupted()) {

                                    throw new InterruptedException();
                                }
                            }
                            bufferLeft = BUFFER_SIZE;
                            int newSize = tempBuffer.length + BUFFER_SIZE;
                            byte[] expandedBuffer = new byte[newSize];
                            System.arraycopy(tempBuffer, 0, expandedBuffer, 0, tempBuffer.length);
                            tempBuffer = expandedBuffer;
                        } while (true);

                        byteBuffer = new byte[bufferOffset];
                        System.arraycopy(tempBuffer, 0, byteBuffer, 0, bufferOffset);

                    } else {
                        byteBuffer = new byte[contentSize];
                        int remainingLength = contentSize;
                        int bufferOffset = 0;

                        while (remainingLength > 0) {
                            int readResult = byteStream.read(byteBuffer, bufferOffset, remainingLength);
                            if (readResult < 0) {
                                throw new EOFException();
                            }
                            bufferOffset += readResult;
                            remainingLength -= readResult;

                            if (Thread.interrupted()) {
                                throw new InterruptedException();
                            }
                        }
                    }

                    if (Thread.interrupted()) {

                        throw new InterruptedException();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                } finally {
                    if (null != byteStream) {
                        try {
                            byteStream.close();
                        } catch (Exception e) {

                        }
                    }
                }
            }
            task.setBuffer(byteBuffer);
            task.handleDownloadState(DOWNLOAD_OK);

        } catch (InterruptedException e1) {

        } finally {
            if (null == byteBuffer) {
                task.handleDownloadState(DOWNLOAD_FAIL);
            }
            task.setCurrentThread(null);
            Thread.interrupted();
        }


    }

}

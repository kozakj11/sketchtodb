package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import cz.cvut.felk.cyber.kozak.sketch2db.drawmodes.PathInfo;

import java.util.List;

/**
 * Class that handles drawing XML/SVG files into bitmaps.
 * Created by kozack on 15. 9. 2016.
 */
public class XmlBasicDrawer {

    private List<PathInfo> pinfos;
    private float height, width;
    private float scale = 1.0f;
    private boolean isOverlayDrawing = false;

    // used when creating bitmap that is shown over results
    private static final Paint overlayPaint;

    // set overlayPaint to green (0,255,0)
    static {
        overlayPaint = new Paint(Paint.DITHER_FLAG);
        overlayPaint.setColor(Color.GREEN);
        overlayPaint.setAntiAlias(true);
        overlayPaint.setStyle(Paint.Style.STROKE);
        overlayPaint.setStrokeJoin(Paint.Join.MITER);
        overlayPaint.setStrokeWidth(10f);
    }

    /**
     * Creates an instance of XML/SVG drawer for specified list of paths.
     * @param pinfos List of paths to be drawn.
     * @param width Width of SVG image
     * @param height Height of SVG image
     */
    public XmlBasicDrawer(List<PathInfo> pinfos, float width, float height) {
        this.pinfos = pinfos;
        this.height = height;
        this.width = width;
    }

    /**
     * Setter for drawing mode. If set to true, drawer will use 10px green lines, otherwise it uses color and width as
     * specified in path info.
     * @param isOverlayDrawing true if returned image is to be shown over results
     */
    public void setIsOverlayDrawing(boolean isOverlayDrawing) {
        this.isOverlayDrawing = isOverlayDrawing;
    }

    private void drawSvg(Canvas canvas) {
        for (PathInfo pi: pinfos) {
            Path p = new Path();
            pi.pathInfoDraw(p);
            Paint paint = isOverlayDrawing ? overlayPaint : pi.getPaint();
            canvas.drawPath(p, paint);
        }
    }

    /**
     * Creates a bitmap with SVG image drawn onto it. If requested maximal dimension is smaller than any size of
     * original SVG image, then the image is scaled to requested size. If original size is smaller then requested
     * maximal dimension, image is drawn in original size as specified in SVG.
     * @param maxDimension Maximal dimension of returned bitmap
     * @return SVG image drawn to bitmap
     */
    public Bitmap getBitmap(int maxDimension) {
        float w = width, h = height;
        Bitmap bmp1 = Bitmap.createBitmap((int) w, (int) h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp1);
        drawSvg(canvas);

        if (maxDimension < w || maxDimension < h) {
            scale = w > h ? maxDimension / w : maxDimension / h;
            bmp1 = Bitmap.createScaledBitmap(bmp1, (int) (w * scale), (int) (h * scale), false);
        }

        return bmp1;
    }

    /**
     * Returns scale factor used when drawing SVG onto bitmap when calling getBitmap.
     * @return scale factor (defaults to 1.0f if getBitmap hasn't been called yet).
     */
    public float getScale() {
        return scale;
    }

}

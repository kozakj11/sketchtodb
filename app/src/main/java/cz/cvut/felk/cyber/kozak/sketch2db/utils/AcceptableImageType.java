package cz.cvut.felk.cyber.kozak.sketch2db.utils;

import android.graphics.Bitmap;

/**
 * Enum with image types the application can work with.
 * Created by kozack on 4. 6. 2016.
 */
public enum AcceptableImageType {

    SVG(".svg", true, null),
    JPG(".jpg", false, Bitmap.CompressFormat.JPEG),
    PNG(".png", false, Bitmap.CompressFormat.PNG),
    JPEG(".jpeg", false, Bitmap.CompressFormat.PNG);

    private String suffix;
    private boolean isSvg;
    private Bitmap.CompressFormat compressFormat;

    AcceptableImageType(String suffix, boolean isSvg, Bitmap.CompressFormat compressFormat) {
        this.suffix = suffix;
        this.isSvg = isSvg;
        this.compressFormat = compressFormat;
    }

    public String getSuffix() {
        return suffix;
    }

    public boolean isSvg() {
        return isSvg;
    }

    public Bitmap.CompressFormat getCompressFormat() {
        return compressFormat;
    }

    @Override
    public String toString() {
        return suffix;
    }

    /**
     * Finds AcceptableImageType according to file extension.
     * @param name File name to check
     * @return Acceptable Image type according to file extension or null if no matches.
     */
    public static AcceptableImageType getFromFileName(String name) {
        if (name == null) {
            return null;
        } else {

            for (AcceptableImageType ait : AcceptableImageType.values()) {
                if (name.endsWith(ait.getSuffix())) {
                    return ait;
                }
            }

            return null;
        }
    }
}

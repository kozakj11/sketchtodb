package cz.cvut.felk.cyber.kozak.sketch2db.drawmodes;

import android.content.Context;
import android.graphics.*;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import cz.cvut.felk.cyber.kozak.sketch2db.SketchingView;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.ImageSaver;
import cz.cvut.felk.cyber.kozak.sketch2db.utils.MathUtils;

import java.util.*;

/**
 * Class for sketching in path mode.
 */
public class PathDraw implements DrawModeListener {

    private boolean editing;

    private float xStart, yStart, xEnd, yEnd;

    private final DrawInput input;

    private PathInfo pathInfo;

    // approximate value of density independent pixels per cm
    // (DIP = 1 px on a 160 dpi screen, 64 = 160 / 2.5)
    private static final float DIP_CM_SQ = 4096.0f; // 64^2
    // maximal squared distance from edited point/path to consider actual action a continuation of previous one
    private final float editingDifference;

    public PathDraw(Context context, DrawInput input) {
        this.input = input;
        this.editing = false;
        this.editingDifference = this.calcEditingDifference(context.getResources().getDisplayMetrics());
    }

    private float calcEditingDifference(DisplayMetrics dm) {
        return dm.density * dm.density * DIP_CM_SQ * 0.25f;
    }

    private void moved(float x, float y) {
        Path path = input.getPath();
        path.reset();
        path.setLastPoint(xStart, yStart);
        path.lineTo(x, y);
    }

    private void down(float x, float y) {
        Path path = input.getPath();

        path.reset();
        if (MathUtils.distSq(x, y, xEnd, yEnd) > editingDifference) {
            xStart = x;
            yStart = y;
        } else {
            xStart = xEnd;
            yStart = yEnd;
        }
        // create a new PathInfo
        input.incrementOrder();
        pathInfo = new PathInfoQuadratic(input.getOrder());
        pathInfo.setStart(new PointF(xStart, yStart));
        pathInfo.setPaint(input.getPaint());

        input.getPathList().add(path);
        path.setLastPoint(xEnd, yEnd);
    }

    private void up(float x, float y) {
        xEnd = x;
        yEnd = y;
        editing = true;
        pathInfo.setEnd(new PointF(xEnd, yEnd));

        float x2 = xStart + xEnd, y2 = yEnd + yStart;
        pathInfo.setControlPoint(new PointF(x2 * 0.5f, y2 * 0.5f));

        input.getPaths().put(input.getPath(), pathInfo);
        moved(x, y);
       // canvas.drawPath(nPath, nPaint);
    }

    private void editDown(float x, float y) {
        if (MathUtils.segmentDistSq(xStart, yStart, xEnd, yEnd, x, y) < editingDifference) {
            editMoved(x, y);
        } else {
            editUp(x, y);
            down(x, y);
        }

    }

    private void editUp(float x, float y) {
        editing = false;
        input.getCanvas().drawPath(input.getPath(), input.getPaint());
        pathInfo.setControlPoint(new PointF(x, y));

        input.resetPathAndPaint();
    }

    private void editMoved(float x, float y) {
        Path path = input.getPath();
        path.reset();
        path.setLastPoint(xStart, yStart);
        path.quadTo(x, y, xEnd, yEnd);
    }

    @Override
    public void onTouch(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        if (editing) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    editMoved(x,y);
                    break;
                case MotionEvent.ACTION_DOWN:
                    editDown(x,y);
                    break;
                case MotionEvent.ACTION_UP:
                    editUp(x,y);
            }
        } else {
            switch(event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    moved(x,y);
                    break;
                case MotionEvent.ACTION_DOWN:
                    down(x,y);
                    break;
                case MotionEvent.ACTION_UP:
                    up(x,y);
            }
        }
    }

    @Override
    public void undoLast() {
            // the best choice would be one of start/end point of new last path, but I don't see any good way
            // how to get them, so I use just some negative number which is definitely out of the screen
            xEnd = -8192.0f;
            yEnd = -8192.0f;
            // to ensure that undone path is not drawn again during onDraw call
            input.resetPathAndPaint();
            editing = false;

    }

}

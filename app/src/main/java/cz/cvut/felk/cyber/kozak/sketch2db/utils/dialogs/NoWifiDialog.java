package cz.cvut.felk.cyber.kozak.sketch2db.utils.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import cz.cvut.felk.cyber.kozak.sketch2db.R;

/**
 * Created by kozack on 5. 9. 2016.
 */
public class NoWifiDialog extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inf = getActivity().getLayoutInflater();
        View dialogText = inf.inflate(R.layout.no_wifi_dialog, null);

        builder.setView(dialogText).setNeutralButton(R.string.ok, null);
        // Create the AlertDialog object and return it
        return builder.create();
    }
}


